﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.CompilerServices;
using Solomon.CreditRiskModel;
using Solomon.CreditRiskModel;
using System.Xml;
using System.IO;
using System.Configuration;
using Solomon.CreditRiskModel.Data;
using Solomon.CreditRiskModel.Data.Writer;
using Solomon.CreditRiskModel.Data.Reader;

namespace Solomon.CreditRiskModel.KeyComponents
{
    public class FileManager :IFiles
    {
        public FileManager()
        {

        }

        public string GenerateNewKey(string path, string companyName, string effectiveDate, string expirationDate,
            string key, byte[] rfc)
        {
            try
            {
                if (!effectiveDate.Contains("/"))
                    return "Please ensure that Effective Date is in 'mm/dd/yyyy' format and try again.";
                if (!expirationDate.Contains("/"))
                    return "Please ensure that Expiration Date is in 'mm/dd/yyyy' format and try again.";
                string encryptedText = BuildClientKey(companyName,effectiveDate, expirationDate, key,rfc);

                List<string> keyTextList = new List<string>();
                keyTextList.Add(encryptedText);

                FactoriesFactory factoryfactory = new FactoriesFactory();
                WriterFactory factory = (WriterFactory)factoryfactory.Create("WRITER");
                TxtWriter writer = (TxtWriter)factory.Create("TEXT", "");
                
                //using this because is already encrypted above.
                writer.GenerateFile(keyTextList, path);

                writer.Close();

                return "Key created";
            }
            catch (Exception ex)
            {
                return "Error in GenerateNewKey: " + ex.Message;
            }
        }

        public string GenerateNewEsdFile(string pathDecrypted, string pathEncrypted, string key, byte[] rfc)
        {
            FactoriesFactory factoryfactory = new FactoriesFactory();
            ReaderFactory readerFactory = (ReaderFactory)factoryfactory.Create("READER"); 
            TxtReader reader = (TxtReader)readerFactory.Create("TEXT", "");

            try
            {
                List<string> lines = new List<string>();
                reader.Open(pathDecrypted);
                while (!reader.EndOfStream)
                {
                    lines.Add(reader.Read().value);
                }
                reader.Close();
                WriterFactory writerFactory = (WriterFactory)factoryfactory.Create("WRITER");
                TxtWriter writer = (TxtWriter)writerFactory.Create("TEXT", "");
                string newPath = pathEncrypted + ".rdd";

                writer.GenerateEncryptedFile(lines,newPath, key, rfc);
                    
                return string.Empty;
            }
            catch (Exception ex)
            {
                return "Error in GenerateNewEsdFile(): " + ex.Message;
            }
        }

        public string DecryptExistingEsdFile(string pathEncrypted, string pathDecrypted, string key, byte[] rfc)
        {
            try
            {
                FactoriesFactory factoryfactory = new FactoriesFactory();
                ReaderFactory readerFactory = (ReaderFactory)factoryfactory.Create("READER");
                TxtReader reader = (TxtReader)readerFactory.Create("TEXT", "");
                reader.Open(pathEncrypted);
                List<string> lines = new List<string>();
                while (!reader.EndOfStream)
                {
                    lines.Add(reader.Read().value);
                }
                WriterFactory writerFactory = (WriterFactory)factoryfactory.Create("WRITER");
                TxtWriter writer = (TxtWriter)writerFactory.Create("TEXT", "");
                string newPath = pathDecrypted.Remove(pathDecrypted.Length - 3) + "txt";
                writer.GenerateDecryptedFile(lines, newPath, key, rfc);
                writer.Close();
                return string.Empty;
            }
            catch (Exception ex)
            {
                ErrorsInfo.AddErrorInfo("DecryptExistingEsdFile", ex.Message);
                return "Error in DecryptExistingEsdFile(): " + ex.Message;
            }
        }

        internal string BuildClientKey(string company, 
            string effectiveDate, string expirationDate, string key, byte[] rfc)
        {
            string retVal = string.Empty;
            retVal = company.Trim() + "$" + effectiveDate.Trim() + "$" + expirationDate.Trim() ;
            Cryptographer crypto = new Cryptographer(key, rfc);
            return crypto.Encrypt(retVal);
        }
        
        public string EncryptString(string theText, string key, byte[] rfc)
        {
            Cryptographer crypto = new Cryptographer(key, rfc);
            return crypto.Encrypt(theText);

        }

        public string DecryptString(string theText, string key, byte[] rfc)
        {
            Cryptographer crypto = new Cryptographer(key, rfc);
            return crypto.Decrypt(theText);
        }

        public bool DecryptKey(string keyText, ref string company,
            ref string expirationDate, ref string effectiveDate, string key, byte[] rfc)
        {
            try
            {
                Cryptographer crypto = new Cryptographer(key, rfc);
                string[] keyDetails = crypto.Decrypt(keyText).Split('$');
                company = keyDetails[0];
                expirationDate = keyDetails[1];
                effectiveDate = keyDetails[2];
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool UserIsExpired(string keyFilePath, string key, byte[] rfc)
        {
            FactoriesFactory factoryfactory = new FactoriesFactory();
            ReaderFactory factory = (ReaderFactory)factoryfactory.Create("READER"); 
            TxtReader reader = (TxtReader)factory.Create("TEXT", "");
            try
            {
                string company = string.Empty;
                string expirationDate = string.Empty;
                string effectiveDate = string.Empty;
                reader.Open(keyFilePath);
                ReaderDetails details = reader.ReadWithDecryption(key, rfc);
                string[] detailsParts = details.value.Split('$');
                string[] dateParts = detailsParts[2].Split('/');
                DateTime expired = new DateTime(Int32.Parse(dateParts[2]),
                    Int32.Parse(dateParts[0]), Int32.Parse(dateParts[1]));
                if (DateTime.Now > expired)
                {
                    ErrorsInfo.AddErrorInfo("KeyManager", "Your key seems to have expired.");
                    return true;
                }
                return false;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                reader.Close();
            }
        }

        public bool UserIsEffective(string keyFilePath, string key, byte[] rfc)
        {
            FactoriesFactory factoryfactory = new FactoriesFactory();
            ReaderFactory factory = (ReaderFactory)factoryfactory.Create("READER"); 
            TxtReader reader = (TxtReader)factory.Create("TEXT", "");
            try
            {
                string company = string.Empty;
                string expirationDate = string.Empty;
                string effectiveDate = string.Empty;
                reader.Open(keyFilePath);
                ReaderDetails details = reader.ReadWithDecryption(key, rfc);
                string[] detailsParts = details.value.Split('$');
                string[] dateParts = detailsParts[1].Split('/');
                DateTime effective = new DateTime(Int32.Parse(dateParts[2]),
                    Int32.Parse(dateParts[0]), Int32.Parse(dateParts[1]));
                if (DateTime.Now >= effective)
                {
                    return true;
                }
                ErrorsInfo.AddErrorInfo("KeyManager", "Your key does not seem to be effective yet.");
                return false;
            }
            catch (Exception ex)
            {
                ErrorsInfo.AddErrorInfo("UserIsEffective", ex.Message);
                return false; //throw ex;
            }
            finally
            {
                reader.Close();
            }
        }


        public bool GetKeyFileContents(string path, ref string company, ref string effectiveDate, ref string expiredDate,
            string key, byte[] rfc)
        {
            try
            {
                FactoriesFactory factoryfactory = new FactoriesFactory();
                ReaderFactory factory = (ReaderFactory)factoryfactory.Create("READER"); 
                TxtReader reader = (TxtReader)factory.Create("TEXT", "");
                reader.Open(path);
                ReaderDetails details = reader.ReadWithDecryption(key, rfc);
                string[] keyParts = details.value.ToString().Split('$');
                company = keyParts[0];
                effectiveDate = keyParts[1];
                expiredDate = keyParts[2];
                return true;
            }
            catch (Exception ex)
            {
                ErrorsInfo.AddErrorInfo("GetKeyFileContents", ex.Message);
                return false;
            }
        }
        
    }
}
