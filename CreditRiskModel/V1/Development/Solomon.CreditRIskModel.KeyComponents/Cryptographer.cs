﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Reflection;
using System.IO;
//using Microsoft.DSG.Security.CryptoServices;

namespace Solomon.CreditRiskModel.KeyComponents
{
    [Serializable]
    public static class Cryptographer
    {
        /* WORKS
        public static string EncryptionKey = "0S1A2C3T4W5S6G7L8C9DBRMF"; // "QWsmTvZ7cjMFNmmGsPZmlP5t"; // "QWsmTvZ7cjMFNmmGsPZmlP5t";
        public static string EncryptionIV = "SACTW0S1G2L3C4D5B6R7M8F9"; //"QWsmTvZ7cjMFNmmGsPZmlP5t"; // EncryptionKey; //= "test"; //"QWsmTvZ7cjMFNmmGsPZmlP5t";// "76aRWIkq1CJLnMFjPwo9kHMA";
        */

        //these look like, but are not, same as in ProfileLite:
        public static string EncryptionKey = "0S1AL2ECY3RT4W7L8C9DBRMF";
        public static string EncryptionIV =  "SACTW0L3C48DQ5HBV6R7M8F9";


        //NOTE: Must also have reference to System.Security here and in calling project. Calling project also has to have local copy of Microsoft.DSG.Security.CryptoServices.
        //public Cryptographer()
        //{

        //    {
        //        //if (EmbeddedAssembly.Load("CryptoServices.dll", "CryptoServices.dll"))
        //        //    AppDomain.CurrentDomain.AssemblyResolve += new ResolveEventHandler(CurrentDomain_AssemblyResolve);
        //    }
        //}

        //private Assembly CurrentDomain_AssemblyResolve(object sender, ResolveEventArgs args)
        //{
        //    return null; // EmbeddedAssembly.Get(args.Name);
        //}


        static byte[] StreamToBytes(Stream input)
        {
            var capacity = input.CanSeek ? (int)input.Length : 0;
            using (var output = new MemoryStream(capacity))
            {
                int readLength;
                var buffer = new byte[4096];

                do
                {
                    readLength = input.Read(buffer, 0, buffer.Length);
                    output.Write(buffer, 0, readLength);
                }
                while (readLength != 0);

                return output.ToArray();
            }
        }
        public static string Encrypt(string aString)
        {
            // Note that the key and IV must be the same for the encrypt and decrypt calls.
            string results = string.Empty;
            try
            {
                TDES tdesEngine = new TDES(EncodingType.ASCIIEncoding);
                tdesEngine.StringKey = EncryptionKey;
                tdesEngine.StringIV = EncryptionIV;
                results = tdesEngine.Encrypt(aString);
            }
            catch (Exception anError)
            {
                throw anError;
            }

            return results;

        }

        public static  string Decrypt(string aString)
        {
            // Note that the key and IV must be the same for the encrypt and decript calls.
            string results = string.Empty;

            try
            {
                TDES tdesEngine = new TDES(EncodingType.ASCIIEncoding);
                tdesEngine.StringKey = EncryptionKey;
                tdesEngine.StringIV = EncryptionIV;
                results = tdesEngine.Decrypt(aString);
            }
            catch (Exception anError)
            {
                throw anError;
            }

            return results;

        }

    }
}