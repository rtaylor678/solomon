﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Solomon.CreditRiskModel.Security;
using System.IO;
namespace Solomon.CreditRiskModel.Security.Test
{
    [TestClass]
    public class SolutionTests
    {
        [TestMethod]
        public void TestCrypto()
        {
            string _key = "AMAR2SPBNRAP390";
            byte[] _rfc2898DeriveBytes = new byte[] { 0x49, 0x76, 0x61, 0x6e, 0x20, 0x4d, 0x65, 0x64, 0x76, 0x65, 0x64, 0x65, 0x76 };
            Cryptographer crypto = new Cryptographer(_key, _rfc2898DeriveBytes);
            string start = "ABC123$%^";
            string enc = crypto.Encrypt(start);
            string dec = crypto.Decrypt(enc);
            Assert.IsTrue(start == dec);
        }
    }
}
