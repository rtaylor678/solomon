﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Solomon.CreditRiskModel.Services;
using Microsoft.Win32;
using System.IO;
using System.Configuration;
using System.Reflection;
using Solomon.CreditRiskModel;
using System.Xml;

namespace Solomon.CreditRiskModel.Presentation2
{
    public partial class Main : Form
    {
        private DataHolder _dataHolder = null;
        private string _keyFilter = "KEY files|*.key";
        private string _csvFilter = "CSV files|*.csv";
        private string _rddFilter = "RDD files|*.rdd";
        private string _keyEncryptionKey = "884587801A43486";
        private byte[] _rfcEncryptionKey = new byte[] { 74, 40, 76, 81, 70, 35, 14, 86, 70, 37, 61, 87, 62 };
        private string _keyEncryptionESD = "F545A7BA84FA4E8";
        private byte[] _rfcEncryptionESD = new byte[] { 64, 52, 67, 50, 59, 48, 98, 22, 47, 26, 29, 29, 82 };
        private string _keyEncryptionInterestRates= "DEE33F5D39414A3";
        private byte[] _rfcEncryptionInterestRates = new byte[] { 98, 10, 226, 95, 90, 243, 33, 106, 99, 66, 70, 57, 100 };
        private string _dataPath = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments).ToString() + @"\Credit Risk Model";

        private string _lastFinanceDataPath = string.Empty;
        private string _lastInterestRatesPath = string.Empty;
        private string _msgBoxCaption = "HSB Credit Risk Model";
        private string _settingsPath = string.Empty;
        private Dictionary<string, string> _settings = new Dictionary<string, string>();
        private string _keyFilePathKey = "KeyFilePath";
        private string _keyFilePath = string.Empty;
        private string _lastFinanceDataPathKey = "LastFinanceDataPath";
        private string _lastInterestRatesPathKey = "LastInterestRatesPath";
        private string _esdDataPathKey = "EsdDataPath";
        private string _esdDataPath = string.Empty;
        private string _ernieRtf = @"{\rtf1\ansi\ansicpg1252\deff0\deflang1033{\fonttbl{\f0\fnil\fcharset0 Arial;}{\f1\fnil\fcharset0 Calibri;}}{\colortbl ;\red102\green102\blue102;}{\*\generator Msftedit 5.41.21.2510;}\viewkind4\uc1\pard\sa200\sl276\slmult1\lang9\b\fs32 E\b0 quivalent \b R\b0 isk a\b N\b0 d \b I\b0 nterest \b E\b0 nhancement\cf1\f1\fs22\par}";
        //private string _p20Rtf = @"{\rtf1\ansi\ansicpg1252\deff0\deflang1033{\fonttbl{\f0\fnil\fcharset0 Arial;}{\f1\fnil\fcharset0 Calibri;}}{\*\generator Msftedit 5.41.21.2510;}\viewkind4\uc1\pard\sa200\sl276\slmult1\lang9\fs20 P\sub 20\nosupersub >\f1\fs22\par}";
        private string _p20Rtf = @"{\rtf1\ansi\ansicpg1252\deff0\deflang1033{\fonttbl{\f0\fnil\fcharset0 Arial;}{\f1\fnil\fcharset0 Calibri;}}{\*\generator Msftedit 5.41.21.2510;}\viewkind4\uc1\pard\sa200\sl276\slmult1\lang9\fs20 P\sub 20 \nosupersub >\f1\fs22\par}";
        private string _p10Rtf = @"{\rtf1\ansi\ansicpg1252\deff0\deflang1033{\fonttbl{\f0\fnil\fcharset0 Arial;}{\f1\fnil\fcharset0 Calibri;}}{\*\generator Msftedit 5.41.21.2510;}\viewkind4\uc1\pard\sa200\sl276\slmult1\lang9\fs20 P\sub 10 \nosupersub >\f1\fs22\par}";


        public Main()
        {
            try
            {
                InitializeComponent();

                try
                {
                    Directory.CreateDirectory(_dataPath);
                }
                catch { }
                try
                {
                    //override below with config file setting, if exists.
                    _settingsPath = ConfigurationManager.AppSettings["SettingsPath"];
                }
                catch { }
                
                try
                {
                    if(_settingsPath == null || _settingsPath.Length<1)
                        _settingsPath = _dataPath + @"\Settings.xml";
                }
                catch { }

                RtfSubscripts(rtf90, "P90 >");
                RtfSubscripts(rtf80, "P80 >");
                RtfSubscripts(rtf70, "P70 >");
                RtfSubscripts(rtf60, "P60 >");
                RtfSubscripts(rtf50, "P50 >");
                RtfSubscripts(rtf40, "P40 >");
                RtfSubscripts(rtf30, "P30 >");
                RtfSubscript20(rtf20); 
                RtfSubscript10(rtf10); 

                
                lblDateTime.Text = DateTime.Now.ToString();


                if (!FindKeyFile())
                    Quit();
                
                /* this moved to method called by FindKeyFile() above.
                try
                {
                    if (_keyFilePath.Trim().Length > 0)
                    {
                        SaveSetting(_keyFilePathKey, _keyFilePath);
                    }
                }
                catch { }
                */

                

                ServiceManager servicemanager = new ServiceManager();
                if (!servicemanager.UserIsEffective(_keyFilePath,_keyEncryptionKey,_rfcEncryptionKey))
                    return;
                if (servicemanager.UserIsExpired(_keyFilePath,_keyEncryptionKey,_rfcEncryptionKey))
                    return;
                string company = string.Empty;
                string effectiveDate = string.Empty;
                string expired = string.Empty;
                if (servicemanager.GetKeyFileContents(_keyFilePath, ref company, ref effectiveDate, ref expired,_keyEncryptionKey,_rfcEncryptionKey))
                    lblLicensedTo.Text = "Licensed to: " + company;

                _dataHolder = new DataHolder();

             
                FindCsvFile(0);
                if (!LoadFinanceDataGrid(GetSetting(_lastFinanceDataPathKey)))
                { }
                FindCsvFile(1);
                if (!LoadInterestRates(GetSetting(_lastInterestRatesPathKey)))
                { }

                XmlRead(); //load from xml into dictionary
                
                gridNoInsurance.DefaultCellStyle.Font = new Font("Arial", 10);
                gridWithIns.DefaultCellStyle.Font = new Font("Arial", 10);
                gridCreditRiskSavings.DefaultCellStyle.Font = new Font("Arial", 10);
                gridLossReserves.DefaultCellStyle.Font = new Font("Arial", 10);
                gridInterestRates.DefaultCellStyle.Font = new Font("Arial", 10);
                gridInterestRates.DefaultCellStyle.ForeColor = Color.Black;
                gridFinanceData.DefaultCellStyle.Font = new Font("Arial", 10);
                toolTip1.SetToolTip(btnProjectSavingsDistributionFile,"");


                gridNoInsurance.DefaultCellStyle.ForeColor = Color.Black;
                gridWithIns.DefaultCellStyle.ForeColor = Color.Black;
                gridCreditRiskSavings.DefaultCellStyle.ForeColor = Color.Black;
                gridLossReserves.DefaultCellStyle.ForeColor = Color.Black;
                gridInterestRates.DefaultCellStyle.ForeColor = Color.Black;
                gridFinanceData.DefaultCellStyle.ForeColor = Color.Black;


                //centering grid headers

                CenterGridHeaders(gridNoInsurance);
                CenterGridHeaders(gridWithIns);
                CenterGridHeaders(gridCreditRiskSavings);
                CenterGridHeaders(gridLossReserves);
                CenterGridHeaders(gridInterestRates);
                CenterGridHeaders(gridFinanceData);
                
                //this.CreditRatingInsured.DropDownStyle = ComboBoxStyle.DropDownList;
                //this.CreditRatingInsurer.DropDownStyle = ComboBoxStyle.DropDownList;

                _esdDataPath = GetSetting(_esdDataPathKey);
                string esdPathTemp = _esdDataPath;
                if (esdPathTemp.Length > 0)
                {
                    string[] parts = esdPathTemp.Split('\\');
                    btnProjectSavingsDistributionFile.Text = parts[parts.Length - 1];
                }

                this.SizeChanged+= frm_sizeChanged;

                HideHorizontalScrollbar();
            }
            catch(Exception ex)
            {
                ErrorsInfo.AddErrorInfo("Main",ex.Message);
            }
            finally
            {
                if (ErrorsInfo.GetAllErrors().Length > 0)
                {
                    ErrorsPopup popup = new ErrorsPopup(ErrorsInfo.GetAllErrors(), false);
                    popup.ShowDialog();
                    popup.Dispose();
                    Quit();
                }
            }
        }

        private void CenterGridHeaders(DataGridView grid)
        {
            try
            {
                foreach (DataGridViewColumn col in grid.Columns)
                {
                    col.SortMode = DataGridViewColumnSortMode.NotSortable;
                }
                grid.ColumnHeadersDefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;

            }
            catch { }
        }

        private bool FindKeyFile()
        {
            string keyFilePath = string.Empty;
            try
            {
                //1 see if setting shows where key file is
                if (_settings != null)
                {
                    keyFilePath = GetSetting(_keyFilePathKey);
                    if (keyFilePath.Length < 1)
                    {
                        keyFilePath = _dataPath;
                    }
                    if (File.Exists(keyFilePath) && keyFilePath.ToLower().EndsWith(".key"))
                    {
                        _keyFilePath = keyFilePath;
                        return true;
                    }
                    bool result = (FileSearchLoop(keyFilePath, ".key", "key", _keyFilePathKey));
                    if (result)
                        _keyFilePath = GetSetting(_keyFilePathKey);
                    return result;
                }
                return false;
            }
            catch (Exception ex)
            {

                ErrorsPopup popup = new ErrorsPopup("There was an error while trying to locate your key file: " + ex.Message, true);
                popup.ShowDialog();
                popup.Dispose();
                return false;
            }
        }

        private bool FindCsvFile(int financeData0InterestRates1)
        {
            string filePath = string.Empty;
            string settingKeyToUse = string.Empty;
            string fileTypeDescriptionToUse = string.Empty;
            switch (financeData0InterestRates1)
            {
                case 0:
                    settingKeyToUse = _lastFinanceDataPathKey;
                    fileTypeDescriptionToUse = "Cumulative Default Probabilities csv";
                    break;
                case 1:
                    settingKeyToUse = _lastInterestRatesPathKey;
                    fileTypeDescriptionToUse = "Interest Rates csv";
                    break;
            }
            try
            {
                //1 see if setting shows where key file is
                if (_settings != null)
                {
                    filePath = GetSetting(settingKeyToUse);
                    if (filePath.Length < 1)
                    {
                        filePath = _dataPath;
                    }
                    bool result = (FileSearchLoop(filePath, ".csv", fileTypeDescriptionToUse, settingKeyToUse));
                    if (result)
                    {
                        if (financeData0InterestRates1 == 0)
                        {
                            _lastFinanceDataPath = GetSetting(settingKeyToUse);
                        }
                        else
                        {
                            _lastInterestRatesPath = GetSetting(settingKeyToUse);
                        }
                    }
                    return result;
                }
                return false;
            }
            catch (Exception ex)
            {
 
                ErrorsPopup popup = new ErrorsPopup("There was an error while trying to locate a " + fileTypeDescriptionToUse + " file: " + ex.Message, true);
                popup.ShowDialog();
                popup.Dispose();
                return false;
            }
        }

        private bool CsvFilePath(int financeData0Interest1,string startingPath)
        {
            string path = startingPath;
            string settingToUse = string.Empty;
            string errorMessage = "There was an error while trying to locate the ";
            try
            {
                //1 see if setting shows where key file is
                if (_settings != null)
                {
                    switch (financeData0Interest1)
                    {
                        case 0:
                            errorMessage += "Finance Data";
                            settingToUse=_lastFinanceDataPathKey;
                            path = GetSetting(settingToUse);
                            break;
                        case 1:
                            errorMessage += "Interest Rates";
                            settingToUse=_lastInterestRatesPathKey;
                            path = GetSetting(settingToUse);
                            break;
                    }

                    if (path.Length < 1)
                    {
                        path = _dataPath;
                    }
                    return FileSearchLoop(path,".csv","csv",settingToUse);
                }
                return false;
            }
            catch (Exception ex)
            {
                errorMessage +=   "file: " + ex.Message;

                ErrorsPopup popup = new ErrorsPopup(errorMessage, true);
                popup.ShowDialog();
                popup.Dispose();
                return false;
            }
        }

        private bool FileSearchLoop(string filePath, string searchPattern, string fileTypeDescription, string settingKeyName)
        {
            string foundPath = string.Empty;
            string dialogFilter = string.Empty;
            switch (fileTypeDescription.ToLower())
            {
                case "key":
                    dialogFilter = _keyFilter;
                    break;
                case "csv":
                    dialogFilter = _csvFilter;
                    break;
                case "rdd":
                    dialogFilter = _rddFilter;
                    break;
            }
            int result = 1; //tryagain
            while (result == 1)
            {
                //result = FileSearch(filePath,"*.csv","csv", ref foundPath); //KeyFileSearch()//0 if success, -1 for quit, 1 for try again
                result = FileSearch(filePath, searchPattern, fileTypeDescription, ref foundPath);
                switch (result)
                {
                    case -1:
                        return false;  //stop trying
                        break;
                    case 0:
                        SaveSetting(settingKeyName, foundPath);
                        return true;
                        break;
                    case 1:
                        bool returnFullPath = fileTypeDescription.ToLower() != "key";
                        result = FileSearch(PromptForFilePath(dialogFilter,searchPattern,returnFullPath),searchPattern,fileTypeDescription,ref foundPath);//prompt user to navigate to it until found
                        if(result==0)
                            SaveSetting(settingKeyName, foundPath);
                        break;
                }
            }
            if (result >=0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        private int FileSearch(string pathToFolder, string searchPattern, string fileTypeDescription,  ref string fullPathToUse)
        {   //return 0 if success, -1 for quit, 1 for try again
            if (pathToFolder.Length < 1)
                return -1;

            //we could be checing a folder, or a path to a file.
            //check if we received a file.
            if(pathToFolder.ToLower().EndsWith(searchPattern.ToLower()))
            {
                //we received a file, check for it.
                if(File.Exists(pathToFolder))
                {
                    fullPathToUse = pathToFolder;
                    return 0;
                }
                else
                {
                    ErrorsPopup popup = new ErrorsPopup("Unable to find " + fileTypeDescription + " at " + pathToFolder + ", please select your " + fileTypeDescription + " file now.", true);
                    popup.ShowDialog();
                    popup.Dispose();
                    return 1; //try again
                }
            }

            //we didn't receive a file, so we must check in the folder for files.
            DirectoryInfo dir = new DirectoryInfo(pathToFolder);
            FileInfo[] files = null;
            try
            {
                files = dir.GetFiles("*" + searchPattern); //"*.csv");
            }
            catch { }            
            if (files == null)
            {
                ErrorsPopup popup = new ErrorsPopup("Unable to find any " + fileTypeDescription + " files at: " + pathToFolder + ", please select your " + fileTypeDescription + " file now.", true);
                popup.ShowDialog();
                popup.Dispose();
                return 1; //try again
            }
            else
            {
                if (files.Length == 1)
                {
                    fullPathToUse = files[0].FullName;
                    return 0; //success
                }
                else if (files.Length > 1)
                {
                    if (fileTypeDescription.ToLower() == "key") //fatal
                    {
                        ErrorsPopup popup = new ErrorsPopup("More than one " + fileTypeDescription + " file was found at " + pathToFolder + ". Please remove one and try again.",true);
                        popup.ShowDialog();
                        popup.Dispose();
                        return -1; //quit
                    }
                    else
                    {
                        ErrorsPopup popup = new ErrorsPopup("More than one " + fileTypeDescription + " file was found at " + pathToFolder + ". Please select one and try again.", true);
                        popup.ShowDialog();
                        popup.Dispose();
                        return 1; //try again
                    }
                }
                else
                {
                    ErrorsPopup popup = new ErrorsPopup("Unable to find your " + fileTypeDescription + " file at: " + pathToFolder + ", please select your " + fileTypeDescription + " file now.", true);
                    popup.ShowDialog();
                    popup.Dispose();
                    return 1;
                }
            }
        }



        private string PromptForFilePath(string filter, string endsWith, bool returnFullPath)
        {
            string keyFilter = filter; // "KEY files|*.key";
            openFileDialog1.Filter = keyFilter;
            openFileDialog1.FilterIndex = 1;
            openFileDialog1.FileName = string.Empty;
            string fileName = string.Empty;
            if (openFileDialog1.ShowDialog() == DialogResult.OK && openFileDialog1.FileName.Length > 0)
            {
                if (File.Exists(openFileDialog1.FileName) && openFileDialog1.FileName.Trim().ToLower().EndsWith(endsWith.ToLower()))     
                {
                    if (!returnFullPath)
                    {
                        string folderOnly = openFileDialog1.FileName.Replace(Utilities.GetFilenameFromPath(openFileDialog1.FileName), "");
                        return folderOnly;
                    }
                    else
                    {
                        return openFileDialog1.FileName;
                    }
                }
            }
            return string.Empty;
        }





        private void frm_sizeChanged(object sender, EventArgs e)
        {
            if (this.Width < 1750 || this.WindowState==FormWindowState.Maximized)
            {
                int widthPad = 50;
                int scrollbarWidth = 20;
                int tabControl1Height = tabControl1.Height;
                tabControl1.Size=new Size(this.Width - widthPad,tabControl1Height);// 680); //750); //710);

                int tabPad = widthPad - 20;

                panel3.Size = new Size(this.Width - widthPad, panel3.Height);
                panel5.Size = new Size(this.Width - widthPad, panel5.Height);
                panel7.Size = new Size(this.Width - widthPad, panel7.Height);
                panel9.Size = new Size(this.Width - widthPad, panel9.Height);

                int gridMaxWidth = 1406;
                int gridLossReservesMaxWidth = 1506;

                if ((tabControl1.Width - 200 < gridMaxWidth))
                    gridMaxWidth=tabControl1.Width - 200;
                if ((tabControl1.Width - 100 < gridLossReservesMaxWidth))
                    gridLossReservesMaxWidth = tabControl1.Width - 100;


                gridNoInsurance.Size = new Size(gridMaxWidth, gridNoInsurance.Height);
                gridWithIns.Size = new Size(gridMaxWidth, gridWithIns.Height);
                gridCreditRiskSavings.Size = new Size(gridMaxWidth, gridCreditRiskSavings.Height);
                gridLossReserves.Size = new Size(gridLossReservesMaxWidth, gridLossReserves.Height);

                gridInterestRates.Size = new Size(this.Width - widthPad - scrollbarWidth, gridInterestRates.Height);
                gridFinanceData.Size = new Size(this.Width - widthPad - scrollbarWidth, gridFinanceData.Height);
            }
        }

        private void RtfSubscript10(Control control)
        {
            RichTextBox rtf = (RichTextBox)control;
            rtf.Text = string.Empty;
            rtf.SelectedRtf =_p10Rtf;
            rtf.Select(0, 4);
        }
        
        private void RtfSubscript20(Control control)
        {
            RichTextBox rtf = (RichTextBox)control;
            rtf.Text = string.Empty;
            rtf.SelectedRtf =_p20Rtf;
            rtf.Select(0,4);
        }

        private void RtfSubscripts(Control control, string controlText, string variation = "")
        {
            RichTextBox rtf = (RichTextBox)control;
            string text = controlText;
            rtf.Text = text;
            if (variation.Length < 1)
            {
                rtf.Select(1, 2);
            }
            else if (variation == "20")
            {
                rtf.Select(2, 2);

            }

            // Get the selected rich text data and selected character data
            string selectedRtf = rtf.SelectedRtf;
            string selectedText = rtf.SelectedText;

            // Now lets insert \super
            selectedRtf = selectedRtf.Insert(selectedRtf.IndexOf(selectedText), "\\sub "); 

            // Now set the Rtf back to the control
            if (variation == "20")
            {
                rtf.Text = string.Empty;
                rtf.SelectedRtf = _p20Rtf;
            }
            else
            {
                rtf.SelectedRtf = selectedRtf;
            }
            rtf.ForeColor = Color.Black;
        }

        
        private bool ValidateNumberEntry( string value, string controlDisplayNane)
        {
            //check for numeric
            value = value.Trim();
            string output = string.Empty;
            double discard = 0;
            if (!Double.TryParse(value, out discard))
                output += value +  " is not a valid number. Please try again." + Environment.NewLine;
            
            //the rest below has been moved to the SErvice layer
            /*
            //replace for commas-do before call this method.
            //olicyCoverageFactor.Text=olicyCoverageFactor.Text.Replace(",", string.Empty);

            //No negatives.
            if (discard < 0)
                output += value + " must not be less than zero. Please try again." + Environment.NewLine;
            
            //no decimals
            if(value.Contains("."))
                output += value + " must not contain decimals. Please try again." + Environment.NewLine;

            //not > 10 digits
            if (value.Length>10)
                output += value + " must not contain more than 10 digits. Please try again." + Environment.NewLine;
            */
            if (output.Length > 0)
            {
                ErrorsPopup popup = new ErrorsPopup("There was a problem in '" + controlDisplayNane + "': " + output, true);
                popup.ShowDialog();
                popup.Dispose();
                return false;
            }
           return true;
        }
        

        private void btnStart_Click(object sender, EventArgs e)
        {
            StartRoutine();
        }

        private void StartRoutine()
        {
            ErrorsInfo.Reset();
            ClearControls();

            string controlDisplayName = "Loan Amount";
            string controlValue = LoanAmount.Text;
            if (!ValidateNumberEntry(controlValue, controlDisplayName))
            {
                return;
            }
            else
            { //add commas
                string addCommas = LoanAmount.Text.Replace(",", string.Empty);
                addCommas = String.Format("{0:N0}", double.Parse(addCommas));
                LoanAmount.Text = addCommas;
            }

            controlDisplayName = "Loan Term";
            controlValue = LoanTerm.Text;
            if (!ValidateNumberEntry(controlValue, controlDisplayName))
            {
                return;
            }
            else
            {
                string addCommas = LoanTerm.Text.Replace(",", string.Empty);
                addCommas = String.Format("{0:N0}", double.Parse(addCommas));
                LoanTerm.Text = addCommas;
            }

            controlDisplayName = "Total Insured Project Savings";
            controlValue = IotalInsuredProjectSavings.Text;
            if (!ValidateNumberEntry(controlValue, controlDisplayName))
            {
                return;
            }
            else
            {
                string addCommas = IotalInsuredProjectSavings.Text.Replace(",", string.Empty);
                addCommas = String.Format("{0:N0}", double.Parse(addCommas));
                IotalInsuredProjectSavings.Text = addCommas;
            }

            olicyCoverageFactor.Text = olicyCoverageFactor.Text.Trim();
            olicyCoverageFactor.Text=olicyCoverageFactor.Text.Replace(",", string.Empty);

            if (gridFinanceData.RowCount < 1)
            {
                ErrorsPopup popup = new ErrorsPopup("Please load Cumulative Default Probabilities on the Finance Data tab and try again.", true);
                popup.ShowDialog();
                popup.Dispose();
                return;
            }
            //--------------------------
            if (gridInterestRates.RowCount < 1)
            {
                ErrorsPopup popup = new ErrorsPopup("Please load Interest Rates on the Finance Data tab and try again.", true);
                    popup.ShowDialog();
                    popup.Dispose();
                return;
            }
            else
            {
                if (!PopulateFinanceDataInterestRates(ref _dataHolder))
                {
                    ErrorsPopup popup = new ErrorsPopup(ErrorsInfo.GetAllErrors(), false);
                    popup.ShowDialog();
                    popup.Dispose();
                    return;
                }
            }
            try
            {
                if (_lastFinanceDataPath.Trim().Length > 0)
                {
                    SaveSetting(_lastFinanceDataPathKey, _lastFinanceDataPath);
                }
            }
            catch { }
            try
            {
                if (_lastInterestRatesPath.Trim().Length > 0)
                {

                        SaveSetting(_lastInterestRatesPathKey, _lastInterestRatesPath);

                }
            }
            catch { }
            
            try
            {
                if (_esdDataPath.Trim().Length > 0)
                {
                        SaveSetting(_esdDataPathKey, _esdDataPath.Trim());

                }
            }
            catch { }

            lblDateTime.Text = DateTime.Now.ToString();
            bool success = PopulateNewDataHolder();
            if (!PopulateFinanceDataInterestRates(ref _dataHolder))
            {
 
                ErrorsPopup popup = new ErrorsPopup(ErrorsInfo.GetAllErrors(), false);
                    popup.ShowDialog();
                    popup.Dispose();
                return;
            }

            if (success)
            {
                //call the service
                ServiceManager serviceManager = new ServiceManager();
                string serviceManagerValidationErrors = string.Empty;


                if (!serviceManager.ValidateInput(_dataHolder,Enums.PeriodTypes.YEAR,olicyCoverageFactor.Text.Trim(), ref serviceManagerValidationErrors))
                {
                    ErrorsPopup popup = new ErrorsPopup(serviceManagerValidationErrors, true);
                    popup.ShowDialog();
                    popup.Dispose();

                    return;
                }
                _dataHolder.CoverageRatio = double.Parse(olicyCoverageFactor.Text) / 100; //convert to a real percent
                success = serviceManager.Analyze(_dataHolder, _keyFilePath, Enums.PeriodTypes.YEAR,_keyEncryptionKey,_rfcEncryptionKey,_keyEncryptionESD,_rfcEncryptionESD,_keyEncryptionInterestRates,_rfcEncryptionInterestRates);


                
                if (success)
                {
                    success = PopulateControls();
                }
            }
            if(!success)
            {
                string errorCheck = ErrorsInfo.GetAllErrors();
                if (errorCheck.Contains("Error in FinanceDataCumulativeDefaultProbabilities: Could not find row") &&
                   _dataHolder.Nterm > gridFinanceData.RowCount - 1)  
                {
                    errorCheck = "Error: you might have more months in the Loan Term Years box (" + LoanTerm.Text + ") than the count of rows in Finance Data's Cumulative Default Probabilities (" + (gridFinanceData.RowCount-1).ToString() + ")." + Environment.NewLine + Environment.NewLine +  errorCheck;
                }
                ErrorsPopup popup = new ErrorsPopup(errorCheck, true);
                popup.ShowDialog();
                popup.Dispose();

            }
            else
            {
                MessageBox.Show("           Complete            ", _msgBoxCaption);                    
           }
        }

        private bool PopulateFinanceDataInterestRates(ref DataHolder data)
        {
            try{
                double[] values = new double[15];
                for(int counter = 0; counter < gridInterestRates.ColumnCount;counter++)
                {
                    values[counter]=(double)gridInterestRates[counter,0].Value;
                }
                _dataHolder.FinanceDataInterestRates=values;
                return true;
            }
            catch(Exception ex)
            {
                ErrorsInfo.AddErrorInfo("PopulateFinanceDataInterestRates",ex.Message);
                return false;
            }
        }

        private void ClearControls()
        {
            //if exceptions is 1 then don't clear the arrow boxes
            UninsuredAnnualLoanPayment.Text = string.Empty;
            InsuredEffectiveLoanPayment.Text = string.Empty;
            SavingsFromLoanPaymentReduction.Text = string.Empty;
            P90.Text = string.Empty;
            P80.Text = string.Empty;
            P70.Text = string.Empty;
            P60.Text = string.Empty;
            P50.Text = string.Empty;
            P40.Text = string.Empty;
            P30.Text = string.Empty;
            P20.Text = string.Empty;
            P10.Text = string.Empty;
            gridNoInsurance.DataSource = Utilities.GetReadOnlyDataTable(false); 
            gridWithIns.DataSource=Utilities.GetReadOnlyDataTable(false);
            gridCreditRiskSavings.DataSource = Utilities.GetReadOnlyDataTable(false);
            gridLossReserves.DataSource=Utilities.GetReadOnlyDataTable(true);

        }

        private bool PopulateControls()
        {
            ClearControls();
            try
            {
                double temp = -99999;
                if (_dataHolder.UninsuredLoanPayment > temp)
                    UninsuredAnnualLoanPayment.Text =String.Format("{0:N0}",  Math.Round(_dataHolder.UninsuredLoanPayment, 0));
                if (_dataHolder.InsuredEffectiveLoanPayment > temp)
                    InsuredEffectiveLoanPayment.Text =String.Format("{0:N0}",   Math.Round(_dataHolder.InsuredEffectiveLoanPayment,0));
                if (_dataHolder.SavingsFromLoanPaymentReduction > temp)
                    SavingsFromLoanPaymentReduction.Text = String.Format("{0:N0}", Math.Round(_dataHolder.SavingsFromLoanPaymentReduction, 0));
                P90.Text = String.Format("{0:N0}", Math.Round(_dataHolder.ProjectSavingsDistribution[0],0));
                P80.Text = String.Format("{0:N0}", Math.Round(_dataHolder.ProjectSavingsDistribution[1],0));
                P70.Text = String.Format("{0:N0}", Math.Round(_dataHolder.ProjectSavingsDistribution[2],0));
                P60.Text = String.Format("{0:N0}", Math.Round(_dataHolder.ProjectSavingsDistribution[3],0));
                P50.Text = String.Format("{0:N0}", Math.Round(_dataHolder.ProjectSavingsDistribution[4],0));
                P40.Text = String.Format("{0:N0}", Math.Round(_dataHolder.ProjectSavingsDistribution[5],0));
                P30.Text = String.Format("{0:N0}", Math.Round(_dataHolder.ProjectSavingsDistribution[6],0));
                P20.Text = String.Format("{0:N0}", Math.Round(_dataHolder.ProjectSavingsDistribution[7],0));
                P10.Text = String.Format("{0:N0}", Math.Round(_dataHolder.ProjectSavingsDistribution[8], 0));

                GridPercentPopulateWithList(gridNoInsurance,_dataHolder.ProjectSavingsCreditEnhancementNoInsurance , false);
                GridPercentPopulateWithList(gridWithIns, _dataHolder.ProjectSavingsCreditEnhancementWithInsurance, false);
                GridCurrencyPopulateWithList(gridCreditRiskSavings, _dataHolder.CreditRiskSavings, false);
                GridCurrencyPopulateWithList(gridLossReserves, _dataHolder.LossReserves, true);

                gridNoInsurance.ClearSelection();
                gridWithIns.ClearSelection();
                gridCreditRiskSavings.ClearSelection();
                gridLossReserves.ClearSelection();
                
                lblNoIns.Text = CreditRatingInsured.Text +  " >";
                lblWithIns.Text = CreditRatingInsured.Text + " >";
                lblCreditRisk.Text = CreditRatingInsured.Text + " >";
                //lblLossReserves.Text = CreditRatingInsured.Text + " >";
                return true;
            }
            catch (Exception ex)
            {
                ErrorsInfo.AddErrorInfo("PopulateControls", ex.Message);
                return false;
            }
        }



        private bool PopulateNewDataHolder()
        {
            string errorAt = string.Empty;
            try
            {
                _dataHolder = new DataHolder(); //wipe out any old data
                _dataHolder.ESDDataPath = _esdDataPath;

                PopulateCumulativeDefaultProbabilities();
                
                
                errorAt = "Path to Project Savings Distribution File";
                if (_dataHolder.ESDDataPath.Trim().Length < 1)
                    throw new Exception("Please select a Project Savings Distribution File (.rdd)");

                errorAt = "Loan Amount";
                _dataHolder.LoanAmt=double.Parse(LoanAmount.Text);
                errorAt = "Loan Term";
                _dataHolder.Nterm=Int32.Parse(LoanTerm.Text.Replace(",",""));
                errorAt = "Credit Rating of the Insured";
                _dataHolder.SelectedInsuredCreditRating=CreditRatingInsured.Text;
                errorAt = "Credit Rating of the Insurer";
                _dataHolder.SelectedInsurerCreditRating=CreditRatingInsurer.Text;

                errorAt = "Total Insured Project Savings";
                _dataHolder.TotalInsuredProjectSavings=double.Parse(IotalInsuredProjectSavings.Text);
                return true;
            }
            catch(System.InvalidCastException exCast)
            {
                ErrorsPopup popup = new ErrorsPopup(errorAt + " is not a valid entry.", true);
                    popup.ShowDialog();
                    popup.Dispose();
                return false;
            }
            catch(Exception ex)
            {
                ErrorsInfo.AddErrorInfo("PopulateNewDataHolder",ex.Message);
                return false;
            }
        }

        private bool PopulateCumulativeDefaultProbabilities()
        {
            try
            {
                //FinanceData is the DataSet
                //_dataHolder.FinanceDataCumulativeDefaultProbabilities is instantiation of the dataset.
                //_dataHolder.FinanceDataCumulativeDefaultProbabilities.CumulativeDefaultProbabilities is the datatable
                FinanceData dataSet = (FinanceData)gridFinanceData.DataSource;
                DataTable grid = dataSet.Tables[0];
                if (grid.Rows.Count < 1)
                {
                    ErrorsInfo.AddErrorInfo("PopulateCumulativeDefaultProbabilities", "No rows found in DataSource for gridFinanceData");
                    return false;
                }
                for (int row = 0; row < grid.Rows.Count; row++)
                {
                    DataRow newRow = _dataHolder.FinanceDataCumulativeDefaultProbabilities.CumulativeDefaultProbabilities.NewRow();
                    for (int col = 0; col < grid.Columns.Count; col++)
                    {
                        newRow[col] = grid.Rows[row][col];
                    }
                    _dataHolder.FinanceDataCumulativeDefaultProbabilities.CumulativeDefaultProbabilities.Rows.Add(newRow);
                }

                return true;
            }
            catch (Exception ex)
            {
                ErrorsInfo.AddErrorInfo("PopulateCumulativeDefaultProbabilities", ex.Message);
                return false;
            }
        }


        private void btnLoad_Click(object sender, EventArgs e)
        {
            ErrorsInfo.Reset(); 
            try
            {
                openFileDialog1.Filter = _csvFilter;
                openFileDialog1.FilterIndex = 1;
                openFileDialog1.FileName = string.Empty;
                openFileDialog1.InitialDirectory = _dataPath;
                string fileName = string.Empty;
                if ( openFileDialog1.ShowDialog()==DialogResult.OK &&  openFileDialog1.FileName.Length > 0)
                {
                    fileName = openFileDialog1.FileName;
                    if (LoadFinanceDataGrid(fileName))
                    {
                        _lastFinanceDataPath = fileName;
                        try
                        {
                            SaveSetting(_lastFinanceDataPathKey, _lastFinanceDataPath);
                        }
                        catch { }
                    }
                }
                else
                {
                    return;
                }                
            }
            catch (Exception ex)
            {
                ErrorsInfo.AddErrorInfo("btnLoad", ex.Message);
            }
            finally
            {
                if (ErrorsInfo.GetAllErrors().Length > 0)
                {
                    ErrorsPopup popup = new ErrorsPopup(ErrorsInfo.GetAllErrors(), false);
                    popup.ShowDialog();
                    popup.Dispose();
                }
                 
            }
        }

        private bool LoadFinanceDataGrid(string path)
        {
            try
            {
                ServiceManager serviceManager = new ServiceManager();
               
                if (path.Length < 2 || !File.Exists(path))
                {
                    return false;
                }
                if (!serviceManager.LoadFinanceData(ref _dataHolder, path))
                {
                    return false;
                }

                gridFinanceData.AutoGenerateColumns = true;
                gridFinanceData.DataSource = _dataHolder.FinanceDataCumulativeDefaultProbabilities;
                gridFinanceData.DataMember = "CumulativeDefaultProbabilities"; // table name you need to show
                //Done elsewhere: SaveSetting(LastFinanceDataPath, path);                
                string fileName = Utilities.GetFilenameFromPath(path);
                string[] nameParts = fileName.Split('.');
                string header = string.Empty;
                for (int counter = 0; counter < nameParts.Length - 1; counter++)
                {
                    header += nameParts[counter] + ".";
                }
                header = header.Remove(header.Length - 1);
                lblFinanceDataHeader.Text = header;
                return true;
            }
            catch (Exception ex)
            {
                ErrorsInfo.AddErrorInfo("LoadFinanceDataGrid", ex.Message);
                return false;
            }
        }

         private void btnSave_Click(object sender, EventArgs e)
        {
            ErrorsInfo.Reset(); 
             saveFileDialog1.Filter = _csvFilter;
            saveFileDialog1.FilterIndex = 1;
            saveFileDialog1.InitialDirectory = _dataPath;
            saveFileDialog1.FileName = string.Empty;

            if (saveFileDialog1.ShowDialog() == DialogResult.OK && saveFileDialog1.FileName.Length > 0)
            {
                string fileName = saveFileDialog1.FileName;
                FinanceData financeData = (FinanceData)gridFinanceData.DataSource;
                ServiceManager serviceManager = new ServiceManager();
                if (!serviceManager.SaveFinanceData(ref financeData, fileName))
                {
                    ErrorsPopup popup = new ErrorsPopup(ErrorsInfo.GetAllErrors(), true);
                    popup.ShowDialog();
                    popup.Dispose();
                }
                else
                {
                    _lastFinanceDataPath = saveFileDialog1.FileName;
                    try
                    {
                        SaveSetting(_lastFinanceDataPathKey, _lastFinanceDataPath);
                    }
                    catch { }
                }
            }
        }

          private void GridCurrencyPopulateWithList(System.Windows.Forms.DataGridView grid, List<double?> list, bool addBMinus)
        {
            DataTable temp = Utilities.GetReadOnlyDataTableForBindingShowingCurrency(list, addBMinus);
            grid.DataSource = temp;
        }

        private void GridPercentPopulateWithList(System.Windows.Forms.DataGridView grid, List<double?> list, bool addBMinus)
        {
            DataTable temp = Utilities.GetReadOnlyDataTableForBindingShowingPercent(list, addBMinus);
            grid.DataSource = temp;
        }

        private void label28_Click(object sender, EventArgs e)
        {

        }


        private void PrepopulateAnnual()
        {
            string intRatePath =  @"C:\CreditRiskModelAnnualInterestRates - Decrypted.csv";
            LoadInterestRates(intRatePath);
            LoanAmount.Text = "8000000";
            LoanTerm.Text = "5";
            IotalInsuredProjectSavings.Text = "4000000";
            olicyCoverageFactor.Text = "100";
            CreditRatingInsured.SelectedIndex = 6;
            CreditRatingInsurer.SelectedIndex = 14;
            btnProjectSavingsDistributionFile.Text = Utilities.GetFilenameFromPath(_esdDataPath);
            string path = @"C:\Cumulative Default Probabilities Annual - Decrypted.csv";
            LoadFinanceDataGrid(path);
        }

        private void btnPrepopulateAnnualAndRun_Click(object sender, EventArgs e)
        {
            PrepopulateAnnual();
        }

        private void btnLoadInterestRates_Click(object sender, EventArgs e)
        {
            ErrorsInfo.Reset(); 
            try
            {
                openFileDialog1.Filter = _csvFilter;
                openFileDialog1.FilterIndex = 1;
                openFileDialog1.FileName = string.Empty;
                openFileDialog1.InitialDirectory = _dataPath;
                string fileName = string.Empty;
                if (openFileDialog1.ShowDialog() == DialogResult.OK && openFileDialog1.FileName.Length > 0)
                {
                    fileName = openFileDialog1.FileName;
                    if (LoadInterestRates(fileName))
                    {
                        _lastInterestRatesPath = fileName;
                        try
                        {

                                SaveSetting(_lastInterestRatesPathKey, _lastInterestRatesPath);

                        }
                        catch { }
                    }
                    else
                    {
                        return;
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorsInfo.AddErrorInfo("btnLoadInterestRates", ex.Message);
            }
            finally
            {
                if (ErrorsInfo.GetAllErrors().Length > 0)
                {
                    ErrorsPopup popup = new ErrorsPopup(ErrorsInfo.GetAllErrors(), true);
                    popup.ShowDialog();
                    popup.Dispose();
                }
            }
        }

        private bool LoadInterestRates(string path)
        {
            try
            {
                ServiceManager serviceManager = new ServiceManager();               
                if (path.Length < 2 ||  !File.Exists(path))
                {
                    return false;
                }
                if (!serviceManager.LoadFinanceDataInterestRates(ref _dataHolder, path))
                {
                    return false;
                }
                gridInterestRates.AutoGenerateColumns = true;
                PopulateGridWithArray(ref gridInterestRates, _dataHolder.FinanceDataInterestRates);
                return true;
            }
            catch (Exception ex)
            {
                ErrorsInfo.AddErrorInfo("LoadInterestRates", ex.Message);
                return false;
            }
        }

        private void PopulateGridWithArray(ref System.Windows.Forms.DataGridView grid, double[] array)
        {
            DataTable temp = Utilities.GetReadOnlyDataTableForBinding(array.ToList(), true);
            grid.DataSource = temp;
        }

        private void btnSaveInterestRates_Click(object sender, EventArgs e)
        {
            ErrorsInfo.Reset(); 
            saveFileDialog1.Filter = _csvFilter;
            saveFileDialog1.FilterIndex = 1;
            saveFileDialog1.InitialDirectory = _dataPath;
            saveFileDialog1.FileName = string.Empty;
            if (saveFileDialog1.ShowDialog() == DialogResult.OK && saveFileDialog1.FileName.Length > 0)
            {
                string fileName = saveFileDialog1.FileName;
                if (!PopulateFinanceDataInterestRates(ref _dataHolder))
                    return;
                ServiceManager serviceManager = new ServiceManager();
                if (!serviceManager.SaveFinanceDataInterestRates(ref _dataHolder, fileName))
                {
                    ErrorsPopup popup = new ErrorsPopup(ErrorsInfo.GetAllErrors(), true);
                    popup.ShowDialog();
                    popup.Dispose();
                }
                else
                {
                    _lastInterestRatesPath = fileName;
                    try
                    {

                            SaveSetting(_lastInterestRatesPathKey, _lastInterestRatesPath);

                    }
                    catch { }
                }
            }
        }

        private void CreditRatingInsured_SelectedIndexChanged(object sender, EventArgs e)
        {
            string change = CreditRatingInsured.Text;
            ClearControls();
            CreditRatingInsured.Text = change;
            lblNoIns.Text = CreditRatingInsured.Text + " >";
            lblWithIns.Text = CreditRatingInsured.Text + " >";
            lblCreditRisk.Text = CreditRatingInsured.Text + " >";
        }

        private void button5_Click(object sender, EventArgs e)
        {
         
        }

        private void HideHorizontalScrollbar()
        {
            this.HorizontalScroll.Maximum = 0;
            this.HorizontalScroll.Enabled = false;
            this.HorizontalScroll.Visible = false;
            this.AutoScroll = false;
            this.VerticalScroll.Visible = true;
            this.AutoScroll = true;
        }


        private void button6_Click(object sender, EventArgs e)
        {
            MessageBox.Show(gridNoInsurance.Width.ToString());

        }

        private string GetSetting(string key)
        {
            string settingValue = string.Empty;
            try
            {
                if (_settings.Keys.Count < 1)
                {
                    if (!XmlRead())
                    {
                        XmlWriteFreshEntries();
                        XmlRead();
                    }
                }
                try
                {
                    settingValue= _settings[key];
                }
                catch (Exception noSetting)
                {
                    //setting not read from xml file, need to add it to xml file.
                    _settings.Add(key, string.Empty);
                    XmlWrite();
                }
                return settingValue;
            }
            catch (Exception ex)
            {
                ErrorsInfo.AddErrorInfo("GetSetting(" + key + ")", ex.Message);
                return string.Empty;
            }
        }
        private bool SaveSettings()
        {
            try
            {
                XmlWrite();
                return true;
            }catch(Exception ex)
            {
                ErrorsInfo.AddErrorInfo("SaveSettings", ex.Message);
                    return false;
            }
        }

        private void SaveSetting(string keyName, string value)
        {
            _settings[keyName] = value;
            SaveSettings();
        }

        private void btnProjectSavingsDistributionFile_Click(object sender, EventArgs e)
        {
            ErrorsInfo.Reset();
            openFileDialog1.Filter = _rddFilter;
            openFileDialog1.FilterIndex = 1;
            openFileDialog1.FileName = string.Empty;
            openFileDialog1.InitialDirectory = _dataPath;
            
            if (openFileDialog1.ShowDialog()==DialogResult.OK &&   openFileDialog1.FileName.Length > 0)
            {
                string fileName = openFileDialog1.FileName;
                _esdDataPath = fileName;
                SaveSetting(_esdDataPathKey, _esdDataPath);
                btnProjectSavingsDistributionFile.Text = Utilities.GetFilenameFromPath(fileName);
            }
        }

        private void LoanAmount_TextChanged(object sender, EventArgs e)
        {
            string change = LoanAmount.Text;
            ClearControls();
            LoanAmount.Text = change;
        }

        private void LoanTerm_TextChanged(object sender, EventArgs e)
        {
            string change = LoanTerm.Text;
            ClearControls();
            LoanTerm.Text = change;
        }

        private void IotalInsuredProjectSavings_TextChanged(object sender, EventArgs e)
        {
            string change = IotalInsuredProjectSavings.Text;
            ClearControls();
            IotalInsuredProjectSavings.Text = change;
        }

        private void olicyCoverageFactor_TextChanged(object sender, EventArgs e)
        {
            string change = olicyCoverageFactor.Text;
            ClearControls();
            olicyCoverageFactor.Text = change;
        }

        private void CreditRatingInsurer_SelectedIndexChanged(object sender, EventArgs e)
        {
            string change = CreditRatingInsurer.Text;
            ClearControls();
            CreditRatingInsurer.Text = change;
        }


        private void cboPeriodChangedRoutine()
        {
 
        }

        private void btnPrint_Click(object sender, EventArgs e)
        {
        }
        private void PrintRoutine()
        {
            ErrorsInfo.Reset();
            CaptureScreen();
           printPreviewDialog1.ShowDialog();
        }


        Bitmap memoryImage;

        private void CaptureScreen()
        {
            Graphics myGraphics = this.CreateGraphics();
            Size s = this.Size;
            memoryImage = new Bitmap(s.Width, s.Height, myGraphics);
            Graphics memoryGraphics = Graphics.FromImage(memoryImage);
            memoryGraphics.CopyFromScreen(this.Location.X, this.Location.Y, 0, 0, s);
        }
        private void printDocument1_PrintPage(object sender, System.Drawing.Printing.PrintPageEventArgs e)
        {
            //try resize
            System.Drawing.Image i = new Bitmap(memoryImage, 700, 700);

            e.Graphics.DrawImage(i, 0, 0);
        }

        private void btnProjectSavingsDistributionFile_MouseHover(object sender, EventArgs e)
        {
            toolTip1.SetToolTip(btnProjectSavingsDistributionFile, _esdDataPath);
        }

        private void rtf10_TextChanged(object sender, EventArgs e)
        {
            RtfSubscript10(rtf10);
        }

        private void rtf20_TextChanged(object sender, EventArgs e)
        {
            RtfSubscript20(rtf20);
        }

        private void gridNoInsurance_Click(object sender, EventArgs e)
        {
            gridWithIns.ClearSelection();
            gridCreditRiskSavings.ClearSelection();
            gridLossReserves.ClearSelection();
        }

        private void gridWithIns_Click(object sender, EventArgs e)
        {
            gridNoInsurance.ClearSelection();
            gridCreditRiskSavings.ClearSelection();
            gridLossReserves.ClearSelection();
        }

        private void gridCreditRiskSavings_Click(object sender, EventArgs e)
        {
            gridNoInsurance.ClearSelection();
            gridWithIns.ClearSelection();
            gridLossReserves.ClearSelection();
        }

        private void gridLossReserves_Click(object sender, EventArgs e)
        {
            gridNoInsurance.ClearSelection();
            gridNoInsurance.ClearSelection();
            gridCreditRiskSavings.ClearSelection();
        }

        private void btnDataDump_Click(object sender, EventArgs e)
        {

            saveFileDialog1.FileName = string.Empty;
            saveFileDialog1.Filter="XML-File | *.xml";
            saveFileDialog1.InitialDirectory = _dataPath;
            if ( saveFileDialog1.ShowDialog() == DialogResult.OK &&    saveFileDialog1.FileName.Trim().Length > 0)
            {
                System.Xml.Serialization.XmlSerializer x = new System.Xml.Serialization.XmlSerializer(_dataHolder.GetType());
                string fileName = saveFileDialog1.FileName;
                if (!fileName.ToUpper().EndsWith(".XML"))
                    fileName += ".xml";
                using (System.Xml.XmlWriter writer = System.Xml.XmlWriter.Create(fileName))
                {
                    x.Serialize(writer, _dataHolder);
                }
            }
        }


        private bool XmlRead()
        {
            try
            {
                _settings.Clear();
                string element = string.Empty;
                string value = string.Empty;
                if (!File.Exists(_settingsPath))
                    return false;
                using (XmlReader reader = XmlReader.Create(_settingsPath))
                {
                    while (reader.Read())
                    {
                        switch (reader.NodeType)
                        {
                            case XmlNodeType.Element:
                                if (element.Length > 0 && element != "HSBCreditRiskModel")
                                {
                                    _settings.Add(element, value);
                                }
                                element = reader.Name;
                                value = "";
                                break;
                            case XmlNodeType.Text:
                                value = reader.Value;
                                _settings.Add(element, reader.Value);
                                element = string.Empty;
                                break;
                            case XmlNodeType.XmlDeclaration:
                            case XmlNodeType.ProcessingInstruction:
                                break;
                            case XmlNodeType.Comment:
                                break;
                            case XmlNodeType.EndElement:
                                break;
                        }
                    }
                    if (element.Length > 0)
                    {
                        _settings.Add(element, value);
                    }
                }
                return true;
            }
            catch (Exception ex)
            {
                ErrorsInfo.AddErrorInfo("XmlRead", ex.Message);
                return false;
            }
        }
        private void XmlWrite()
        {
            StreamWriter streamWriter = null;
            try
            {
                streamWriter = new StreamWriter(_settingsPath);
                XmlWriterSettings settings = new XmlWriterSettings();
                settings.Indent = true;
                settings.IndentChars = "\t";
                using (XmlWriter writer = XmlWriter.Create(streamWriter, settings))
                {
                    writer.WriteStartElement("HSBCreditRiskModel");

                    foreach (KeyValuePair<string, string> pair in _settings)
                    {
                        writer.WriteElementString(pair.Key, pair.Value);
                    }
                    writer.WriteEndElement();
                    writer.Flush();
                }
            }
            catch (Exception ex)
            {
                ErrorsInfo.AddErrorInfo("XmlWrite", ex.Message);
            }
            finally
            {
                streamWriter.Close();
            }
        }
        private void XmlWriteFreshEntries()
        {
            StreamWriter streamWriter = null;
            try
            {
                streamWriter = new StreamWriter(_settingsPath);
                XmlWriterSettings settings = new XmlWriterSettings();
                settings.Indent = true;
                settings.IndentChars = "\t";
                using (XmlWriter writer = XmlWriter.Create(streamWriter, settings))
                {
                    writer.WriteStartElement("HSBCreditRiskModel");
                    writer.WriteElementString(_keyFilePathKey,"");
                    writer.WriteElementString(_lastFinanceDataPathKey, _dataPath + @"\CumulativeDefaultProbabilities.csv");
                    writer.WriteElementString(_lastInterestRatesPathKey, _dataPath + @"\AnnualInterestRates.csv");
                    string[] files = Directory.GetFiles(_dataPath, "*.rdd");
                    string esdFilePath = string.Empty;
                    if (files.Count() == 1)
                    {
                        esdFilePath = files[0];
                    }
                    writer.WriteElementString(_esdDataPathKey, esdFilePath);
                    writer.WriteEndElement();
                    writer.Flush();
                }
            }
            catch (Exception ex)
            {
                ErrorsInfo.AddErrorInfo("XmlWriteFreshEntries", ex.Message);
            }
            finally
            {
                streamWriter.Close();
            }
        }

        private void Quit()
        {
            System.Environment.Exit(0);
        }

        enum ControlToValidate
        {
            LoanAmount,
            LoanTerm,
            TotalInsuredProjectSavings,
            CreditRatingInsured,
            CreditRatingInsurer,
            GridEntry
        };

        private void Main_Load(object sender, EventArgs e)
        {

        }

        private void tabPage1_Click(object sender, EventArgs e)
        {

        }


        private void DrawCenteredCboBoxItems(object sender, DrawItemEventArgs e)
        {
            //thanks to modiX at http://stackoverflow.com/questions/11817062/align-text-in-combobox
            //set DrawStyle to OwnerDrawFixed
            ComboBox cbx = sender as ComboBox;
            if (cbx != null)
            {
                // Always draw the background
                e.DrawBackground();

                // Drawing one of the items?
                if (e.Index >= 0)
                {
                    // Set the string alignment.  Choices are Center, Near and Far
                    StringFormat sf = new StringFormat();
                    sf.LineAlignment = StringAlignment.Center;
                    sf.Alignment = StringAlignment.Center;

                    // Set the Brush to ComboBox ForeColor to maintain any ComboBox color settings
                    // Assumes Brush is solid
                    Brush brush = new SolidBrush(cbx.ForeColor);

                    // If drawing highlighted selection, change brush
                    if ((e.State & DrawItemState.Selected) == DrawItemState.Selected)
                        brush = SystemBrushes.HighlightText;

                    // Draw the string
                    e.Graphics.DrawString(cbx.Items[e.Index].ToString(), cbx.Font, brush, e.Bounds, sf);
                }
            }
        }


        private void panel2_Paint(object sender, PaintEventArgs e)
        {

        }



        private void rtf90_TextChanged(object sender, EventArgs e)
        {
            rtf90.Text = string.Empty;
            RtfSubscripts(rtf90, "P90 >");
        }

        private void rtf80_TextChanged(object sender, EventArgs e)
        {
            rtf80.Text = string.Empty;
            RtfSubscripts(rtf80, "P80 >");      
        }

        private void rtf70_TextChanged(object sender, EventArgs e)
        {
            rtf70.Text = string.Empty;
            RtfSubscripts(rtf70, "P70 >");  
        }

        private void rtf60_TextChanged(object sender, EventArgs e)
        {
            rtf60.Text = string.Empty;
            RtfSubscripts(rtf60, "P60 >");  
        }

        private void rtf50_TextChanged(object sender, EventArgs e)
        {
            rtf50.Text = string.Empty;
            RtfSubscripts(rtf50, "P50 >"); 
        }

        private void rtf40_TextChanged(object sender, EventArgs e)
        {
            rtf40.Text = string.Empty;
            RtfSubscripts(rtf40, "P40 >");    
        }

        private void rtf30_TextChanged(object sender, EventArgs e)
        {
            rtf30.Text = string.Empty;
            RtfSubscripts(rtf30, "P30 >");     
        }

        private void picStart_Click(object sender, EventArgs e)
        {
            StartRoutine();
        }

        private void picPrint_Click(object sender, EventArgs e)
        {
            PrintRoutine();
        }

        private void tabControl1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void HeaderColor(int tabPageNumber)
        {

        }


    }
}
