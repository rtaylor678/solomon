﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Solomon.CreditRiskModel.Presentation2
{
    public partial class ErrorsPopup : Form
    {
        private bool _showDetails = false;
        public ErrorsPopup(string messages, bool userError)
        {
            InitializeComponent();
            textBox1.Text = messages;
            _showDetails = userError;
            DetailsRoutine();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            _showDetails = !_showDetails;            
            DetailsRoutine();
        }
        private void DetailsRoutine()
        {              
            if (_showDetails)
            {
                label2.Visible = false;
                this.Height = 434;
                textBox1.Visible = true;
                button1.Text = "Hide Details";
            }
            else
            {
                label2.Visible = true;
                this.Height = 176;
                textBox1.Visible = false ;
                button1.Text = "Show Details";
            }    
        }
    }
}
