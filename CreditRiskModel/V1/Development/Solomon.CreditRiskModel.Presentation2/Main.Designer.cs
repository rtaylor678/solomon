﻿namespace Solomon.CreditRiskModel.Presentation2
{
    partial class Main
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle9 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle10 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle11 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle12 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Main));
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.panel9 = new System.Windows.Forms.Panel();
            this.label9 = new System.Windows.Forms.Label();
            this.label26 = new System.Windows.Forms.Label();
            this.label28 = new System.Windows.Forms.Label();
            this.gridLossReserves = new System.Windows.Forms.DataGridView();
            this.panel7 = new System.Windows.Forms.Panel();
            this.label6 = new System.Windows.Forms.Label();
            this.label27 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.lblCreditRisk = new System.Windows.Forms.Label();
            this.gridCreditRiskSavings = new System.Windows.Forms.DataGridView();
            this.panel5 = new System.Windows.Forms.Panel();
            this.label21 = new System.Windows.Forms.Label();
            this.lblWithIns = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.gridWithIns = new System.Windows.Forms.DataGridView();
            this.panel3 = new System.Windows.Forms.Panel();
            this.label20 = new System.Windows.Forms.Label();
            this.lblNoIns = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.gridNoInsurance = new System.Windows.Forms.DataGridView();
            this.panel2 = new System.Windows.Forms.Panel();
            this.rtf10 = new System.Windows.Forms.RichTextBox();
            this.rtf20 = new System.Windows.Forms.RichTextBox();
            this.rtf30 = new System.Windows.Forms.RichTextBox();
            this.rtf40 = new System.Windows.Forms.RichTextBox();
            this.rtf50 = new System.Windows.Forms.RichTextBox();
            this.rtf60 = new System.Windows.Forms.RichTextBox();
            this.rtf70 = new System.Windows.Forms.RichTextBox();
            this.rtf80 = new System.Windows.Forms.RichTextBox();
            this.rtf90 = new System.Windows.Forms.RichTextBox();
            this.P10 = new System.Windows.Forms.TextBox();
            this.P20 = new System.Windows.Forms.TextBox();
            this.P30 = new System.Windows.Forms.TextBox();
            this.P40 = new System.Windows.Forms.TextBox();
            this.P50 = new System.Windows.Forms.TextBox();
            this.P60 = new System.Windows.Forms.TextBox();
            this.P70 = new System.Windows.Forms.TextBox();
            this.P80 = new System.Windows.Forms.TextBox();
            this.P90 = new System.Windows.Forms.TextBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.label31 = new System.Windows.Forms.Label();
            this.SavingsFromLoanPaymentReduction = new System.Windows.Forms.TextBox();
            this.InsuredEffectiveLoanPayment = new System.Windows.Forms.TextBox();
            this.UninsuredAnnualLoanPayment = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.lblUninsuredLoanPayment = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.cboPeriod = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.lblLoanTerm = new System.Windows.Forms.Label();
            this.CreditRatingInsurer = new System.Windows.Forms.ComboBox();
            this.CreditRatingInsured = new System.Windows.Forms.ComboBox();
            this.olicyCoverageFactor = new System.Windows.Forms.TextBox();
            this.IotalInsuredProjectSavings = new System.Windows.Forms.TextBox();
            this.LoanTerm = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.btnProjectSavingsDistributionFile = new System.Windows.Forms.Button();
            this.label8 = new System.Windows.Forms.Label();
            this.LoanAmount = new System.Windows.Forms.TextBox();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.lblFinanceDataHeader = new System.Windows.Forms.Label();
            this.btnSaveInterestRates = new System.Windows.Forms.Button();
            this.btnLoadInterestRates = new System.Windows.Forms.Button();
            this.gridInterestRates = new System.Windows.Forms.DataGridView();
            this.gridFinanceData = new System.Windows.Forms.DataGridView();
            this.btnSave = new System.Windows.Forms.Button();
            this.btnLoad = new System.Windows.Forms.Button();
            this.label33 = new System.Windows.Forms.Label();
            this.label32 = new System.Windows.Forms.Label();
            this.btnDataDump = new System.Windows.Forms.Button();
            this.btnPrint = new System.Windows.Forms.Button();
            this.button6 = new System.Windows.Forms.Button();
            this.btnPrepopulateAnnualAndRun = new System.Windows.Forms.Button();
            this.btnStart = new System.Windows.Forms.Button();
            this.lblDateTime = new System.Windows.Forms.Label();
            this.lblLicensedTo = new System.Windows.Forms.Label();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.printDocument1 = new System.Drawing.Printing.PrintDocument();
            this.printPreviewDialog1 = new System.Windows.Forms.PrintPreviewDialog();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.picStart = new System.Windows.Forms.PictureBox();
            this.picPrint = new System.Windows.Forms.PictureBox();
            this.saveFileDialog1 = new System.Windows.Forms.SaveFileDialog();
            this.pictureBoxRiskSolutions = new System.Windows.Forms.PictureBox();
            this.pictureBoxERNIE = new System.Windows.Forms.PictureBox();
            this.pictureBoxErnieDescription = new System.Windows.Forms.PictureBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.panel9.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridLossReserves)).BeginInit();
            this.panel7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridCreditRiskSavings)).BeginInit();
            this.panel5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridWithIns)).BeginInit();
            this.panel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridNoInsurance)).BeginInit();
            this.panel2.SuspendLayout();
            this.panel1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridInterestRates)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridFinanceData)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picStart)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picPrint)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxRiskSolutions)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxERNIE)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxErnieDescription)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tabControl1.Location = new System.Drawing.Point(26, 288);
            this.tabControl1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(930, 778);
            this.tabControl1.TabIndex = 0;
            this.tabControl1.SelectedIndexChanged += new System.EventHandler(this.tabControl1_SelectedIndexChanged);
            // 
            // tabPage1
            // 
            this.tabPage1.BackColor = System.Drawing.Color.White;
            this.tabPage1.Controls.Add(this.panel9);
            this.tabPage1.Controls.Add(this.panel7);
            this.tabPage1.Controls.Add(this.panel5);
            this.tabPage1.Controls.Add(this.panel3);
            this.tabPage1.Controls.Add(this.panel2);
            this.tabPage1.Controls.Add(this.panel1);
            this.tabPage1.ForeColor = System.Drawing.Color.Black;
            this.tabPage1.Location = new System.Drawing.Point(4, 25);
            this.tabPage1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.tabPage1.Size = new System.Drawing.Size(922, 749);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Analysis";
            this.tabPage1.Click += new System.EventHandler(this.tabPage1_Click);
            // 
            // panel9
            // 
            this.panel9.Controls.Add(this.label9);
            this.panel9.Controls.Add(this.label26);
            this.panel9.Controls.Add(this.label28);
            this.panel9.Controls.Add(this.gridLossReserves);
            this.panel9.Location = new System.Drawing.Point(2, 638);
            this.panel9.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.panel9.Name = "panel9";
            this.panel9.Size = new System.Drawing.Size(904, 108);
            this.panel9.TabIndex = 5;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.BackColor = System.Drawing.Color.Transparent;
            this.label9.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(127)))), ((int)(((byte)(146)))));
            this.label9.Location = new System.Drawing.Point(109, 5);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(123, 19);
            this.label9.TabIndex = 6;
            this.label9.Text = "Loss Reserves";
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.BackColor = System.Drawing.Color.Transparent;
            this.label26.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label26.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(127)))), ((int)(((byte)(146)))));
            this.label26.Location = new System.Drawing.Point(2, 38);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(81, 19);
            this.label26.TabIndex = 5;
            this.label26.Text = "Reserves";
            this.label26.Visible = false;
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.BackColor = System.Drawing.Color.Transparent;
            this.label28.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label28.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(127)))), ((int)(((byte)(146)))));
            this.label28.Location = new System.Drawing.Point(4, 15);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(47, 19);
            this.label28.TabIndex = 4;
            this.label28.Text = "Loss";
            this.label28.Visible = false;
            this.label28.Click += new System.EventHandler(this.label28_Click);
            // 
            // gridLossReserves
            // 
            this.gridLossReserves.AllowUserToAddRows = false;
            this.gridLossReserves.AllowUserToDeleteRows = false;
            this.gridLossReserves.BackgroundColor = System.Drawing.Color.Gainsboro;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.gridLossReserves.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.gridLossReserves.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.gridLossReserves.DefaultCellStyle = dataGridViewCellStyle2;
            this.gridLossReserves.Location = new System.Drawing.Point(12, 35);
            this.gridLossReserves.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.gridLossReserves.MultiSelect = false;
            this.gridLossReserves.Name = "gridLossReserves";
            this.gridLossReserves.ReadOnly = true;
            this.gridLossReserves.RowHeadersVisible = false;
            this.gridLossReserves.ScrollBars = System.Windows.Forms.ScrollBars.Horizontal;
            this.gridLossReserves.Size = new System.Drawing.Size(878, 65);
            this.gridLossReserves.TabIndex = 0;
            this.gridLossReserves.Click += new System.EventHandler(this.gridLossReserves_Click);
            // 
            // panel7
            // 
            this.panel7.Controls.Add(this.label6);
            this.panel7.Controls.Add(this.label27);
            this.panel7.Controls.Add(this.label24);
            this.panel7.Controls.Add(this.lblCreditRisk);
            this.panel7.Controls.Add(this.gridCreditRiskSavings);
            this.panel7.Location = new System.Drawing.Point(2, 528);
            this.panel7.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.panel7.Name = "panel7";
            this.panel7.Size = new System.Drawing.Size(904, 108);
            this.panel7.TabIndex = 4;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.BackColor = System.Drawing.Color.Transparent;
            this.label6.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(127)))), ((int)(((byte)(146)))));
            this.label6.Location = new System.Drawing.Point(109, 5);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(159, 19);
            this.label6.TabIndex = 6;
            this.label6.Text = "Credit Risk Savings";
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.BackColor = System.Drawing.Color.Transparent;
            this.label27.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label27.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(127)))), ((int)(((byte)(146)))));
            this.label27.Location = new System.Drawing.Point(67, 5);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(71, 19);
            this.label27.TabIndex = 5;
            this.label27.Text = "Savings";
            this.label27.Visible = false;
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.BackColor = System.Drawing.Color.Transparent;
            this.label24.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label24.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(127)))), ((int)(((byte)(146)))));
            this.label24.Location = new System.Drawing.Point(4, 15);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(93, 19);
            this.label24.TabIndex = 4;
            this.label24.Text = "Credit Risk";
            this.label24.Visible = false;
            // 
            // lblCreditRisk
            // 
            this.lblCreditRisk.AutoSize = true;
            this.lblCreditRisk.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(249)))), ((int)(((byte)(249)))), ((int)(((byte)(249)))));
            this.lblCreditRisk.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCreditRisk.ForeColor = System.Drawing.Color.Black;
            this.lblCreditRisk.Location = new System.Drawing.Point(5, 62);
            this.lblCreditRisk.Name = "lblCreditRisk";
            this.lblCreditRisk.Size = new System.Drawing.Size(16, 16);
            this.lblCreditRisk.TabIndex = 3;
            this.lblCreditRisk.Text = ">";
            // 
            // gridCreditRiskSavings
            // 
            this.gridCreditRiskSavings.AllowUserToAddRows = false;
            this.gridCreditRiskSavings.AllowUserToDeleteRows = false;
            this.gridCreditRiskSavings.BackgroundColor = System.Drawing.Color.Gainsboro;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.gridCreditRiskSavings.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle3;
            this.gridCreditRiskSavings.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle4.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle4.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle4.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.gridCreditRiskSavings.DefaultCellStyle = dataGridViewCellStyle4;
            this.gridCreditRiskSavings.Location = new System.Drawing.Point(114, 35);
            this.gridCreditRiskSavings.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.gridCreditRiskSavings.MultiSelect = false;
            this.gridCreditRiskSavings.Name = "gridCreditRiskSavings";
            this.gridCreditRiskSavings.ReadOnly = true;
            this.gridCreditRiskSavings.RowHeadersVisible = false;
            this.gridCreditRiskSavings.ScrollBars = System.Windows.Forms.ScrollBars.Horizontal;
            this.gridCreditRiskSavings.Size = new System.Drawing.Size(778, 65);
            this.gridCreditRiskSavings.TabIndex = 0;
            this.gridCreditRiskSavings.Click += new System.EventHandler(this.gridCreditRiskSavings_Click);
            // 
            // panel5
            // 
            this.panel5.Controls.Add(this.label21);
            this.panel5.Controls.Add(this.lblWithIns);
            this.panel5.Controls.Add(this.label23);
            this.panel5.Controls.Add(this.gridWithIns);
            this.panel5.Location = new System.Drawing.Point(2, 418);
            this.panel5.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(904, 108);
            this.panel5.TabIndex = 3;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.BackColor = System.Drawing.Color.Transparent;
            this.label21.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label21.ForeColor = System.Drawing.Color.Black;
            this.label21.Location = new System.Drawing.Point(5, 45);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(74, 16);
            this.label21.TabIndex = 4;
            this.label21.Text = "Probability";
            // 
            // lblWithIns
            // 
            this.lblWithIns.AutoSize = true;
            this.lblWithIns.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(249)))), ((int)(((byte)(249)))), ((int)(((byte)(249)))));
            this.lblWithIns.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblWithIns.ForeColor = System.Drawing.Color.Black;
            this.lblWithIns.Location = new System.Drawing.Point(5, 62);
            this.lblWithIns.Name = "lblWithIns";
            this.lblWithIns.Size = new System.Drawing.Size(16, 16);
            this.lblWithIns.TabIndex = 3;
            this.lblWithIns.Text = ">";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label23.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(127)))), ((int)(((byte)(146)))));
            this.label23.Location = new System.Drawing.Point(109, 5);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(416, 19);
            this.label23.TabIndex = 2;
            this.label23.Text = "Project Savings Credit Enhancement - With Insurance";
            // 
            // gridWithIns
            // 
            this.gridWithIns.AllowUserToAddRows = false;
            this.gridWithIns.AllowUserToDeleteRows = false;
            this.gridWithIns.BackgroundColor = System.Drawing.Color.Gainsboro;
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle5.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle5.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle5.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle5.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle5.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.gridWithIns.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle5;
            this.gridWithIns.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle6.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle6.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle6.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle6.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle6.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle6.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.gridWithIns.DefaultCellStyle = dataGridViewCellStyle6;
            this.gridWithIns.Location = new System.Drawing.Point(114, 35);
            this.gridWithIns.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.gridWithIns.MultiSelect = false;
            this.gridWithIns.Name = "gridWithIns";
            this.gridWithIns.ReadOnly = true;
            this.gridWithIns.RowHeadersVisible = false;
            this.gridWithIns.ScrollBars = System.Windows.Forms.ScrollBars.Horizontal;
            this.gridWithIns.Size = new System.Drawing.Size(778, 65);
            this.gridWithIns.TabIndex = 0;
            this.gridWithIns.Click += new System.EventHandler(this.gridWithIns_Click);
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.label20);
            this.panel3.Controls.Add(this.lblNoIns);
            this.panel3.Controls.Add(this.label18);
            this.panel3.Controls.Add(this.gridNoInsurance);
            this.panel3.Location = new System.Drawing.Point(2, 308);
            this.panel3.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(904, 108);
            this.panel3.TabIndex = 2;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.BackColor = System.Drawing.Color.Transparent;
            this.label20.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.ForeColor = System.Drawing.Color.Black;
            this.label20.Location = new System.Drawing.Point(5, 45);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(74, 16);
            this.label20.TabIndex = 4;
            this.label20.Text = "Probability";
            // 
            // lblNoIns
            // 
            this.lblNoIns.AutoSize = true;
            this.lblNoIns.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(249)))), ((int)(((byte)(249)))), ((int)(((byte)(249)))));
            this.lblNoIns.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNoIns.ForeColor = System.Drawing.Color.Black;
            this.lblNoIns.Location = new System.Drawing.Point(5, 62);
            this.lblNoIns.Name = "lblNoIns";
            this.lblNoIns.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.lblNoIns.Size = new System.Drawing.Size(16, 16);
            this.lblNoIns.TabIndex = 3;
            this.lblNoIns.Text = ">";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(127)))), ((int)(((byte)(146)))));
            this.label18.Location = new System.Drawing.Point(109, 5);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(404, 19);
            this.label18.TabIndex = 2;
            this.label18.Text = "Project Savings Credit Enhancement - No Insurance";
            // 
            // gridNoInsurance
            // 
            this.gridNoInsurance.AllowUserToAddRows = false;
            this.gridNoInsurance.AllowUserToDeleteRows = false;
            this.gridNoInsurance.BackgroundColor = System.Drawing.Color.Gainsboro;
            dataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle7.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle7.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle7.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle7.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle7.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle7.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.gridNoInsurance.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle7;
            this.gridNoInsurance.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle8.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle8.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle8.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle8.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle8.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle8.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.gridNoInsurance.DefaultCellStyle = dataGridViewCellStyle8;
            this.gridNoInsurance.Location = new System.Drawing.Point(114, 35);
            this.gridNoInsurance.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.gridNoInsurance.MultiSelect = false;
            this.gridNoInsurance.Name = "gridNoInsurance";
            this.gridNoInsurance.ReadOnly = true;
            this.gridNoInsurance.RowHeadersVisible = false;
            this.gridNoInsurance.ScrollBars = System.Windows.Forms.ScrollBars.Horizontal;
            this.gridNoInsurance.Size = new System.Drawing.Size(778, 65);
            this.gridNoInsurance.TabIndex = 0;
            this.gridNoInsurance.Click += new System.EventHandler(this.gridNoInsurance_Click);
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.rtf10);
            this.panel2.Controls.Add(this.rtf20);
            this.panel2.Controls.Add(this.rtf30);
            this.panel2.Controls.Add(this.rtf40);
            this.panel2.Controls.Add(this.rtf50);
            this.panel2.Controls.Add(this.rtf60);
            this.panel2.Controls.Add(this.rtf70);
            this.panel2.Controls.Add(this.rtf80);
            this.panel2.Controls.Add(this.rtf90);
            this.panel2.Controls.Add(this.P10);
            this.panel2.Controls.Add(this.P20);
            this.panel2.Controls.Add(this.P30);
            this.panel2.Controls.Add(this.P40);
            this.panel2.Controls.Add(this.P50);
            this.panel2.Controls.Add(this.P60);
            this.panel2.Controls.Add(this.P70);
            this.panel2.Controls.Add(this.P80);
            this.panel2.Controls.Add(this.P90);
            this.panel2.Location = new System.Drawing.Point(530, 4);
            this.panel2.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(204, 269);
            this.panel2.TabIndex = 1;
            this.panel2.Paint += new System.Windows.Forms.PaintEventHandler(this.panel2_Paint);
            // 
            // rtf10
            // 
            this.rtf10.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(249)))), ((int)(((byte)(249)))), ((int)(((byte)(249)))));
            this.rtf10.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.rtf10.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rtf10.ForeColor = System.Drawing.Color.Black;
            this.rtf10.Location = new System.Drawing.Point(7, 229);
            this.rtf10.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.rtf10.Name = "rtf10";
            this.rtf10.ScrollBars = System.Windows.Forms.RichTextBoxScrollBars.None;
            this.rtf10.Size = new System.Drawing.Size(55, 25);
            this.rtf10.TabIndex = 30;
            this.rtf10.Text = "ten";
            this.rtf10.WordWrap = false;
            this.rtf10.TextChanged += new System.EventHandler(this.rtf10_TextChanged);
            // 
            // rtf20
            // 
            this.rtf20.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(249)))), ((int)(((byte)(249)))), ((int)(((byte)(249)))));
            this.rtf20.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.rtf20.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rtf20.ForeColor = System.Drawing.Color.Black;
            this.rtf20.Location = new System.Drawing.Point(7, 201);
            this.rtf20.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.rtf20.Name = "rtf20";
            this.rtf20.ScrollBars = System.Windows.Forms.RichTextBoxScrollBars.None;
            this.rtf20.Size = new System.Drawing.Size(55, 25);
            this.rtf20.TabIndex = 29;
            this.rtf20.Text = "";
            this.rtf20.WordWrap = false;
            this.rtf20.TextChanged += new System.EventHandler(this.rtf20_TextChanged);
            // 
            // rtf30
            // 
            this.rtf30.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(249)))), ((int)(((byte)(249)))), ((int)(((byte)(249)))));
            this.rtf30.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.rtf30.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rtf30.ForeColor = System.Drawing.Color.Black;
            this.rtf30.Location = new System.Drawing.Point(8, 173);
            this.rtf30.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.rtf30.Name = "rtf30";
            this.rtf30.ScrollBars = System.Windows.Forms.RichTextBoxScrollBars.None;
            this.rtf30.Size = new System.Drawing.Size(55, 25);
            this.rtf30.TabIndex = 28;
            this.rtf30.Text = "30";
            this.rtf30.WordWrap = false;
            this.rtf30.TextChanged += new System.EventHandler(this.rtf30_TextChanged);
            // 
            // rtf40
            // 
            this.rtf40.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(249)))), ((int)(((byte)(249)))), ((int)(((byte)(249)))));
            this.rtf40.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.rtf40.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rtf40.ForeColor = System.Drawing.Color.Black;
            this.rtf40.Location = new System.Drawing.Point(7, 145);
            this.rtf40.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.rtf40.Name = "rtf40";
            this.rtf40.ScrollBars = System.Windows.Forms.RichTextBoxScrollBars.None;
            this.rtf40.Size = new System.Drawing.Size(55, 25);
            this.rtf40.TabIndex = 27;
            this.rtf40.Text = "40";
            this.rtf40.WordWrap = false;
            this.rtf40.TextChanged += new System.EventHandler(this.rtf40_TextChanged);
            // 
            // rtf50
            // 
            this.rtf50.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(249)))), ((int)(((byte)(249)))), ((int)(((byte)(249)))));
            this.rtf50.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.rtf50.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rtf50.ForeColor = System.Drawing.Color.Black;
            this.rtf50.Location = new System.Drawing.Point(7, 117);
            this.rtf50.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.rtf50.Name = "rtf50";
            this.rtf50.ScrollBars = System.Windows.Forms.RichTextBoxScrollBars.None;
            this.rtf50.Size = new System.Drawing.Size(55, 25);
            this.rtf50.TabIndex = 26;
            this.rtf50.Text = "50";
            this.rtf50.WordWrap = false;
            this.rtf50.TextChanged += new System.EventHandler(this.rtf50_TextChanged);
            // 
            // rtf60
            // 
            this.rtf60.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(249)))), ((int)(((byte)(249)))), ((int)(((byte)(249)))));
            this.rtf60.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.rtf60.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rtf60.ForeColor = System.Drawing.Color.Black;
            this.rtf60.Location = new System.Drawing.Point(8, 89);
            this.rtf60.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.rtf60.Name = "rtf60";
            this.rtf60.ScrollBars = System.Windows.Forms.RichTextBoxScrollBars.None;
            this.rtf60.Size = new System.Drawing.Size(55, 25);
            this.rtf60.TabIndex = 25;
            this.rtf60.Text = "60";
            this.rtf60.WordWrap = false;
            this.rtf60.TextChanged += new System.EventHandler(this.rtf60_TextChanged);
            // 
            // rtf70
            // 
            this.rtf70.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(249)))), ((int)(((byte)(249)))), ((int)(((byte)(249)))));
            this.rtf70.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.rtf70.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rtf70.ForeColor = System.Drawing.Color.Black;
            this.rtf70.Location = new System.Drawing.Point(8, 61);
            this.rtf70.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.rtf70.Name = "rtf70";
            this.rtf70.ScrollBars = System.Windows.Forms.RichTextBoxScrollBars.None;
            this.rtf70.Size = new System.Drawing.Size(55, 25);
            this.rtf70.TabIndex = 24;
            this.rtf70.Text = "70";
            this.rtf70.WordWrap = false;
            this.rtf70.TextChanged += new System.EventHandler(this.rtf70_TextChanged);
            // 
            // rtf80
            // 
            this.rtf80.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(249)))), ((int)(((byte)(249)))), ((int)(((byte)(249)))));
            this.rtf80.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.rtf80.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rtf80.ForeColor = System.Drawing.Color.Black;
            this.rtf80.Location = new System.Drawing.Point(8, 33);
            this.rtf80.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.rtf80.Name = "rtf80";
            this.rtf80.ScrollBars = System.Windows.Forms.RichTextBoxScrollBars.None;
            this.rtf80.Size = new System.Drawing.Size(55, 25);
            this.rtf80.TabIndex = 23;
            this.rtf80.Text = "80";
            this.rtf80.WordWrap = false;
            this.rtf80.TextChanged += new System.EventHandler(this.rtf80_TextChanged);
            // 
            // rtf90
            // 
            this.rtf90.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(249)))), ((int)(((byte)(249)))), ((int)(((byte)(249)))));
            this.rtf90.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.rtf90.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rtf90.ForeColor = System.Drawing.Color.Black;
            this.rtf90.Location = new System.Drawing.Point(8, 5);
            this.rtf90.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.rtf90.Name = "rtf90";
            this.rtf90.ScrollBars = System.Windows.Forms.RichTextBoxScrollBars.None;
            this.rtf90.Size = new System.Drawing.Size(55, 25);
            this.rtf90.TabIndex = 22;
            this.rtf90.Text = "90";
            this.rtf90.WordWrap = false;
            this.rtf90.TextChanged += new System.EventHandler(this.rtf90_TextChanged);
            // 
            // P10
            // 
            this.P10.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(235)))), ((int)(((byte)(235)))), ((int)(((byte)(235)))));
            this.P10.Enabled = false;
            this.P10.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.P10.ForeColor = System.Drawing.Color.Black;
            this.P10.Location = new System.Drawing.Point(92, 229);
            this.P10.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.P10.Name = "P10";
            this.P10.ReadOnly = true;
            this.P10.Size = new System.Drawing.Size(100, 23);
            this.P10.TabIndex = 10;
            this.P10.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // P20
            // 
            this.P20.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(235)))), ((int)(((byte)(235)))), ((int)(((byte)(235)))));
            this.P20.Enabled = false;
            this.P20.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.P20.ForeColor = System.Drawing.Color.Black;
            this.P20.Location = new System.Drawing.Point(92, 201);
            this.P20.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.P20.Name = "P20";
            this.P20.ReadOnly = true;
            this.P20.Size = new System.Drawing.Size(100, 23);
            this.P20.TabIndex = 9;
            this.P20.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // P30
            // 
            this.P30.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(235)))), ((int)(((byte)(235)))), ((int)(((byte)(235)))));
            this.P30.Enabled = false;
            this.P30.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.P30.ForeColor = System.Drawing.Color.Black;
            this.P30.Location = new System.Drawing.Point(92, 173);
            this.P30.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.P30.Name = "P30";
            this.P30.ReadOnly = true;
            this.P30.Size = new System.Drawing.Size(100, 23);
            this.P30.TabIndex = 8;
            this.P30.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // P40
            // 
            this.P40.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(235)))), ((int)(((byte)(235)))), ((int)(((byte)(235)))));
            this.P40.Enabled = false;
            this.P40.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.P40.ForeColor = System.Drawing.Color.Black;
            this.P40.Location = new System.Drawing.Point(92, 145);
            this.P40.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.P40.Name = "P40";
            this.P40.ReadOnly = true;
            this.P40.Size = new System.Drawing.Size(100, 23);
            this.P40.TabIndex = 7;
            this.P40.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // P50
            // 
            this.P50.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(235)))), ((int)(((byte)(235)))), ((int)(((byte)(235)))));
            this.P50.Enabled = false;
            this.P50.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.P50.ForeColor = System.Drawing.Color.Black;
            this.P50.Location = new System.Drawing.Point(92, 117);
            this.P50.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.P50.Name = "P50";
            this.P50.ReadOnly = true;
            this.P50.Size = new System.Drawing.Size(100, 23);
            this.P50.TabIndex = 6;
            this.P50.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // P60
            // 
            this.P60.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(235)))), ((int)(((byte)(235)))), ((int)(((byte)(235)))));
            this.P60.Enabled = false;
            this.P60.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.P60.ForeColor = System.Drawing.Color.Black;
            this.P60.Location = new System.Drawing.Point(92, 89);
            this.P60.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.P60.Name = "P60";
            this.P60.ReadOnly = true;
            this.P60.Size = new System.Drawing.Size(100, 23);
            this.P60.TabIndex = 5;
            this.P60.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // P70
            // 
            this.P70.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(235)))), ((int)(((byte)(235)))), ((int)(((byte)(235)))));
            this.P70.Enabled = false;
            this.P70.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.P70.ForeColor = System.Drawing.Color.Black;
            this.P70.Location = new System.Drawing.Point(92, 61);
            this.P70.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.P70.Name = "P70";
            this.P70.ReadOnly = true;
            this.P70.Size = new System.Drawing.Size(100, 23);
            this.P70.TabIndex = 4;
            this.P70.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // P80
            // 
            this.P80.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(235)))), ((int)(((byte)(235)))), ((int)(((byte)(235)))));
            this.P80.Enabled = false;
            this.P80.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.P80.ForeColor = System.Drawing.SystemColors.WindowText;
            this.P80.Location = new System.Drawing.Point(92, 33);
            this.P80.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.P80.Name = "P80";
            this.P80.ReadOnly = true;
            this.P80.Size = new System.Drawing.Size(100, 23);
            this.P80.TabIndex = 3;
            this.P80.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // P90
            // 
            this.P90.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(235)))), ((int)(((byte)(235)))), ((int)(((byte)(235)))));
            this.P90.Enabled = false;
            this.P90.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.P90.ForeColor = System.Drawing.Color.Black;
            this.P90.Location = new System.Drawing.Point(92, 5);
            this.P90.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.P90.Name = "P90";
            this.P90.ReadOnly = true;
            this.P90.Size = new System.Drawing.Size(100, 23);
            this.P90.TabIndex = 2;
            this.P90.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.label31);
            this.panel1.Controls.Add(this.SavingsFromLoanPaymentReduction);
            this.panel1.Controls.Add(this.InsuredEffectiveLoanPayment);
            this.panel1.Controls.Add(this.UninsuredAnnualLoanPayment);
            this.panel1.Controls.Add(this.label7);
            this.panel1.Controls.Add(this.lblUninsuredLoanPayment);
            this.panel1.Controls.Add(this.label5);
            this.panel1.Controls.Add(this.cboPeriod);
            this.panel1.Controls.Add(this.label4);
            this.panel1.Controls.Add(this.label3);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.lblLoanTerm);
            this.panel1.Controls.Add(this.CreditRatingInsurer);
            this.panel1.Controls.Add(this.CreditRatingInsured);
            this.panel1.Controls.Add(this.olicyCoverageFactor);
            this.panel1.Controls.Add(this.IotalInsuredProjectSavings);
            this.panel1.Controls.Add(this.LoanTerm);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.btnProjectSavingsDistributionFile);
            this.panel1.Controls.Add(this.label8);
            this.panel1.Controls.Add(this.LoanAmount);
            this.panel1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.panel1.Location = new System.Drawing.Point(2, 5);
            this.panel1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(425, 269);
            this.panel1.TabIndex = 0;
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label31.ForeColor = System.Drawing.Color.Black;
            this.label31.Location = new System.Drawing.Point(4, 240);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(257, 16);
            this.label31.TabIndex = 18;
            this.label31.Text = "Savings from Loan Payment Reduction:";
            // 
            // SavingsFromLoanPaymentReduction
            // 
            this.SavingsFromLoanPaymentReduction.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(235)))), ((int)(((byte)(235)))), ((int)(((byte)(235)))));
            this.SavingsFromLoanPaymentReduction.Enabled = false;
            this.SavingsFromLoanPaymentReduction.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SavingsFromLoanPaymentReduction.ForeColor = System.Drawing.Color.Black;
            this.SavingsFromLoanPaymentReduction.Location = new System.Drawing.Point(309, 240);
            this.SavingsFromLoanPaymentReduction.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.SavingsFromLoanPaymentReduction.Name = "SavingsFromLoanPaymentReduction";
            this.SavingsFromLoanPaymentReduction.ReadOnly = true;
            this.SavingsFromLoanPaymentReduction.Size = new System.Drawing.Size(100, 23);
            this.SavingsFromLoanPaymentReduction.TabIndex = 17;
            this.SavingsFromLoanPaymentReduction.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // InsuredEffectiveLoanPayment
            // 
            this.InsuredEffectiveLoanPayment.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(235)))), ((int)(((byte)(235)))), ((int)(((byte)(235)))));
            this.InsuredEffectiveLoanPayment.Enabled = false;
            this.InsuredEffectiveLoanPayment.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.InsuredEffectiveLoanPayment.ForeColor = System.Drawing.Color.Black;
            this.InsuredEffectiveLoanPayment.Location = new System.Drawing.Point(309, 215);
            this.InsuredEffectiveLoanPayment.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.InsuredEffectiveLoanPayment.Name = "InsuredEffectiveLoanPayment";
            this.InsuredEffectiveLoanPayment.ReadOnly = true;
            this.InsuredEffectiveLoanPayment.Size = new System.Drawing.Size(100, 23);
            this.InsuredEffectiveLoanPayment.TabIndex = 16;
            this.InsuredEffectiveLoanPayment.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // UninsuredAnnualLoanPayment
            // 
            this.UninsuredAnnualLoanPayment.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(235)))), ((int)(((byte)(235)))), ((int)(((byte)(235)))));
            this.UninsuredAnnualLoanPayment.Enabled = false;
            this.UninsuredAnnualLoanPayment.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.UninsuredAnnualLoanPayment.ForeColor = System.Drawing.Color.Black;
            this.UninsuredAnnualLoanPayment.Location = new System.Drawing.Point(309, 190);
            this.UninsuredAnnualLoanPayment.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.UninsuredAnnualLoanPayment.Name = "UninsuredAnnualLoanPayment";
            this.UninsuredAnnualLoanPayment.ReadOnly = true;
            this.UninsuredAnnualLoanPayment.Size = new System.Drawing.Size(100, 23);
            this.UninsuredAnnualLoanPayment.TabIndex = 15;
            this.UninsuredAnnualLoanPayment.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.Black;
            this.label7.Location = new System.Drawing.Point(4, 215);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(212, 16);
            this.label7.TabIndex = 14;
            this.label7.Text = "Insured Effective Loan Payment:";
            // 
            // lblUninsuredLoanPayment
            // 
            this.lblUninsuredLoanPayment.AutoSize = true;
            this.lblUninsuredLoanPayment.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblUninsuredLoanPayment.ForeColor = System.Drawing.Color.Black;
            this.lblUninsuredLoanPayment.Location = new System.Drawing.Point(4, 190);
            this.lblUninsuredLoanPayment.Name = "lblUninsuredLoanPayment";
            this.lblUninsuredLoanPayment.Size = new System.Drawing.Size(171, 16);
            this.lblUninsuredLoanPayment.TabIndex = 13;
            this.lblUninsuredLoanPayment.Text = "Uninsured Loan Payment:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.Black;
            this.label5.Location = new System.Drawing.Point(3, 130);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(183, 16);
            this.label5.TabIndex = 12;
            this.label5.Text = "Credit Rating of the Insurer:";
            // 
            // cboPeriod
            // 
            this.cboPeriod.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboPeriod.ForeColor = System.Drawing.Color.Black;
            this.cboPeriod.FormattingEnabled = true;
            this.cboPeriod.Items.AddRange(new object[] {
            "Year",
            "Month"});
            this.cboPeriod.Location = new System.Drawing.Point(228, 33);
            this.cboPeriod.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.cboPeriod.MaxDropDownItems = 2;
            this.cboPeriod.Name = "cboPeriod";
            this.cboPeriod.Size = new System.Drawing.Size(75, 24);
            this.cboPeriod.TabIndex = 10;
            this.cboPeriod.Visible = false;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.Black;
            this.label4.Location = new System.Drawing.Point(4, 105);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(186, 16);
            this.label4.TabIndex = 11;
            this.label4.Text = "Credit Rating of the Insured:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.Black;
            this.label3.Location = new System.Drawing.Point(4, 80);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(186, 16);
            this.label3.TabIndex = 10;
            this.label3.Text = "Policy Coverage Factor (%):";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Black;
            this.label2.Location = new System.Drawing.Point(4, 55);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(195, 16);
            this.label2.TabIndex = 9;
            this.label2.Text = "Total Insured Project Savings:";
            // 
            // lblLoanTerm
            // 
            this.lblLoanTerm.AutoSize = true;
            this.lblLoanTerm.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblLoanTerm.ForeColor = System.Drawing.Color.Black;
            this.lblLoanTerm.Location = new System.Drawing.Point(4, 30);
            this.lblLoanTerm.Name = "lblLoanTerm";
            this.lblLoanTerm.Size = new System.Drawing.Size(119, 16);
            this.lblLoanTerm.TabIndex = 8;
            this.lblLoanTerm.Text = "Loan Term Years:";
            // 
            // CreditRatingInsurer
            // 
            this.CreditRatingInsurer.AutoCompleteCustomSource.AddRange(new string[] {
            "B-",
            "B",
            "B+",
            "BB-",
            "BB\t",
            "BB+",
            "BBB-",
            "BBB-",
            "BBB+",
            "A-",
            "A",
            "A+",
            "AA-",
            "AA",
            "AA+"});
            this.CreditRatingInsurer.BackColor = System.Drawing.SystemColors.Window;
            this.CreditRatingInsurer.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CreditRatingInsurer.ForeColor = System.Drawing.Color.Black;
            this.CreditRatingInsurer.FormattingEnabled = true;
            this.CreditRatingInsurer.Items.AddRange(new object[] {
            "B-",
            "B",
            "B+",
            "BB-",
            "BB\t",
            "BB+",
            "BBB-",
            "BBB-",
            "BBB+",
            "A-",
            "A",
            "A+",
            "AA-",
            "AA",
            "AA+"});
            this.CreditRatingInsurer.Location = new System.Drawing.Point(309, 130);
            this.CreditRatingInsurer.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.CreditRatingInsurer.Name = "CreditRatingInsurer";
            this.CreditRatingInsurer.Size = new System.Drawing.Size(100, 24);
            this.CreditRatingInsurer.TabIndex = 6;
            this.CreditRatingInsurer.SelectedIndexChanged += new System.EventHandler(this.CreditRatingInsurer_SelectedIndexChanged);
            // 
            // CreditRatingInsured
            // 
            this.CreditRatingInsured.BackColor = System.Drawing.SystemColors.Window;
            this.CreditRatingInsured.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CreditRatingInsured.ForeColor = System.Drawing.Color.Black;
            this.CreditRatingInsured.FormattingEnabled = true;
            this.CreditRatingInsured.Items.AddRange(new object[] {
            "B-",
            "B",
            "B+",
            "BB-",
            "BB\t",
            "BB+",
            "BBB-",
            "BBB-",
            "BBB+",
            "A-",
            "A",
            "A+",
            "AA-",
            "AA",
            "AA+"});
            this.CreditRatingInsured.Location = new System.Drawing.Point(309, 105);
            this.CreditRatingInsured.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.CreditRatingInsured.Name = "CreditRatingInsured";
            this.CreditRatingInsured.Size = new System.Drawing.Size(100, 24);
            this.CreditRatingInsured.TabIndex = 5;
            this.CreditRatingInsured.SelectedIndexChanged += new System.EventHandler(this.CreditRatingInsured_SelectedIndexChanged);
            // 
            // olicyCoverageFactor
            // 
            this.olicyCoverageFactor.BackColor = System.Drawing.Color.White;
            this.olicyCoverageFactor.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.olicyCoverageFactor.ForeColor = System.Drawing.Color.Black;
            this.olicyCoverageFactor.Location = new System.Drawing.Point(309, 80);
            this.olicyCoverageFactor.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.olicyCoverageFactor.Name = "olicyCoverageFactor";
            this.olicyCoverageFactor.Size = new System.Drawing.Size(100, 23);
            this.olicyCoverageFactor.TabIndex = 4;
            this.olicyCoverageFactor.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.olicyCoverageFactor.TextChanged += new System.EventHandler(this.olicyCoverageFactor_TextChanged);
            // 
            // IotalInsuredProjectSavings
            // 
            this.IotalInsuredProjectSavings.BackColor = System.Drawing.Color.White;
            this.IotalInsuredProjectSavings.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.IotalInsuredProjectSavings.ForeColor = System.Drawing.Color.Black;
            this.IotalInsuredProjectSavings.Location = new System.Drawing.Point(309, 55);
            this.IotalInsuredProjectSavings.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.IotalInsuredProjectSavings.Name = "IotalInsuredProjectSavings";
            this.IotalInsuredProjectSavings.Size = new System.Drawing.Size(100, 23);
            this.IotalInsuredProjectSavings.TabIndex = 3;
            this.IotalInsuredProjectSavings.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.IotalInsuredProjectSavings.TextChanged += new System.EventHandler(this.IotalInsuredProjectSavings_TextChanged);
            // 
            // LoanTerm
            // 
            this.LoanTerm.BackColor = System.Drawing.Color.White;
            this.LoanTerm.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LoanTerm.ForeColor = System.Drawing.Color.Black;
            this.LoanTerm.Location = new System.Drawing.Point(309, 30);
            this.LoanTerm.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.LoanTerm.Name = "LoanTerm";
            this.LoanTerm.Size = new System.Drawing.Size(100, 23);
            this.LoanTerm.TabIndex = 2;
            this.LoanTerm.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.LoanTerm.TextChanged += new System.EventHandler(this.LoanTerm_TextChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Black;
            this.label1.Location = new System.Drawing.Point(4, 5);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(95, 16);
            this.label1.TabIndex = 1;
            this.label1.Text = "Loan Amount:";
            // 
            // btnProjectSavingsDistributionFile
            // 
            this.btnProjectSavingsDistributionFile.BackColor = System.Drawing.Color.Transparent;
            this.btnProjectSavingsDistributionFile.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnProjectSavingsDistributionFile.ForeColor = System.Drawing.Color.Black;
            this.btnProjectSavingsDistributionFile.Location = new System.Drawing.Point(307, 162);
            this.btnProjectSavingsDistributionFile.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnProjectSavingsDistributionFile.Name = "btnProjectSavingsDistributionFile";
            this.btnProjectSavingsDistributionFile.Size = new System.Drawing.Size(100, 22);
            this.btnProjectSavingsDistributionFile.TabIndex = 0;
            this.btnProjectSavingsDistributionFile.UseVisualStyleBackColor = false;
            this.btnProjectSavingsDistributionFile.Click += new System.EventHandler(this.btnProjectSavingsDistributionFile_Click);
            this.btnProjectSavingsDistributionFile.MouseHover += new System.EventHandler(this.btnProjectSavingsDistributionFile_MouseHover);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.Color.Black;
            this.label8.Location = new System.Drawing.Point(5, 162);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(212, 16);
            this.label8.TabIndex = 1;
            this.label8.Text = "Project Savings Distribution File:";
            // 
            // LoanAmount
            // 
            this.LoanAmount.BackColor = System.Drawing.Color.White;
            this.LoanAmount.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LoanAmount.ForeColor = System.Drawing.Color.Black;
            this.LoanAmount.Location = new System.Drawing.Point(309, 5);
            this.LoanAmount.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.LoanAmount.Name = "LoanAmount";
            this.LoanAmount.Size = new System.Drawing.Size(100, 23);
            this.LoanAmount.TabIndex = 0;
            this.LoanAmount.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.LoanAmount.TextChanged += new System.EventHandler(this.LoanAmount_TextChanged);
            // 
            // tabPage2
            // 
            this.tabPage2.BackColor = System.Drawing.Color.White;
            this.tabPage2.Controls.Add(this.lblFinanceDataHeader);
            this.tabPage2.Controls.Add(this.btnSaveInterestRates);
            this.tabPage2.Controls.Add(this.btnLoadInterestRates);
            this.tabPage2.Controls.Add(this.gridInterestRates);
            this.tabPage2.Controls.Add(this.gridFinanceData);
            this.tabPage2.Controls.Add(this.btnSave);
            this.tabPage2.Controls.Add(this.btnLoad);
            this.tabPage2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(93)))), ((int)(((byte)(97)))), ((int)(((byte)(102)))));
            this.tabPage2.Location = new System.Drawing.Point(4, 25);
            this.tabPage2.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.tabPage2.Size = new System.Drawing.Size(922, 749);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Finance Data";
            // 
            // lblFinanceDataHeader
            // 
            this.lblFinanceDataHeader.AutoSize = true;
            this.lblFinanceDataHeader.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(127)))), ((int)(((byte)(146)))));
            this.lblFinanceDataHeader.Location = new System.Drawing.Point(7, 158);
            this.lblFinanceDataHeader.Name = "lblFinanceDataHeader";
            this.lblFinanceDataHeader.Size = new System.Drawing.Size(0, 16);
            this.lblFinanceDataHeader.TabIndex = 6;
            // 
            // btnSaveInterestRates
            // 
            this.btnSaveInterestRates.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSaveInterestRates.ForeColor = System.Drawing.Color.Black;
            this.btnSaveInterestRates.Location = new System.Drawing.Point(458, 8);
            this.btnSaveInterestRates.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnSaveInterestRates.Name = "btnSaveInterestRates";
            this.btnSaveInterestRates.Size = new System.Drawing.Size(149, 28);
            this.btnSaveInterestRates.TabIndex = 5;
            this.btnSaveInterestRates.Text = "Save Interest Rates";
            this.btnSaveInterestRates.UseVisualStyleBackColor = true;
            this.btnSaveInterestRates.Click += new System.EventHandler(this.btnSaveInterestRates_Click);
            // 
            // btnLoadInterestRates
            // 
            this.btnLoadInterestRates.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLoadInterestRates.ForeColor = System.Drawing.Color.Black;
            this.btnLoadInterestRates.Location = new System.Drawing.Point(7, 8);
            this.btnLoadInterestRates.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnLoadInterestRates.Name = "btnLoadInterestRates";
            this.btnLoadInterestRates.Size = new System.Drawing.Size(149, 28);
            this.btnLoadInterestRates.TabIndex = 4;
            this.btnLoadInterestRates.Text = "Load Interest Rates";
            this.btnLoadInterestRates.UseVisualStyleBackColor = true;
            this.btnLoadInterestRates.Click += new System.EventHandler(this.btnLoadInterestRates_Click);
            // 
            // gridInterestRates
            // 
            this.gridInterestRates.AllowUserToAddRows = false;
            this.gridInterestRates.AllowUserToDeleteRows = false;
            this.gridInterestRates.BackgroundColor = System.Drawing.Color.Gainsboro;
            dataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle9.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle9.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle9.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(93)))), ((int)(((byte)(97)))), ((int)(((byte)(102)))));
            dataGridViewCellStyle9.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle9.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle9.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.gridInterestRates.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle9;
            this.gridInterestRates.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle10.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle10.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle10.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle10.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(93)))), ((int)(((byte)(97)))), ((int)(((byte)(102)))));
            dataGridViewCellStyle10.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle10.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle10.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.gridInterestRates.DefaultCellStyle = dataGridViewCellStyle10;
            this.gridInterestRates.Location = new System.Drawing.Point(2, 44);
            this.gridInterestRates.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.gridInterestRates.Name = "gridInterestRates";
            this.gridInterestRates.Size = new System.Drawing.Size(900, 65);
            this.gridInterestRates.TabIndex = 3;
            // 
            // gridFinanceData
            // 
            this.gridFinanceData.BackgroundColor = System.Drawing.Color.Gainsboro;
            dataGridViewCellStyle11.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle11.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle11.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle11.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(93)))), ((int)(((byte)(97)))), ((int)(((byte)(102)))));
            dataGridViewCellStyle11.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle11.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle11.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.gridFinanceData.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle11;
            this.gridFinanceData.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle12.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle12.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle12.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle12.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(93)))), ((int)(((byte)(97)))), ((int)(((byte)(102)))));
            dataGridViewCellStyle12.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle12.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle12.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.gridFinanceData.DefaultCellStyle = dataGridViewCellStyle12;
            this.gridFinanceData.Location = new System.Drawing.Point(2, 184);
            this.gridFinanceData.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.gridFinanceData.Name = "gridFinanceData";
            this.gridFinanceData.Size = new System.Drawing.Size(900, 489);
            this.gridFinanceData.TabIndex = 2;
            // 
            // btnSave
            // 
            this.btnSave.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSave.ForeColor = System.Drawing.Color.Black;
            this.btnSave.Location = new System.Drawing.Point(458, 116);
            this.btnSave.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(253, 28);
            this.btnSave.TabIndex = 1;
            this.btnSave.Text = "Save Cumulative Default Probabilities";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // btnLoad
            // 
            this.btnLoad.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLoad.ForeColor = System.Drawing.Color.Black;
            this.btnLoad.Location = new System.Drawing.Point(7, 116);
            this.btnLoad.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnLoad.Name = "btnLoad";
            this.btnLoad.Size = new System.Drawing.Size(253, 28);
            this.btnLoad.TabIndex = 0;
            this.btnLoad.Text = "Load Cumulative Default Probabilities";
            this.btnLoad.UseVisualStyleBackColor = true;
            this.btnLoad.Click += new System.EventHandler(this.btnLoad_Click);
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label33.ForeColor = System.Drawing.Color.Black;
            this.label33.Location = new System.Drawing.Point(42, 1111);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(234, 16);
            this.label33.TabIndex = 8;
            this.label33.Text = "China Patent App. # 201480024381";
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label32.ForeColor = System.Drawing.Color.Black;
            this.label32.Location = new System.Drawing.Point(42, 1088);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(159, 16);
            this.label32.TabIndex = 7;
            this.label32.Text = "U.S. Patent # 8,812,331";
            // 
            // btnDataDump
            // 
            this.btnDataDump.BackColor = System.Drawing.Color.Silver;
            this.btnDataDump.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDataDump.Location = new System.Drawing.Point(561, 207);
            this.btnDataDump.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnDataDump.Name = "btnDataDump";
            this.btnDataDump.Size = new System.Drawing.Size(148, 38);
            this.btnDataDump.TabIndex = 17;
            this.btnDataDump.Text = "(QA Only) Save Program Data To XML File";
            this.btnDataDump.UseVisualStyleBackColor = false;
            this.btnDataDump.Visible = false;
            this.btnDataDump.Click += new System.EventHandler(this.btnDataDump_Click);
            // 
            // btnPrint
            // 
            this.btnPrint.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPrint.ForeColor = System.Drawing.Color.Black;
            this.btnPrint.Location = new System.Drawing.Point(732, 171);
            this.btnPrint.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnPrint.Name = "btnPrint";
            this.btnPrint.Size = new System.Drawing.Size(75, 28);
            this.btnPrint.TabIndex = 16;
            this.btnPrint.Text = "Print Page";
            this.btnPrint.UseVisualStyleBackColor = true;
            this.btnPrint.Visible = false;
            this.btnPrint.Click += new System.EventHandler(this.btnPrint_Click);
            // 
            // button6
            // 
            this.button6.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(93)))), ((int)(((byte)(97)))), ((int)(((byte)(102)))));
            this.button6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button6.Location = new System.Drawing.Point(550, 110);
            this.button6.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.button6.Name = "button6";
            this.button6.Size = new System.Drawing.Size(176, 53);
            this.button6.TabIndex = 14;
            this.button6.Text = "test";
            this.button6.UseVisualStyleBackColor = false;
            this.button6.Visible = false;
            this.button6.Click += new System.EventHandler(this.button6_Click);
            // 
            // btnPrepopulateAnnualAndRun
            // 
            this.btnPrepopulateAnnualAndRun.BackColor = System.Drawing.Color.Silver;
            this.btnPrepopulateAnnualAndRun.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPrepopulateAnnualAndRun.Location = new System.Drawing.Point(561, 171);
            this.btnPrepopulateAnnualAndRun.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnPrepopulateAnnualAndRun.Name = "btnPrepopulateAnnualAndRun";
            this.btnPrepopulateAnnualAndRun.Size = new System.Drawing.Size(165, 28);
            this.btnPrepopulateAnnualAndRun.TabIndex = 9;
            this.btnPrepopulateAnnualAndRun.Text = "(QA Only) PrepopulateAnnual";
            this.btnPrepopulateAnnualAndRun.UseVisualStyleBackColor = false;
            this.btnPrepopulateAnnualAndRun.Visible = false;
            this.btnPrepopulateAnnualAndRun.Click += new System.EventHandler(this.btnPrepopulateAnnualAndRun_Click);
            // 
            // btnStart
            // 
            this.btnStart.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnStart.ForeColor = System.Drawing.Color.Black;
            this.btnStart.Location = new System.Drawing.Point(26, 241);
            this.btnStart.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnStart.Name = "btnStart";
            this.btnStart.Size = new System.Drawing.Size(75, 28);
            this.btnStart.TabIndex = 6;
            this.btnStart.Text = "Start";
            this.btnStart.UseVisualStyleBackColor = true;
            this.btnStart.Click += new System.EventHandler(this.btnStart_Click);
            // 
            // lblDateTime
            // 
            this.lblDateTime.AutoSize = true;
            this.lblDateTime.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDateTime.ForeColor = System.Drawing.Color.Black;
            this.lblDateTime.Location = new System.Drawing.Point(316, 1111);
            this.lblDateTime.Name = "lblDateTime";
            this.lblDateTime.Size = new System.Drawing.Size(0, 16);
            this.lblDateTime.TabIndex = 15;
            // 
            // lblLicensedTo
            // 
            this.lblLicensedTo.AutoSize = true;
            this.lblLicensedTo.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblLicensedTo.ForeColor = System.Drawing.Color.Black;
            this.lblLicensedTo.Location = new System.Drawing.Point(316, 1088);
            this.lblLicensedTo.Name = "lblLicensedTo";
            this.lblLicensedTo.Size = new System.Drawing.Size(85, 16);
            this.lblLicensedTo.TabIndex = 13;
            this.lblLicensedTo.Text = "Licensed to:";
            // 
            // pictureBox2
            // 
            this.pictureBox2.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox2.Image")));
            this.pictureBox2.Location = new System.Drawing.Point(26, 8);
            this.pictureBox2.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(291, 59);
            this.pictureBox2.TabIndex = 32;
            this.pictureBox2.TabStop = false;
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            // 
            // printDocument1
            // 
            this.printDocument1.PrintPage += new System.Drawing.Printing.PrintPageEventHandler(this.printDocument1_PrintPage);
            // 
            // printPreviewDialog1
            // 
            this.printPreviewDialog1.AutoScrollMargin = new System.Drawing.Size(0, 0);
            this.printPreviewDialog1.AutoScrollMinSize = new System.Drawing.Size(0, 0);
            this.printPreviewDialog1.ClientSize = new System.Drawing.Size(400, 300);
            this.printPreviewDialog1.Document = this.printDocument1;
            this.printPreviewDialog1.Enabled = true;
            this.printPreviewDialog1.Icon = ((System.Drawing.Icon)(resources.GetObject("printPreviewDialog1.Icon")));
            this.printPreviewDialog1.Name = "printPreviewDialog1";
            this.printPreviewDialog1.Visible = false;
            // 
            // picStart
            // 
            this.picStart.Image = ((System.Drawing.Image)(resources.GetObject("picStart.Image")));
            this.picStart.InitialImage = null;
            this.picStart.Location = new System.Drawing.Point(109, 248);
            this.picStart.Name = "picStart";
            this.picStart.Size = new System.Drawing.Size(20, 21);
            this.picStart.TabIndex = 39;
            this.picStart.TabStop = false;
            this.toolTip1.SetToolTip(this.picStart, "Start");
            this.picStart.Visible = false;
            this.picStart.Click += new System.EventHandler(this.picStart_Click);
            // 
            // picPrint
            // 
            this.picPrint.Image = ((System.Drawing.Image)(resources.GetObject("picPrint.Image")));
            this.picPrint.Location = new System.Drawing.Point(152, 247);
            this.picPrint.Name = "picPrint";
            this.picPrint.Size = new System.Drawing.Size(22, 16);
            this.picPrint.TabIndex = 40;
            this.picPrint.TabStop = false;
            this.toolTip1.SetToolTip(this.picPrint, "Print");
            this.picPrint.Click += new System.EventHandler(this.picPrint_Click);
            // 
            // pictureBoxRiskSolutions
            // 
            this.pictureBoxRiskSolutions.Image = ((System.Drawing.Image)(resources.GetObject("pictureBoxRiskSolutions.Image")));
            this.pictureBoxRiskSolutions.Location = new System.Drawing.Point(26, 128);
            this.pictureBoxRiskSolutions.Name = "pictureBoxRiskSolutions";
            this.pictureBoxRiskSolutions.Size = new System.Drawing.Size(144, 16);
            this.pictureBoxRiskSolutions.TabIndex = 35;
            this.pictureBoxRiskSolutions.TabStop = false;
            // 
            // pictureBoxERNIE
            // 
            this.pictureBoxERNIE.Image = ((System.Drawing.Image)(resources.GetObject("pictureBoxERNIE.Image")));
            this.pictureBoxERNIE.Location = new System.Drawing.Point(587, 46);
            this.pictureBoxERNIE.Name = "pictureBoxERNIE";
            this.pictureBoxERNIE.Size = new System.Drawing.Size(329, 27);
            this.pictureBoxERNIE.TabIndex = 36;
            this.pictureBoxERNIE.TabStop = false;
            // 
            // pictureBoxErnieDescription
            // 
            this.pictureBoxErnieDescription.Image = ((System.Drawing.Image)(resources.GetObject("pictureBoxErnieDescription.Image")));
            this.pictureBoxErnieDescription.Location = new System.Drawing.Point(26, 158);
            this.pictureBoxErnieDescription.Name = "pictureBoxErnieDescription";
            this.pictureBoxErnieDescription.Size = new System.Drawing.Size(410, 68);
            this.pictureBoxErnieDescription.TabIndex = 37;
            this.pictureBoxErnieDescription.TabStop = false;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(848, 149);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(32, 50);
            this.pictureBox1.TabIndex = 38;
            this.pictureBox1.TabStop = false;
            this.pictureBox1.Visible = false;
            // 
            // Main
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(1006, 1054);
            this.Controls.Add(this.label33);
            this.Controls.Add(this.picPrint);
            this.Controls.Add(this.label32);
            this.Controls.Add(this.picStart);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.btnDataDump);
            this.Controls.Add(this.pictureBoxERNIE);
            this.Controls.Add(this.btnPrepopulateAnnualAndRun);
            this.Controls.Add(this.button6);
            this.Controls.Add(this.btnPrint);
            this.Controls.Add(this.lblLicensedTo);
            this.Controls.Add(this.lblDateTime);
            this.Controls.Add(this.pictureBoxErnieDescription);
            this.Controls.Add(this.pictureBoxRiskSolutions);
            this.Controls.Add(this.pictureBox2);
            this.Controls.Add(this.btnStart);
            this.Controls.Add(this.tabControl1);
            this.Font = new System.Drawing.Font("Arial Narrow", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Name = "Main";
            this.Text = "HSB Credit Risk Model";
            this.Load += new System.EventHandler(this.Main_Load);
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.panel9.ResumeLayout(false);
            this.panel9.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridLossReserves)).EndInit();
            this.panel7.ResumeLayout(false);
            this.panel7.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridCreditRiskSavings)).EndInit();
            this.panel5.ResumeLayout(false);
            this.panel5.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridWithIns)).EndInit();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridNoInsurance)).EndInit();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.tabPage2.ResumeLayout(false);
            this.tabPage2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridInterestRates)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridFinanceData)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picStart)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picPrint)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxRiskSolutions)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxERNIE)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxErnieDescription)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.DataGridView gridFinanceData;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.Button btnLoad;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.TextBox InsuredEffectiveLoanPayment;
        private System.Windows.Forms.TextBox UninsuredAnnualLoanPayment;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label lblUninsuredLoanPayment;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label lblLoanTerm;
        private System.Windows.Forms.ComboBox CreditRatingInsurer;
        private System.Windows.Forms.ComboBox CreditRatingInsured;
        private System.Windows.Forms.TextBox olicyCoverageFactor;
        private System.Windows.Forms.TextBox IotalInsuredProjectSavings;
        private System.Windows.Forms.TextBox LoanTerm;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox LoanAmount;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.TextBox P10;
        private System.Windows.Forms.TextBox P20;
        private System.Windows.Forms.TextBox P30;
        private System.Windows.Forms.TextBox P40;
        private System.Windows.Forms.TextBox P50;
        private System.Windows.Forms.TextBox P60;
        private System.Windows.Forms.TextBox P70;
        private System.Windows.Forms.TextBox P80;
        private System.Windows.Forms.TextBox P90;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Button btnProjectSavingsDistributionFile;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.DataGridView gridNoInsurance;
        private System.Windows.Forms.Label lblNoIns;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label lblWithIns;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.DataGridView gridWithIns;
        private System.Windows.Forms.Panel panel7;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label lblCreditRisk;
        private System.Windows.Forms.DataGridView gridCreditRiskSavings;
        private System.Windows.Forms.Panel panel9;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.DataGridView gridLossReserves;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.Button btnStart;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.TextBox SavingsFromLoanPaymentReduction;
        private System.Windows.Forms.Button btnPrepopulateAnnualAndRun;
        private System.Windows.Forms.DataGridView gridInterestRates;
        private System.Windows.Forms.Button btnSaveInterestRates;
        private System.Windows.Forms.Button btnLoadInterestRates;
        private System.Windows.Forms.Button button6;
        private System.Windows.Forms.Label lblLicensedTo;
        private System.Windows.Forms.Label lblFinanceDataHeader;
        private System.Windows.Forms.Label lblDateTime;
        private System.Windows.Forms.Button btnPrint;
        private System.Drawing.Printing.PrintDocument printDocument1;
        private System.Windows.Forms.PrintPreviewDialog printPreviewDialog1;
        private System.Windows.Forms.ToolTip toolTip1;
        private System.Windows.Forms.RichTextBox rtf90;
        private System.Windows.Forms.RichTextBox rtf10;
        private System.Windows.Forms.RichTextBox rtf20;
        private System.Windows.Forms.RichTextBox rtf30;
        private System.Windows.Forms.RichTextBox rtf40;
        private System.Windows.Forms.RichTextBox rtf50;
        private System.Windows.Forms.RichTextBox rtf60;
        private System.Windows.Forms.RichTextBox rtf70;
        private System.Windows.Forms.RichTextBox rtf80;
        private System.Windows.Forms.SaveFileDialog saveFileDialog1;
        private System.Windows.Forms.Button btnDataDump;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.ComboBox cboPeriod;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.PictureBox pictureBoxRiskSolutions;
        private System.Windows.Forms.PictureBox pictureBoxERNIE;
        private System.Windows.Forms.PictureBox pictureBoxErnieDescription;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.PictureBox picStart;
        private System.Windows.Forms.PictureBox picPrint;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label9;
    }
}