﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;


namespace Solomon.CreditRiskModel.Presentation2
{
    static class Utilities
    {
        internal static DataTable GetReadOnlyDataTable(bool AddBMinus)
        {
            DataTable dataTableReadOnly = new DataTable();
            if(AddBMinus)
                dataTableReadOnly.Columns.Add(new DataColumn("B-", typeof(System.Double)));
            dataTableReadOnly.Columns.Add(new DataColumn("B", typeof(System.Double)));
            dataTableReadOnly.Columns.Add(new DataColumn("B+", typeof(System.Double)));
            dataTableReadOnly.Columns.Add(new DataColumn("BB-", typeof(System.Double)));
            dataTableReadOnly.Columns.Add(new DataColumn("BB", typeof(System.Double)));
            dataTableReadOnly.Columns.Add(new DataColumn("BB+", typeof(System.Double)));
            dataTableReadOnly.Columns.Add(new DataColumn("BBB-", typeof(System.Double)));
            dataTableReadOnly.Columns.Add(new DataColumn("BBB", typeof(System.Double)));
            dataTableReadOnly.Columns.Add(new DataColumn("BBB+", typeof(System.Double)));
            dataTableReadOnly.Columns.Add(new DataColumn("A-", typeof(System.Double)));
            dataTableReadOnly.Columns.Add(new DataColumn("A", typeof(System.Double)));
            dataTableReadOnly.Columns.Add(new DataColumn("A+", typeof(System.Double)));
            dataTableReadOnly.Columns.Add(new DataColumn("AA-", typeof(System.Double)));
            dataTableReadOnly.Columns.Add(new DataColumn("AA", typeof(System.Double)));
            dataTableReadOnly.Columns.Add(new DataColumn("AA+", typeof(System.Double)));
            return dataTableReadOnly;
        }

        internal static DataTable GetReadOnlyDataTableForStrings(bool AddBMinus)
        {
            DataTable dataTableReadOnly = new DataTable();
            if (AddBMinus)
                dataTableReadOnly.Columns.Add(new DataColumn("B-", typeof(System.String)));
            dataTableReadOnly.Columns.Add(new DataColumn("B", typeof(System.String)));
            dataTableReadOnly.Columns.Add(new DataColumn("B+", typeof(System.String)));
            dataTableReadOnly.Columns.Add(new DataColumn("BB-", typeof(System.String)));
            dataTableReadOnly.Columns.Add(new DataColumn("BB", typeof(System.String)));
            dataTableReadOnly.Columns.Add(new DataColumn("BB+", typeof(System.String)));
            dataTableReadOnly.Columns.Add(new DataColumn("BBB-", typeof(System.String)));
            dataTableReadOnly.Columns.Add(new DataColumn("BBB", typeof(System.String)));
            dataTableReadOnly.Columns.Add(new DataColumn("BBB+", typeof(System.String)));
            dataTableReadOnly.Columns.Add(new DataColumn("A-", typeof(System.String)));
            dataTableReadOnly.Columns.Add(new DataColumn("A", typeof(System.String)));
            dataTableReadOnly.Columns.Add(new DataColumn("A+", typeof(System.String)));
            dataTableReadOnly.Columns.Add(new DataColumn("AA-", typeof(System.String)));
            dataTableReadOnly.Columns.Add(new DataColumn("AA", typeof(System.String)));
            dataTableReadOnly.Columns.Add(new DataColumn("AA+", typeof(System.String)));
            return dataTableReadOnly;
        }

        //internal static void GridBindToList(ref System.Windows.Forms.DataGridView grid, List<double> list)
        //{
        //    DataTable dataTableReadOnly = GetReadOnlyDataTable();
        //    DataRow row = dataTableReadOnly.NewRow();
        //    for (int counter = 0; counter < list.Count; counter++)
        //    {
        //        row[counter] = list[counter];
        //    }
        //    dataTableReadOnly.Rows.Add(row);
        //    grid.DataSource = dataTableReadOnly;
        //}

        internal static DataTable GetReadOnlyDataTableForBindingShowingCurrency(List<double?> list, bool addBMinus)
        {
            DataTable dataTableReadOnly = GetReadOnlyDataTableForStrings(addBMinus);
            DataRow row = dataTableReadOnly.NewRow();
            for (int counter = 0; counter < list.Count; counter++)
            {
                //row[counter] = list[counter] ? DBNull.Value : list[counter];
                if(list[counter]==null)
                {
                    row[counter] =DBNull.Value;
                }
                else
                {
                    decimal rounded = Math.Round((decimal)list[counter], 0);
                    string formatted = rounded.ToString("C");
                    formatted = formatted.Remove(formatted.Length - 3);
                    row[counter] = formatted;
                }
            }
            dataTableReadOnly.Rows.Add(row);
            return dataTableReadOnly;
        }

        internal static DataTable GetReadOnlyDataTableForBindingShowingPercent(List<double?> list, bool addBMinus)
        {
            DataTable dataTableReadOnly = GetReadOnlyDataTableForStrings(addBMinus);
            DataRow row = dataTableReadOnly.NewRow();
            for (int counter = 0; counter < list.Count; counter++)
            {
                //row[counter] = list[counter] ? DBNull.Value : list[counter];
                if (list[counter] == null)
                {
                    row[counter] = DBNull.Value;
                }
                else
                {
                    decimal rounded = Math.Round((decimal)list[counter]*100, 0);
                    string formatted = rounded.ToString() + "%";
                    row[counter] = formatted; // list[counter];
                }
            }
            dataTableReadOnly.Rows.Add(row);
            return dataTableReadOnly;
        }

        internal static DataTable GetReadOnlyDataTableForBinding(List<double> list, bool addBMinus)
        {
            DataTable dataTableReadOnly = GetReadOnlyDataTable(addBMinus);
            DataRow row = dataTableReadOnly.NewRow();
            for (int counter = 0; counter < list.Count; counter++)
            {
                row[counter] = list[counter];
            }
            dataTableReadOnly.Rows.Add(row);
            return dataTableReadOnly;
        }

        internal static DataTable GetReadOnlyDataTableForDecimalBinding(List<decimal> list)
        {
            DataTable dataTableReadOnly = GetReadOnlyDataTable(false);
            DataRow row = dataTableReadOnly.NewRow();
            for (int counter = 0; counter < list.Count; counter++)
            {
                row[counter] = list[counter];
            }
            dataTableReadOnly.Rows.Add(row);
            return dataTableReadOnly;
        }

        public static  string GetFilenameFromPath(string path)
        {
            string[] pathParts = path.Split('\\');
            return pathParts[pathParts.Length - 1];
        }



    }
}
