﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
namespace Solomon.CreditRiskModel.Data.Writer
{
    public sealed class WriterFactory
    {
        private AbstractWriter _writer = null;
        public AbstractWriter Create(string fileType, string versionType) //sealed throws error
        {
            switch (fileType.ToUpper().Trim())
            {
                case "TEXT":
                    _writer = new TxtWriter();
                    break;
                case "KEY":
                    _writer = new TxtWriter();
                    break;
                default:
                    throw new Exception("Unexpected writer type");
                    break;
            }
            return _writer;
        }
    }
}
