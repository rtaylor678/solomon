﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.ComponentModel;
using Solomon.CreditRiskModel;

namespace Presentation
{
    /// <summary>
    /// Interaction logic for Window1.xaml
    /// </summary>
    public partial class Window1 : Window
    {
        private DataHolder _dataHolder = new DataHolder();
        private string _esdPath = string.Empty;

        public Window1()
        {
            InitializeComponent();

            BindInXaml();
        }

        private void BindInXaml()
        {
            //base.DataContext = new Person
            //{
            //    FirstName = "Josh",
            //    LastName = "Smith"
            //};
            //base.DataContext = _person;
            base.DataContext = _dataHolder;
        }

        private void BindingTest_Click(object sender, RoutedEventArgs e)
        {

            _dataHolder.IntRateInsurer = .5;
            _dataHolder.LoanAmt = 100000;

            _dataHolder.ProjectSavingsDistribution = new List<decimal>();
            _dataHolder.ProjectSavingsDistribution.Add(142006.234375m);
            _dataHolder.ProjectSavingsDistribution.Add(146873.90625m);
            _dataHolder.ProjectSavingsDistribution.Add(150326.828125m);
            _dataHolder.ProjectSavingsDistribution.Add(153320.015625m);
            _dataHolder.ProjectSavingsDistribution.Add(156106.96875m);
            _dataHolder.ProjectSavingsDistribution.Add(158797.640625m);
            _dataHolder.ProjectSavingsDistribution.Add(161541.03125m);
            _dataHolder.ProjectSavingsDistribution.Add(164611.671875m);
            _dataHolder.ProjectSavingsDistribution.Add(168171.203125m);

            _dataHolder.ProjectSavingsCreditEnhancementNoInsurance = new List<double?>();
            _dataHolder.ProjectSavingsCreditEnhancementNoInsurance.Add(null);
            _dataHolder.ProjectSavingsCreditEnhancementNoInsurance.Add(null);
            int counter = 0;
            for (counter = 2; counter < 14; counter++)
            {
                _dataHolder.ProjectSavingsCreditEnhancementNoInsurance.Add(counter);
            }

            _dataHolder.ProjectSavingsCreditEnhancementWithInsurance = new List<double?>();
            for (counter = 0; counter < 14; counter++)
            {
                _dataHolder.ProjectSavingsCreditEnhancementWithInsurance.Add(counter * 2);
            }

            _dataHolder.CreditRiskSavings = new List<double?>();
            for (counter = 0; counter < 14; counter++)
            {
                _dataHolder.CreditRiskSavings.Add(counter);
            }

            _dataHolder.LossReserves = new List<double?>();
            for (counter = 0; counter < 15; counter++)
            {
                //if(counter>3)
                _dataHolder.LossReserves.Add(counter + 5);
            }

            _dataHolder.SavingsFromLoanPaymentReduction = 100000;


            //_dataHolder.TestList.Add(2.1);
            //_dataHolder.TestArr = new double[] { 5, 6 };
            _dataHolder.LoanAmt += 1000;
            _dataHolder.Nterm += 5;
            _dataHolder.TotalInsuredProjectSavings += 4000000;
            _dataHolder.CoverageRatio+=100;
            _dataHolder.UninsuredLoanPayment += 1980734;
            _dataHolder.InsuredEffectiveLoanPayment += 1910311;
            _dataHolder.SavingsFromLoanPaymentReduction += 50000;            
            List<decimal> newProjectSavingsDistribution = new List<decimal>();
            counter = 0;
            for (counter = 0; counter < 9; counter++)
            {
                newProjectSavingsDistribution.Add(_dataHolder.ProjectSavingsDistribution[counter] + 1000);
            }
            _dataHolder.ProjectSavingsDistribution = newProjectSavingsDistribution;

            List<double?> newDouble = new List<double?>();
            for (counter = 0; counter < 14; counter++)
            {
                newDouble.Add(_dataHolder.ProjectSavingsCreditEnhancementNoInsurance[counter] == null ? null : _dataHolder.ProjectSavingsCreditEnhancementNoInsurance[counter]+1);
            }
            _dataHolder.ProjectSavingsCreditEnhancementNoInsurance = newDouble;

            newDouble = new List<double?>();
            for (counter = 0; counter < 14; counter++)
            {
                newDouble.Add(_dataHolder.ProjectSavingsCreditEnhancementWithInsurance[counter] == null ? null : _dataHolder.ProjectSavingsCreditEnhancementWithInsurance[counter] *2);
            }
            _dataHolder.ProjectSavingsCreditEnhancementWithInsurance = newDouble;

            newDouble = new List<double?>();
            for (counter = 0; counter < 14; counter++)
            {
                newDouble.Add(_dataHolder.CreditRiskSavings[counter] == null ? null : _dataHolder.CreditRiskSavings[counter] +3);
            }
            _dataHolder.CreditRiskSavings = newDouble;

            newDouble = new List<double?>();
            for (counter = 0; counter < 15; counter++)
            {
                newDouble.Add(_dataHolder.LossReserves[counter] == null ? null : _dataHolder.LossReserves[counter] + 5);
            }
            _dataHolder.LossReserves = newDouble;


            string testCbos = string.Empty;
            testCbos = InsuredCreditRating.SelectedItem==null? string.Empty:InsuredCreditRating.SelectedItem.ToString();
            testCbos += InsurerCreditRating.SelectedItem == null ? string.Empty : InsurerCreditRating.SelectedItem.ToString();
            this.Title = testCbos;
            /*
            _dataHolder.IntRateInsurer += 0.1;
            

            List<double> newList = new List<double>();
            newList.Add(_dataHolder.TestList[0] + 1);
            //newList.Add(_dataHolder.TestList[1] + .1);
            _dataHolder.TestList = newList;

            double[] newArr = new double[2];
            newArr[0] = _dataHolder.TestArr[0] + 1;

            _dataHolder.TestArr = newArr;
             * */
        }

        private void HSB_Click(object sender, RoutedEventArgs e)
        {
            //HSB button
            //this.Title = _dataHolder.Nterm.ToString();

            // Name="InsuredRating3" 
            


            ErrorsInfo.Reset();
            ClearDataholderProperties();
            _dataHolder.ESDDataPath = _esdPath ;
            _dataHolder.SelectedInsuredCreditRating = InsuredCreditRating.SelectedItem == null ? string.Empty : InsuredCreditRating.SelectedItem.ToString().Trim();
            InsuredRating1.Content = _dataHolder.SelectedInsuredCreditRating + " >";
            InsuredRating2.Content = _dataHolder.SelectedInsuredCreditRating + " >";
            InsuredRating3.Content = _dataHolder.SelectedInsuredCreditRating + " >";
            _dataHolder.SelectedInsurerCreditRating = InsurerCreditRating.SelectedItem == null ? string.Empty : InsurerCreditRating.SelectedItem.ToString().Trim();
            Solomon.CreditRiskModel.Components.CreditRiskManager creditRiskManager = 
                new Solomon.CreditRiskModel.Components.CreditRiskManager(_dataHolder);
            if (!creditRiskManager.Analyze(PeriodType.Text, @"C:\ErnieKey.txt"))
            {
                StringBuilder errors = new StringBuilder();
                errors.Append("Credit Risk Model errors occurred: ");
                foreach(string error in ErrorsInfo.GetAllErrors())
                {   
                    errors.Append(error);
                }
                MessageBox.Show(errors.ToString());
            }
            BindFinanceDataCumulativeDefaultProbabilitiesYear(ref _dataHolder);
            //If binding problem on List<T>, then don't do _dataHolder = new List<T> and populate that; instead make new List<T> and populate it, then assign it to _dataHolder
        }

        private void BindFinanceDataCumulativeDefaultProbabilitiesYear(ref DataHolder _dataHolder)
        {
            FinanceDataYear0I0.Text = _dataHolder.FinanceDataCumulativeDefaultProbabilitiesYear[0, 0].ToString();
            FinanceDataYear0I1.Text = _dataHolder.FinanceDataCumulativeDefaultProbabilitiesYear[0, 1].ToString();
        }


        private void ClearDataholderProperties()
        {
            //_dataHolder.InsurAmount = 0;
            _dataHolder.InsuredEffectiveLoanPayment = 0;
            _dataHolder.IntRateBorrower = 0;
            _dataHolder.IntRateInsurer = 0;
            _dataHolder.SavingsFromLoanPaymentReduction = 0;
            _dataHolder.UninsuredLoanPayment = 0;
            if (PeriodType.Text == "YEAR")
            {
                _dataHolder.FinanceDataCumulativeDefaultProbabilitiesYear = new double[15, 16];
            }
            else if(PeriodType.Text=="MONTH")
            {
                _dataHolder.FinanceDataCumulativeDefaultProbabilitiesMonth = new double[180, 15];
            }

            _dataHolder.LossCostsNoInsurance = new double[102, 15];
            _dataHolder.LossCostsWithInsurance   = new double[102, 15];
            _dataHolder.FinanceDataInterestRates = new double[15];
            _dataHolder.IntRateArray = new double[15];
            //_dataHolder.TestArr = new double[];
            _dataHolder.CreditRiskBorrower =0;
            _dataHolder.CreditRiskInsurer  =0;
            _dataHolder.ProjectSavingsDistribution = new List<decimal>();
            _dataHolder.CreditRiskSavings = new List<double?>();
            _dataHolder.LossReserves = new List<double?>();
            _dataHolder.ProjectSavingsCreditEnhancementNoInsurance = new List<double?>();
            _dataHolder.ProjectSavingsCreditEnhancementWithInsurance = new List<double?>();
            _dataHolder.EnergySvgsDist = new List<double>();
            //_dataHolder.ShortRatings = new List<string>();
            //_dataHolder.SPRatings = new string[];
        }

        private void ESDDataPath_Click(object sender, RoutedEventArgs e)
        {
            //http://www.wpf-tutorial.com/dialogs/the-openfiledialog/
            Microsoft.Win32.OpenFileDialog dialog = new Microsoft.Win32.OpenFileDialog();
            if (dialog.ShowDialog() == true && dialog.FileName.Length>2)
            {
                _dataHolder.ESDDataPath = dialog.FileName;
                string[] pathParts = dialog.FileName.Split('\\');
                ESDDataPath.Content = pathParts[pathParts.Length-1];
            }
        }

        //private void PrePopulate_Click(object sender, RoutedEventArgs e)
        //{
        //    PrePopulateData();
        //}
        private void PrePopulateDataAnnual_Click(object sender, RoutedEventArgs e)
        {
            //ANNUAL

            LoanAmt.Text= "8000000";
            Nterm.Text=  "5";
            TotalInsuredProjectSavings.Text = "4000000";
            CoverageRatio.Text = "1"; // ".88";
            InsuredCreditRating.SelectedIndex=6;
            InsurerCreditRating.SelectedIndex = 14; //AA+
            PeriodType.SelectedIndex = 0;
            _esdPath = @"C:\AnnTest.txt";            
        }

        private void PrePopulateDataMonth_Click(object sender, RoutedEventArgs e)
        {
            //MONTH

            LoanAmt.Text = "8000000";
            Nterm.Text = "60";
            TotalInsuredProjectSavings.Text = "8000000";
            CoverageRatio.Text = ".88";
            InsuredCreditRating.SelectedIndex = 5; //BB+
            InsurerCreditRating.SelectedIndex = 14; //AA+
            PeriodType.SelectedIndex = 1;
            _esdPath = @"C:\MonthTest1.txt";

        }


    }
}
