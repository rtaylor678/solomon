﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Solomon.CreditRiskModel.Components;
using Solomon.CreditRiskModel;
using System.Collections.Generic;
using System.Xml;
using System.Xml.Serialization;

namespace Solomon.CreditRiskModel.Components.Tests
{
    [TestClass]
    public class MonthlyTests
    {
        private DataHolder _data = null;
        private string _keyFilePath = @"C:\Users\sfb.DC1\Documents\Credit Risk Model\SOLOMON.Key";
        private string _keyEncryptionKey = "884587801A43486";
        private byte[] _rfcEncryptionKey = new byte[] { 74, 40, 76, 81, 70, 35, 14, 86, 70, 37, 61, 87, 62 };
        private string _keyEncryptionESD = "F545A7BA84FA4E8";
        private byte[] _rfcEncryptionESD = new byte[] { 64, 52, 67, 50, 59, 48, 98, 22, 47, 26, 29, 29, 82 };
        private string _keyEncryptionInterestRates = "DEE33F5D39414A3";
        private byte[] _rfcEncryptionInterestRates = new byte[] { 98, 10, 226, 95, 90, 243, 33, 106, 99, 66, 70, 57, 100 };
        private string _esdPath =  @"C:\Users\sfb.DC1\Documents\Credit Risk Model\ESD-MonthTestEncrypted.rdd";



        private DataHolder PopulateDataHolder1()
        {
            DataHolder tempData = new DataHolder();
            
            tempData.FinanceDataInterestRates = new double[] { 10.345, 10, 9.5, 9.345, 9, 8.735, 7.565, 6.675, 5.785, 5.53, 5.275, 5.02, 4.96166666666667, 4.90333333333333, 4.845 };
            tempData.SelectedInsuredCreditRating = "BB+";
            tempData.SelectedInsurerCreditRating = "AA+";
            FinanceDataManager fdm = new FinanceDataManager();
            if (!fdm.LoadFinanceData(ref tempData, @"C:\Users\sfb.DC1\Documents\Credit Risk Model\Cumulative Default Probabilities Month.csv"))
                throw new Exception("Error in fdm.LoadFinanceData in  PopulateDataHolder1()");
            return tempData;
        }


        [TestMethod]
        public void PopulateFinanceDataCumulativeDefaultProbabilitiesTest()
        {
            DataHolder tempData = PopulateDataHolder1();
            Solomon.CreditRiskModel.Components.Data.AnalysisData analysisData =
                new Data.AnalysisData(Enums.PeriodTypes.MONTH, _keyFilePath, _keyEncryptionKey, _rfcEncryptionKey, _keyEncryptionESD, _rfcEncryptionESD, _keyEncryptionInterestRates, _rfcEncryptionInterestRates);

            Assert.IsTrue(analysisData.FinanceDataCumulativeDefaultProbabilitiesValueAt(ref tempData, 0, 0) == .625);
            Assert.IsTrue(analysisData.FinanceDataCumulativeDefaultProbabilitiesValueAt(ref tempData, 89, 7) == 2.61);
            Assert.IsTrue(analysisData.FinanceDataCumulativeDefaultProbabilitiesValueAt(ref tempData, 179, 14) == 0.85);
        }

        [TestMethod]
        public void BorrowerAndInsurerIntRateArraysTest()
        {

            _data = PopulateDataHolder1();

            object[] constructor = new object[] { Enums.PeriodTypes.MONTH, _keyFilePath, _keyEncryptionKey, _rfcEncryptionKey, _keyEncryptionESD, _rfcEncryptionESD, _keyEncryptionInterestRates, _rfcEncryptionInterestRates };
            object[] args = new object[1] { _data };
            PrivateObject privateObject = new PrivateObject(typeof(Solomon.CreditRiskModel.Components.Data.AnalysisData), constructor);
            privateObject.Invoke("PopulateBorrowerAndInsurerIntRateArrays", args);
            _data = (DataHolder)args[0];

            Assert.IsTrue(_data.CreditRiskBorrower == 5);
            Assert.IsTrue(_data.CreditRiskInsurer == 14);

            Assert.IsTrue(_data.IntRateBorrower == 0.0072791666666666664);
            Assert.IsTrue(_data.IntRateInsurer == 0.0040375);

            Assert.IsTrue(_data.IntRateArray[0] == 0.86208333333333342);
            Assert.IsTrue(_data.IntRateArray[1] == 0.83333333333333337);
            Assert.IsTrue(_data.IntRateArray[2] == 0.79166666666666663);
            Assert.IsTrue(_data.IntRateArray[3] == 0.77875);
            Assert.IsTrue(_data.IntRateArray[4] == 0.75);
            Assert.IsTrue(_data.IntRateArray[5] == 0.72791666666666666);
            Assert.IsTrue(_data.IntRateArray[6] == 0.63041666666666674);
            Assert.IsTrue(_data.IntRateArray[7] == 0.55625);
            Assert.IsTrue(_data.IntRateArray[8] == 0.48208333333333336);
            Assert.IsTrue(_data.IntRateArray[9] == 0.46083333333333337);
            Assert.IsTrue(_data.IntRateArray[10] == 0.43958333333333338);
            Assert.IsTrue(_data.IntRateArray[11] == 0.41833333333333328);
            Assert.IsTrue(_data.IntRateArray[12] == 0.41347222222222246);
            Assert.IsTrue(_data.IntRateArray[13] == 0.40861111111111081);
            Assert.IsTrue(_data.IntRateArray[14] == 0.40375);

        }

        [TestMethod]
        public void InsuredAndUninsuredAnnualLoanPmtsTest()
        {
            if (_data == null)
            {
                _data = PopulateDataHolder1();
                _data.IntRateBorrower = 0.0072791666666666664;
                _data.IntRateInsurer = 0.0040375;
                _data.Nterm = 60;
                _data.LoanAmt = 8000000;
                _data.CoverageRatio = .88;
                _data.TotalInsuredProjectSavings = 8000000;
            }
            Solomon.CreditRiskModel.Components.Data.AnalysisData analysis =
                new Data.AnalysisData(Enums.PeriodTypes.MONTH, _keyFilePath, _keyEncryptionKey, _rfcEncryptionKey, _keyEncryptionESD, _rfcEncryptionESD, _keyEncryptionInterestRates, _rfcEncryptionInterestRates);
            analysis.PopulateUninsuredLoanPayment(ref _data);

            //NOTE: Excel returns 1980733.67532334. But .Net yields 1980733.161058086.
            //Because the exponent calc gives 0.694456571991399 in .net, but Excel gives  0.694456651320775 
            Assert.IsTrue(Within1(_data.UninsuredLoanPayment, 165039.82993755635));

            analysis.PopulateInsuredEffectiveLoanPayment(ref _data);
            Assert.IsTrue(Within1(_data.InsuredEffectiveLoanPayment, 152158.91767668203));
        }


        [TestMethod]
        public void PopulateEnergySvgsDistArrayTest()
        {
            if (_data == null)
            {
                _data = PopulateDataHolder1();
                _data.IntRateBorrower = 0.0072791666666666664;
                _data.IntRateInsurer = 0.0040375;
                _data.Nterm = 60;
                _data.LoanAmt = 8000000;
                _data.CoverageRatio = .88;
                _data.TotalInsuredProjectSavings = 8000000;

                _data.UninsuredLoanPayment = 165039.82993755635;
                _data.InsuredEffectiveLoanPayment = 152158.91767668203;
                _data.ESDDataPath =_esdPath;
            }
            object[] constructor = new object[] { Enums.PeriodTypes.MONTH, _keyFilePath, _keyEncryptionKey, _rfcEncryptionKey, _keyEncryptionESD, _rfcEncryptionESD, _keyEncryptionInterestRates, _rfcEncryptionInterestRates };
            object[] args = new object[1] { _data };
            PrivateObject privateObject = new PrivateObject(typeof(Solomon.CreditRiskModel.Components.Data.AnalysisData), constructor);
            privateObject.Invoke("PopulateEnergySvgsDistArray", args);
            _data = (DataHolder)args[0];
            Assert.IsTrue(_data.EnergySvgsDist[0] == 0);
            Assert.IsTrue(Within1(_data.EnergySvgsDist[1], 130589.5));
            Assert.IsTrue(Within1(_data.EnergySvgsDist[50], 156107));
            Assert.IsTrue(Within1(_data.EnergySvgsDist[100], 181705.2));
            Assert.IsTrue(_data.EnergySvgsDist[101] == 0);
        }

        [TestMethod]
        public void ProjectSavingsDistributionTest()
        {
            if (_data == null)
            {
                _data = PopulateDataHolder1();
                _data.IntRateBorrower = 0.0072791666666666664;
                _data.IntRateInsurer = 0.0040375;
                _data.Nterm = 60;
                _data.LoanAmt = 8000000;
                _data.CoverageRatio = .88;
                _data.TotalInsuredProjectSavings = 8000000;

                _data.UninsuredLoanPayment = 165039.82993755635;
                _data.InsuredEffectiveLoanPayment = 152158.91767668203;
                _data.ESDDataPath = _esdPath;

                PopulateEnergySvgsDistArrayTest();
            }
            Solomon.CreditRiskModel.Components.Data.AnalysisData analysis =
                new Data.AnalysisData(Enums.PeriodTypes.MONTH, _keyFilePath, _keyEncryptionKey, _rfcEncryptionKey, _keyEncryptionESD, _rfcEncryptionESD, _keyEncryptionInterestRates, _rfcEncryptionInterestRates);
            Assert.IsTrue( analysis.PopulateProjectSavingsDistribution(ref _data));

            Assert.IsTrue(Within1((double)_data.ProjectSavingsDistribution[0], 142006.2344));
            Assert.IsTrue(Within1((double)_data.ProjectSavingsDistribution[1], 146873.9062));
            Assert.IsTrue(Within1((double)_data.ProjectSavingsDistribution[2], 150326.8281));
            Assert.IsTrue(Within1((double)_data.ProjectSavingsDistribution[3], 153320.0156));
            Assert.IsTrue(Within1((double)_data.ProjectSavingsDistribution[4], 156106.9688));
            Assert.IsTrue(Within1((double)_data.ProjectSavingsDistribution[5], 158797.6406));
            Assert.IsTrue(Within1((double)_data.ProjectSavingsDistribution[6], 161541.0312));
            Assert.IsTrue(Within1((double)_data.ProjectSavingsDistribution[7], 164611.6719));
            Assert.IsTrue(Within1((double)_data.ProjectSavingsDistribution[8], 168171.2031));
        }

        [TestMethod]
        public void PopulateLossCostsNoInsuranceTest()
        {
            if (_data == null)
            {
                _data = PopulateDataHolder1();
                BorrowerAndInsurerIntRateArraysTest();
                _data.IntRateBorrower = 0.0072791666666666664;
                _data.IntRateInsurer = 0.0040375;
                _data.Nterm = 60;
                _data.LoanAmt = 8000000;
                _data.CoverageRatio = .88;
                _data.TotalInsuredProjectSavings = 8000000;

                _data.UninsuredLoanPayment = 165039.82993755635;
                _data.InsuredEffectiveLoanPayment = 152158.91767668203;
                _data.ESDDataPath = _esdPath;
                                
                PopulateEnergySvgsDistArrayTest();
                ProjectSavingsDistributionTest();
                
                _data.CreditRiskBorrower = 5;
                _data.CreditRiskInsurer = 14;

            }
            object[] constructor = new object[] { Enums.PeriodTypes.MONTH, _keyFilePath, _keyEncryptionKey, _rfcEncryptionKey, _keyEncryptionESD, _rfcEncryptionESD, _keyEncryptionInterestRates, _rfcEncryptionInterestRates };
            object[] args = new object[1] { _data };
            PrivateObject privateObject = new PrivateObject(typeof(Solomon.CreditRiskModel.Components.Data.AnalysisData), constructor);
            privateObject.Invoke("PopulateLossCostsNoInsurance", args);
            _data = (DataHolder)args[0];

            Assert.IsTrue(_data.ProjectSavingsCreditEnhancementNoInsurance[0] == null);
            Assert.IsTrue(_data.ProjectSavingsCreditEnhancementNoInsurance[1] == null);
            Assert.IsTrue(_data.ProjectSavingsCreditEnhancementNoInsurance[2] == null);
            Assert.IsTrue(_data.ProjectSavingsCreditEnhancementNoInsurance[3] == null);
            Assert.IsTrue(_data.ProjectSavingsCreditEnhancementNoInsurance[4] == null);
            Assert.IsTrue(Within6Decimals((double)_data.ProjectSavingsCreditEnhancementNoInsurance[5] , (double)0.996663689916326));
            Assert.IsTrue(Within6Decimals((double)_data.ProjectSavingsCreditEnhancementNoInsurance[6] ,(double) 0.991839092949016));
            Assert.IsTrue(Within6Decimals((double)_data.ProjectSavingsCreditEnhancementNoInsurance[7] , (double) 0.990737351483586));
            Assert.IsTrue(Within6Decimals((double)_data.ProjectSavingsCreditEnhancementNoInsurance[8] , (double) 0.901276923339286));
            Assert.IsTrue(Within6Decimals((double)_data.ProjectSavingsCreditEnhancementNoInsurance[9] , (double) 0.830882459296693));
            Assert.IsTrue(Within6Decimals((double)_data.ProjectSavingsCreditEnhancementNoInsurance[10] , (double) 0.771525453858632));
            Assert.IsTrue(Within6Decimals((double)_data.ProjectSavingsCreditEnhancementNoInsurance[11] , (double) 0.644904082461302));
            Assert.IsTrue(Within6Decimals((double)_data.ProjectSavingsCreditEnhancementNoInsurance[12], (double)0.511155623745838));
            Assert.IsTrue(_data.ProjectSavingsCreditEnhancementNoInsurance[13] == null);
        }

        private bool Within6Decimals(double first, double second)
        {
            string firstArg = first.ToString();
            firstArg = firstArg.Substring(1, 7);
            return firstArg == second.ToString().Substring(1, 7);
        }

    
        [TestMethod]
        public void PopulateLossCostsWithInsuranceTest()
        {
            if (_data == null)
            {
                _data = PopulateDataHolder1();
                BorrowerAndInsurerIntRateArraysTest();
                _data.IntRateBorrower = 0.0072791666666666664;
                _data.IntRateInsurer = 0.0040375;
                _data.Nterm = 60;
                _data.LoanAmt = 8000000;
                _data.CoverageRatio = .88;
                _data.TotalInsuredProjectSavings = 8000000;

                _data.UninsuredLoanPayment = 165039.82993755635;
                _data.InsuredEffectiveLoanPayment = 152158.91767668203;
                _data.ESDDataPath = _esdPath;

                PopulateEnergySvgsDistArrayTest();
                ProjectSavingsDistributionTest();

                _data.CreditRiskBorrower = 5;
                _data.CreditRiskInsurer = 14;

                _data.TotalInsuredProjectSavings = 8000000;

            }
            object[] constructor = new object[] { Enums.PeriodTypes.MONTH, _keyFilePath, _keyEncryptionKey, _rfcEncryptionKey, _keyEncryptionESD, _rfcEncryptionESD, _keyEncryptionInterestRates, _rfcEncryptionInterestRates };
            object[] args = new object[1] { _data };
            PrivateObject privateObject = new PrivateObject(typeof(Solomon.CreditRiskModel.Components.Data.AnalysisData), constructor);
            privateObject.Invoke("PopulateLossCostsWithInsurance", args);
            _data = (DataHolder)args[0];

            Assert.IsTrue(Within1((double)_data.LossCostsWithInsurance.Rows[0][0] ,0));
            Assert.IsTrue(Within1((double)_data.LossCostsWithInsurance.Rows[0][5], 171155.95854861577));
            Assert.IsTrue(Within1((double)_data.LossCostsWithInsurance.Rows[6][10], 1658.1783090867893));
            Assert.IsTrue(Within1((double)_data.LossCostsWithInsurance.Rows[24][3], 0.0));
            Assert.IsTrue(Within1((double)_data.LossCostsWithInsurance.Rows[30][8], 158.21754314104058));
            Assert.IsTrue(Within1((double)_data.LossCostsWithInsurance.Rows[47][3], 0.0));
            Assert.IsTrue(Within1((double)_data.LossCostsWithInsurance.Rows[101][5], 39173.558548615765));
            Assert.IsTrue(Within1((double)_data.LossCostsWithInsurance.Rows[101][6], 29179.867486025058));
            Assert.IsTrue(Within1((double)_data.LossCostsWithInsurance.Rows[101][7], 14176.68824495393));
            Assert.IsTrue(Within1((double)_data.LossCostsWithInsurance.Rows[101][8], 10790.371239755619));
            Assert.IsTrue(Within1((double)_data.LossCostsWithInsurance.Rows[101][9], 5676.4439402054632));
            Assert.IsTrue(Within1((double)_data.LossCostsWithInsurance.Rows[101][10], 4759.27530439929));
            Assert.IsTrue(Within1((double)_data.LossCostsWithInsurance.Rows[101][11], 4201.2538180553966));
            Assert.IsTrue(Within1((double)_data.LossCostsWithInsurance.Rows[101][12], 3220.9524996353389));
            Assert.IsTrue(Within1((double)_data.LossCostsWithInsurance.Rows[101][13], 2286.844049444413));
            Assert.IsTrue(Within1((double)_data.LossCostsWithInsurance.Rows[101][14], 1198.7547411568412));
           
            Assert.IsTrue(_data.ProjectSavingsCreditEnhancementWithInsurance[0] == null);
            Assert.IsTrue(_data.ProjectSavingsCreditEnhancementWithInsurance[1] == null);
            Assert.IsTrue(_data.ProjectSavingsCreditEnhancementWithInsurance[2] == null);
            Assert.IsTrue(_data.ProjectSavingsCreditEnhancementWithInsurance[3] == null);
            Assert.IsTrue(_data.ProjectSavingsCreditEnhancementWithInsurance[4] == null);
            Assert.IsTrue(Within1((double)_data.ProjectSavingsCreditEnhancementWithInsurance[5], 0.99713264921580647));
            Assert.IsTrue(Within1((double)_data.ProjectSavingsCreditEnhancementWithInsurance[6], 0.99265543369492826));
            Assert.IsTrue(Within1((double)_data.ProjectSavingsCreditEnhancementWithInsurance[7], 0.99165689369131071));
            Assert.IsTrue(Within1((double)_data.ProjectSavingsCreditEnhancementWithInsurance[8], 0.99009343628982827));
            Assert.IsTrue(Within1((double)_data.ProjectSavingsCreditEnhancementWithInsurance[9], 0.98215938433154493));
            Assert.IsTrue(Within1((double)_data.ProjectSavingsCreditEnhancementWithInsurance[10], 0.97053186868283625));
            Assert.IsTrue(Within1((double)_data.ProjectSavingsCreditEnhancementWithInsurance[11], 0.93840953720176956));
            Assert.IsTrue(Within1((double)_data.ProjectSavingsCreditEnhancementWithInsurance[12], 0.88523434778294663));
            Assert.IsTrue(_data.ProjectSavingsCreditEnhancementWithInsurance[13] == null);



            Assert.IsTrue(_data.LossReserves[0] == null);
            Assert.IsTrue(_data.LossReserves[1] == null);
            Assert.IsTrue(_data.LossReserves[2] == null);
            Assert.IsTrue(_data.LossReserves[3] == null);
            Assert.IsTrue(_data.LossReserves[4] == null);
            Assert.IsTrue(Within1((double)_data.LossReserves[5], 171155.95854861577));
            Assert.IsTrue(Within1((double)_data.LossReserves[6], 129036.40081935836));
            Assert.IsTrue(Within1((double)_data.LossReserves[7], 63268.954911620589));
            Assert.IsTrue(Within1((double)_data.LossReserves[8], 48601.03790642228));
            Assert.IsTrue(Within1((double)_data.LossReserves[9], 25634.843940205472));
            Assert.IsTrue(Within1((double)_data.LossReserves[10], 21549.675304399298));
            Assert.IsTrue(Within1((double)_data.LossReserves[11], 19073.253818055397));
            Assert.IsTrue(Within1((double)_data.LossReserves[12], 14631.619166302007));
            Assert.IsTrue(Within1((double)_data.LossReserves[13], 10394.577382777747));
            Assert.IsTrue(Within1((double)_data.LossReserves[14], 5452.088074490176));

            Assert.IsTrue(_data.CreditRiskSavings[0] == null);
            Assert.IsTrue(_data.CreditRiskSavings[1] == null);
            Assert.IsTrue(_data.CreditRiskSavings[2] == null);
            Assert.IsTrue(_data.CreditRiskSavings[3] == null);
            Assert.IsTrue(_data.CreditRiskSavings[4] == null);
            Assert.IsTrue(Within1((double)_data.CreditRiskSavings[5], 42119.36719));
            Assert.IsTrue(Within1((double)_data.CreditRiskSavings[6], 107886.5313));
            Assert.IsTrue(Within1((double)_data.CreditRiskSavings[7], 122554.3828));
            Assert.IsTrue(Within1((double)_data.CreditRiskSavings[8], 145520.4688));
            Assert.IsTrue(Within1((double)_data.CreditRiskSavings[9], 149605.625));
            Assert.IsTrue(Within1((double)_data.CreditRiskSavings[10], 152082.0313));
            Assert.IsTrue(Within1((double)_data.CreditRiskSavings[11], 156523.6563));
            Assert.IsTrue(Within1((double)_data.CreditRiskSavings[12], 160760.6719));
            Assert.IsTrue(_data.CreditRiskSavings[13] == null);
             

        }
        private bool Within1(double toCompare, double compareWith)
        {
            if (toCompare < compareWith + 1 && toCompare > compareWith - 1)
            {
                return true;
            }
            else
            { return false; }
        }

    }
}
