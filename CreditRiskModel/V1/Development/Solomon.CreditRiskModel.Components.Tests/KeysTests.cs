﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Solomon.CreditRiskModel.Components;
using Solomon.CreditRiskModel;
using System.Collections.Generic;
using System.IO;
using Solomon.CreditRiskModel.KeyComponents;
using Solomon.CreditRiskModel.Data;
using Solomon.CreditRiskModel.Data.Writer;

namespace Solomon.CreditRiskModel.Components.Tests
{
    [TestClass]
    public class KeysTests
    {
        private string _keyEncryptionKey = "884587801A43486";
        private byte[] _rfcEncryptionKey = new byte[] { 74, 40, 76, 81, 70, 35, 14, 86, 70, 37, 61, 87, 62 };
        private string _keyEncryptionESD = "F545A7BA84FA4E8";
        private byte[] _rfcEncryptionESD = new byte[] { 64, 52, 67, 50, 59, 48, 98, 22, 47, 26, 29, 29, 82 };
        private string _keyEncryptionInterestRates = "DEE33F5D39414A3";
        private byte[] _rfcEncryptionInterestRates = new byte[] { 98, 10, 226, 95, 90, 243, 33, 106, 99, 66, 70, 57, 100 };

        [TestMethod]
        public void TestKeyGen()
        {
            //string testKey = "jc8T08LhbPTq0co+jNDLQ3A37jzcSkfbNBMGb3x+Ekw=";
            string company = "SOLOMON";
            IFiles keyGenerator = new FileManager();
            //build expired key
            string effectiveDate = "07/01/2016";
            string expirationDate = "07/01/2016";
            object[] args = new object[5] { company,effectiveDate,expirationDate,_keyEncryptionKey,_rfcEncryptionKey};
            PrivateObject privateObject = new PrivateObject(typeof(FileManager));
            string encryptedText = privateObject.Invoke("BuildClientKey", args).ToString();            
            StreamWriter writer = new StreamWriter(@"C:\Users\sfb.DC1\Desktop\Credit Risk Model\UnitTestFiles\ErnieKeyExpired.txt");
            writer.WriteLine(encryptedText);
            writer.Close();

            //build not-effective-yet key
            effectiveDate = "07/01/2019";
            expirationDate = "07/01/2019";            
            object[] args2 = new object[5] { company, effectiveDate,expirationDate , _keyEncryptionKey, _rfcEncryptionKey };
            PrivateObject privateObject2 = new PrivateObject(typeof(FileManager));
            encryptedText = privateObject2.Invoke("BuildClientKey", args2).ToString();
            writer = new StreamWriter(@"C:\Users\sfb.DC1\Desktop\Credit Risk Model\UnitTestFiles\ErnieKeyNotEffective.txt");
            writer.WriteLine(encryptedText);
            writer.Close();

            //the good one
            effectiveDate = "07/01/2016";
            expirationDate = "07/01/2019";
            object[] args3 = new object[5] { company, effectiveDate, expirationDate, _keyEncryptionKey, _rfcEncryptionKey };
            PrivateObject privateObject3 = new PrivateObject(typeof(FileManager));
            encryptedText = privateObject3.Invoke("BuildClientKey", args3).ToString();
            writer = new StreamWriter(@"C:\Users\sfb.DC1\Desktop\Credit Risk Model\UnitTestFiles\ErnieKey.txt");
            writer.WriteLine(encryptedText);
            writer.Close();

            string decryptedCompany = string.Empty;
            string decryptedExpirationDate= string.Empty;
            string decryptedEffectiveDate = string.Empty;
            Assert.IsTrue(keyGenerator.DecryptKey(encryptedText, ref decryptedCompany,
                 ref decryptedEffectiveDate, ref decryptedExpirationDate, _keyEncryptionKey, _rfcEncryptionKey)); // ref decryptedLocation, ref decryptedRefnum));
            Assert.IsTrue(company == decryptedCompany);
            Assert.IsTrue(expirationDate == decryptedExpirationDate);
            Assert.IsTrue(effectiveDate == decryptedEffectiveDate);
        }

        [TestMethod]
        public void TestMakeEncryptedFile()
        {
            /* we aren't encrypting this anymore
            //internal class. visible here because I added this to Components project's Assembly.cs file: [assembly: InternalsVisibleTo("Solomon.CreditRiskModel.Components.Tests")]fs
            Writer writer = new Writer();
            Assert.IsTrue(writer.MakeEncryptedCsvFile(@"C:\Cumulative Default Probabilities Annual - Decrypted.csv", @"C:\Cumulative Default Probabilities Annual.csv"));
            //Assert.IsTrue(writer.MakeEncryptedCsvFile(@"C:\Cumulative Default Probabilities Month - Decrypted.csv", @"C:\Cumulative Default Probabilities Month.csv"));
            //Assert.IsTrue(writer.MakeEncryptedCsvFile(@"C:\Cumulative Default Probabilities Month - ExcelFORMAT(SHORT) - Decrypted.csv", @"C:\Cumulative Default Probabilities Month - ExcelFORMAT(SHORT).csv"));
            //Assert.IsTrue(writer.MakeEncryptedCsvFile(@"C:\Cumulative Default Probabilities Month - TEXTFORMAT(PRECISE) - Decrypted.csv", @"C:\Cumulative Default Probabilities Month - TEXTFORMAT(PRECISE).csv"));
            Assert.IsTrue(writer.MakeEncryptedCsvFile(@"C:\Cumulative Default Probabilities Month - Decrypted.csv", @"C:\Cumulative Default Probabilities Month.csv"));
            */
        }

        [TestMethod]
        public void TestMakeEncryptedFileIntRates()
        {
            /* we aren't encrypting this anymore
            //internal class. visible here because I added this to Components project's Assembly.cs file: [assembly: InternalsVisibleTo("Solomon.CreditRiskModel.Components.Tests")]
            FactoriesFactory factoryfactory = new FactoriesFactory();
            WriterFactory factory = (WriterFactory)factoryfactory.Create("WRITER");
            TxtWriter writer = (TxtWriter)factory.Create("TEXT", "");
            Assert.IsTrue(writer.GenerateEncryptedCsvFile(@"C:\CreditRiskModelAnnualInterestRates - Decrypted.csv", @"C:\CreditRiskModelAnnualInterestRates.csv",_keyEncryptionInterestRates,_rfcEncryptionInterestRates));
            Assert.IsTrue(writer.GenerateEncryptedCsvFile(@"C:\CreditRiskModelMonthInterestRates - Decrypted.csv", @"C:\CreditRiskModelMonthInterestRates.csv", _keyEncryptionInterestRates, _rfcEncryptionInterestRates));
            */
        }


        [TestMethod]
        public void TestMakeEncryptedFileESD()
        {
            //internal class. visible here because I added this to Components project's Assembly.cs file: [assembly: InternalsVisibleTo("Solomon.CreditRiskModel.Components.Tests")]fs
            FactoriesFactory factoryfactory = new FactoriesFactory();
            WriterFactory factory = (WriterFactory)factoryfactory.Create("WRITER");
            TxtWriter writer = (TxtWriter)factory.Create("TEXT", "");
            Assert.IsTrue(writer.GenerateEncryptedCsvFile(@"C:\Users\sfb.DC1\Desktop\Credit Risk Model\UnitTestFiles\AnnualESD-Decrypted.txt", @"C:\Users\sfb.DC1\Desktop\Credit Risk Model\UnitTestFiles\AnnTest.txt", _keyEncryptionESD, _rfcEncryptionESD));
            Assert.IsTrue(writer.GenerateEncryptedCsvFile(@"C:\Users\sfb.DC1\Desktop\Credit Risk Model\UnitTestFiles\MonthTest1 - Decrypted.txt", @"C:\Users\sfb.DC1\Desktop\Credit Risk Model\UnitTestFiles\MonthTest1.txt", _keyEncryptionESD, _rfcEncryptionESD));
        }

        [TestMethod]
        public void UserExpiredTest()
        {
            IFiles keyManager = new FileManager();
            Assert.IsTrue(keyManager.UserIsExpired(@"C:\Users\sfb.DC1\Desktop\Credit Risk Model\UnitTestFiles\ErnieKeyExpired.txt", _keyEncryptionKey, _rfcEncryptionKey));
        }

        [TestMethod]
        public void UserNotEffectiveTest()
        {
            IFiles keyManager = new FileManager();
            Assert.IsFalse(keyManager.UserIsEffective(@"C:\Users\sfb.DC1\Desktop\Credit Risk Model\UnitTestFiles\ErnieKeyNotEffective.txt", _keyEncryptionKey, _rfcEncryptionKey));
        }
        
        [ClassCleanup]
        public static void DeleteTestFiles()
        {
            DelFile(@"C:\Users\sfb.DC1\Desktop\Credit Risk Model\UnitTestFiles\MonthTest1.txt");
            DelFile(@"C:\Users\sfb.DC1\Desktop\Credit Risk Model\UnitTestFiles\AnnTest.txt");
            DelFile(@"C:\Users\sfb.DC1\Desktop\Credit Risk Model\UnitTestFiles\ErnieKey.txt");
            DelFile(@"C:\Users\sfb.DC1\Desktop\Credit Risk Model\UnitTestFiles\ErnieKeyNotEffective.txt");
            DelFile(@"C:\Users\sfb.DC1\Desktop\Credit Risk Model\UnitTestFiles\ErnieKeyExpired.txt");
        }
        public static void DelFile(string path)
        {
            try
            {
                if (File.Exists(path))
                    File.Delete(path);
            }
            catch { }
        }

    }
}
