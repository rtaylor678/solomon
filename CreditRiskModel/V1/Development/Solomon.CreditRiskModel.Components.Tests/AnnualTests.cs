﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Solomon.CreditRiskModel.Components;
using Solomon.CreditRiskModel;
using System.Collections.Generic;
using System.Data;
using Solomon.CreditRiskModel.Data;
using Solomon.CreditRiskModel.Data.Reader;
using Solomon.CreditRiskModel.Data.Writer;

namespace Solomon.CreditRiskModel.Components.Tests
{
    [TestClass]
    public class AnnualTests
    {
        private DataHolder _data = null;
        private string _keyFilePath = @"C:\Users\sfb.DC1\Documents\Credit Risk Model\SOLOMON.Key";
        private string _keyEncryptionKey = "884587801A43486";
        private byte[] _rfcEncryptionKey = new byte[] { 74, 40, 76, 81, 70, 35, 14, 86, 70, 37, 61, 87, 62 };
        private string _keyEncryptionESD = "F545A7BA84FA4E8";
        private byte[] _rfcEncryptionESD = new byte[] { 64, 52, 67, 50, 59, 48, 98, 22, 47, 26, 29, 29, 82 };
        private string _keyEncryptionInterestRates = "DEE33F5D39414A3";
        private byte[] _rfcEncryptionInterestRates = new byte[] { 98, 10, 226, 95, 90, 243, 33, 106, 99, 66, 70, 57, 100 };
        private string _esdPath = @"C:\Users\sfb.DC1\Documents\Credit Risk Model\ESD-AnnualTestEncrypted.rdd";

        private DataHolder PopulateDataHolder1()
        {
            DataHolder tempData = new DataHolder();
            
            tempData.FinanceDataInterestRates = new double[] { 10.345, 10, 9.5, 9.345, 9, 8.735, 7.565, 6.675, 5.785, 5.53, 5.275, 5.02, 4.96166666666667, 4.90333333333333, 4.845 };
            tempData.SelectedInsuredCreditRating = "BBB-";
            tempData.SelectedInsurerCreditRating = "AA+";
            FinanceDataManager fdm = new FinanceDataManager();
            if (!fdm.LoadFinanceData(ref tempData, @"C:\Users\sfb.DC1\Documents\Credit Risk Model\Cumulative Default Probabilities Annual.csv"))
                throw new Exception("Error in fdm.LoadFinanceData in  PopulateDataHolder1()");
            return tempData;
        }

        [TestMethod]
        public void PopulateFinanceDataCumulativeDefaultProbabilitiesTest()
        {
            DataHolder tempData = PopulateDataHolder1();
            Solomon.CreditRiskModel.Components.Data.AnalysisData analysisData = new Data.AnalysisData(Enums.PeriodTypes.YEAR, _keyFilePath,_keyEncryptionKey,_rfcEncryptionKey,_keyEncryptionESD,_rfcEncryptionESD,_keyEncryptionInterestRates,_rfcEncryptionInterestRates);

            //now must force population of FinanceDataCumulativeDefaultProbabilities
            FinanceDataManager fdm = new FinanceDataManager();
            Assert.IsTrue(fdm.LoadFinanceData(ref tempData, @"C:\Users\sfb.DC1\Documents\Credit Risk Model\Cumulative Default Probabilities Annual.csv"));
            Assert.IsTrue(analysisData.FinanceDataCumulativeDefaultProbabilitiesValueAt(ref tempData, 0, 0) == 7.5);
            Assert.IsTrue(analysisData.FinanceDataCumulativeDefaultProbabilitiesValueAt(ref tempData, 6, 7) == 2.41);
            Assert.IsTrue(analysisData.FinanceDataCumulativeDefaultProbabilitiesValueAt(ref tempData, 14, 14) == 0.85);
        }


        [TestMethod]
        public void BorrowerAndInsurerIntRateArraysTest()
        {
            _data = PopulateDataHolder1();

            object[] constructor = new object[] { Enums.PeriodTypes.YEAR, _keyFilePath, _keyEncryptionKey, _rfcEncryptionKey, _keyEncryptionESD, _rfcEncryptionESD, _keyEncryptionInterestRates, _rfcEncryptionInterestRates };
            object[] args = new object[1] { _data };
            PrivateObject privateObject = new PrivateObject(typeof(Solomon.CreditRiskModel.Components.Data.AnalysisData), constructor);
            privateObject.Invoke("PopulateBorrowerAndInsurerIntRateArrays", args);
            _data = (DataHolder)args[0];

            Assert.IsTrue(_data.CreditRiskBorrower == 6);
            Assert.IsTrue(_data.CreditRiskInsurer == 14);

            Assert.IsTrue(_data.IntRateArray[0] == 10.345);
            Assert.IsTrue(_data.IntRateArray[1] == 10);
            Assert.IsTrue(_data.IntRateArray[2] == 9.5);
            Assert.IsTrue(_data.IntRateArray[3] == 9.345);
            Assert.IsTrue(_data.IntRateArray[4] == 9);
            Assert.IsTrue(_data.IntRateArray[5] == 8.735);
            Assert.IsTrue(_data.IntRateArray[6] == 7.565);
            Assert.IsTrue(_data.IntRateArray[7] == 6.675);
            Assert.IsTrue(_data.IntRateArray[8] == 5.785);
            Assert.IsTrue(_data.IntRateArray[9] == 5.53);
            Assert.IsTrue(_data.IntRateArray[10] == 5.275);
            Assert.IsTrue(_data.IntRateArray[11] == 5.02);
            Assert.IsTrue(_data.IntRateArray[12] == 4.96166666666667);
            Assert.IsTrue(_data.IntRateArray[13] == 4.90333333333333);
            Assert.IsTrue(_data.IntRateArray[14] == 4.845);

        }

        [TestMethod]
        public void InsuredAndUninsuredAnnualLoanPmtsTest()
        {
            if (_data == null)
            {
                _data = PopulateDataHolder1();
                _data.IntRateBorrower = .07565;
                _data.IntRateInsurer = .04845;
                _data.Nterm = 5;
                _data.LoanAmt = 8000000;
                _data.CoverageRatio = 1;
                _data.TotalInsuredProjectSavings= 4000000;
            }
            Solomon.CreditRiskModel.Components.Data.AnalysisData analysis = new Data.AnalysisData(Enums.PeriodTypes.YEAR, _keyFilePath, _keyEncryptionKey, _rfcEncryptionKey, _keyEncryptionESD, _rfcEncryptionESD, _keyEncryptionInterestRates, _rfcEncryptionInterestRates);
            analysis.PopulateUninsuredLoanPayment(ref _data);

            //NOTE: Excel returns 1980733.67532334. But .Net yields 1980733.161058086.
            //Because the exponent calc gives 0.694456571991399 in .net, but Excel gives  0.694456651320775 
            Assert.IsTrue(Within1(_data.UninsuredLoanPayment, 1980733.67532334));

            analysis.PopulateInsuredEffectiveLoanPayment(ref _data);
            Assert.IsTrue(Within1(_data.InsuredEffectiveLoanPayment, 1910311.07915448));
        }


        [TestMethod]
        public void PopulateEnergySvgsDistArrayTest()
        {
            if (_data == null)
            {
                _data = PopulateDataHolder1();
                _data.IntRateBorrower = .07565;
                _data.IntRateInsurer = .04845;
                _data.Nterm = 5;
                _data.LoanAmt = 8000000;
                _data.CoverageRatio = 1;
                _data.TotalInsuredProjectSavings = 4000000;

                _data.UninsuredLoanPayment = 1980733.67532334;
                _data.InsuredEffectiveLoanPayment = 1910311.07915448;
                _data.ESDDataPath = _esdPath;
            }
            object[] constructor = new object[] { Enums.PeriodTypes.YEAR, _keyFilePath, _keyEncryptionKey, _rfcEncryptionKey, _keyEncryptionESD, _rfcEncryptionESD, _keyEncryptionInterestRates, _rfcEncryptionInterestRates };
            object[] args = new object[1] { _data };
            PrivateObject privateObject = new PrivateObject(typeof(Solomon.CreditRiskModel.Components.Data.AnalysisData), constructor);
            privateObject.Invoke("PopulateEnergySvgsDistArray", args);
            _data = (DataHolder)args[0];
            Assert.IsTrue(_data.EnergySvgsDist[0] == 0);
            Assert.IsTrue(Within1(_data.EnergySvgsDist[1], 1567074.375));
            Assert.IsTrue(Within1(_data.EnergySvgsDist[50],1873283.625));
            Assert.IsTrue(Within1(_data.EnergySvgsDist[100], 2180462.75));
            Assert.IsTrue(_data.EnergySvgsDist[101]== 0);
        }

        [TestMethod]
        public void ProjectSavingsDistributionTest()
        {
            if (_data == null)
            {
                _data = PopulateDataHolder1();
                _data.IntRateBorrower = .07565;
                _data.IntRateInsurer = .04845;
                _data.Nterm = 5;
                _data.LoanAmt = 8000000;
                _data.CoverageRatio = 1;
                _data.TotalInsuredProjectSavings = 4000000;

                _data.UninsuredLoanPayment = 1980733.67532334;
                _data.InsuredEffectiveLoanPayment = 1910311.07915448;
                _data.ESDDataPath = _esdPath;

                PopulateEnergySvgsDistArrayTest();
            }
            Solomon.CreditRiskModel.Components.Data.AnalysisData analysis = new Data.AnalysisData(Enums.PeriodTypes.YEAR, _keyFilePath, _keyEncryptionKey, _rfcEncryptionKey, _keyEncryptionESD, _rfcEncryptionESD, _keyEncryptionInterestRates, _rfcEncryptionInterestRates);
            Assert.IsTrue( analysis.PopulateProjectSavingsDistribution(ref _data));
            Assert.IsTrue(Within1((double)_data.ProjectSavingsDistribution[0], 1704075));
            Assert.IsTrue(Within1((double)_data.ProjectSavingsDistribution[1], 1762487));
            Assert.IsTrue(Within1((double)_data.ProjectSavingsDistribution[2], 1803922));
            Assert.IsTrue(Within1((double)_data.ProjectSavingsDistribution[3], 1839840));
            Assert.IsTrue(Within1((double)_data.ProjectSavingsDistribution[4], 1873284));
            Assert.IsTrue(Within1((double)_data.ProjectSavingsDistribution[5], 1905572));
            Assert.IsTrue(Within1((double)_data.ProjectSavingsDistribution[6], 1938492));
            Assert.IsTrue(Within1((double)_data.ProjectSavingsDistribution[7], 1975340));
            Assert.IsTrue(Within1((double)_data.ProjectSavingsDistribution[8], 2018054));
        }

        [TestMethod]
        public void PopulateLossCostsNoInsuranceTest()
        {
            if (_data == null)
            {
                _data = PopulateDataHolder1();
                BorrowerAndInsurerIntRateArraysTest();
                _data.IntRateBorrower = .07565;
                _data.IntRateInsurer = .04845;
                _data.Nterm = 5;
                _data.LoanAmt = 8000000;
                _data.CoverageRatio = 1;
                _data.TotalInsuredProjectSavings = 4000000;

                _data.UninsuredLoanPayment = 1980733.67532334;
                _data.InsuredEffectiveLoanPayment = 1910311.07915448;
                _data.ESDDataPath = _esdPath;

                PopulateFinanceDataCumulativeDefaultProbabilitiesTest();           
                PopulateEnergySvgsDistArrayTest();
                ProjectSavingsDistributionTest();
                
                _data.CreditRiskBorrower = 6;
                _data.CreditRiskInsurer = 14;

            }
            object[] constructor = new object[] { Enums.PeriodTypes.YEAR, _keyFilePath, _keyEncryptionKey, _rfcEncryptionKey, _keyEncryptionESD, _rfcEncryptionESD, _keyEncryptionInterestRates, _rfcEncryptionInterestRates };
            object[] args = new object[1] { _data };
            PrivateObject privateObject = new PrivateObject(typeof(Solomon.CreditRiskModel.Components.Data.AnalysisData), constructor);
            privateObject.Invoke("PopulateLossCostsNoInsurance", args);
            _data = (DataHolder)args[0];
            
            Assert.IsTrue(_data.ProjectSavingsCreditEnhancementNoInsurance[0] == null);
            Assert.IsTrue(_data.ProjectSavingsCreditEnhancementNoInsurance[1] == null);
            Assert.IsTrue(_data.ProjectSavingsCreditEnhancementNoInsurance[2] == null);
            Assert.IsTrue(_data.ProjectSavingsCreditEnhancementNoInsurance[3] == null);
            Assert.IsTrue(_data.ProjectSavingsCreditEnhancementNoInsurance[4] == null);
            Assert.IsTrue(_data.ProjectSavingsCreditEnhancementNoInsurance[5] == null);
            Assert.IsTrue(_data.ProjectSavingsCreditEnhancementNoInsurance[6] == 0.99341679679523343);
            Assert.IsTrue(_data.ProjectSavingsCreditEnhancementNoInsurance[7] == 0.99189948297860531);
            Assert.IsTrue(_data.ProjectSavingsCreditEnhancementNoInsurance[8] == 0.97909703118279667);
            Assert.IsTrue(_data.ProjectSavingsCreditEnhancementNoInsurance[9] == 0.93916356183098193);
            Assert.IsTrue(_data.ProjectSavingsCreditEnhancementNoInsurance[10] == 0.90077708363666764);
            Assert.IsTrue(_data.ProjectSavingsCreditEnhancementNoInsurance[11] == 0.78377599402971487);
            Assert.IsTrue(_data.ProjectSavingsCreditEnhancementNoInsurance[12] == 0.65448275002816536);
            Assert.IsTrue(_data.ProjectSavingsCreditEnhancementNoInsurance[13] == null);
        }



        [TestMethod]
        public void PopulateLossCostsWithInsuranceTest()
        {
            if (_data == null)
            {
                _data = PopulateDataHolder1();
                BorrowerAndInsurerIntRateArraysTest();
                _data.IntRateBorrower = .07565;
                _data.IntRateInsurer = .04845;
                _data.Nterm = 5;
                _data.LoanAmt = 8000000;
                _data.CoverageRatio = 1;
                _data.TotalInsuredProjectSavings = 4000000;

                _data.UninsuredLoanPayment = 1980733.67532334;
                _data.InsuredEffectiveLoanPayment = 1910311.07915448;
                _data.ESDDataPath = _esdPath;

                PopulateFinanceDataCumulativeDefaultProbabilitiesTest();   
                PopulateEnergySvgsDistArrayTest();
                ProjectSavingsDistributionTest();

                _data.CreditRiskBorrower = 6;
                _data.CreditRiskInsurer = 14;
            }
            object[] constructor = new object[] { Enums.PeriodTypes.YEAR, _keyFilePath, _keyEncryptionKey, _rfcEncryptionKey, _keyEncryptionESD, _rfcEncryptionESD, _keyEncryptionInterestRates, _rfcEncryptionInterestRates };
            object[] args = new object[1] { _data };
            PrivateObject privateObject = new PrivateObject(typeof(Solomon.CreditRiskModel.Components.Data.AnalysisData), constructor);
            privateObject.Invoke("PopulateLossCostsWithInsurance", args);
            _data = (DataHolder)args[0];

            Assert.IsTrue(_data.ProjectSavingsCreditEnhancementWithInsurance[0] == null);
            Assert.IsTrue(_data.ProjectSavingsCreditEnhancementWithInsurance[1] == null);
            Assert.IsTrue(_data.ProjectSavingsCreditEnhancementWithInsurance[2] == null);
            Assert.IsTrue(_data.ProjectSavingsCreditEnhancementWithInsurance[3] == null);
            Assert.IsTrue(_data.ProjectSavingsCreditEnhancementWithInsurance[4] == null);
            Assert.IsTrue(_data.ProjectSavingsCreditEnhancementWithInsurance[5] == null);
            Assert.IsTrue(Within1((double)_data.LossReserves[6], 164286.7));
            Assert.IsTrue(Within1((double)_data.LossReserves[7], 79632.79));
            Assert.IsTrue(Within1((double)_data.LossReserves[8], 60393.04));
            Assert.IsTrue(Within1((double)_data.LossReserves[9], 31947.64));
            Assert.IsTrue(Within1((double)_data.LossReserves[10], 26652.64));
            Assert.IsTrue(Within1((double)_data.LossReserves[11], 23792.17));
            Assert.IsTrue(Within1((double)_data.LossReserves[12], 18060.07));
            Assert.IsTrue(Within1((double)_data.LossReserves[13], 13810.32));
            Assert.IsTrue(Within1((double)_data.LossReserves[14], 6991.576));
            

        }
        private bool Within1(double toCompare, double compareWith)
        {
            if (toCompare < compareWith + 1 && toCompare > compareWith - 1)
            {
                return true;
            }
            else
            { return false; }
        }

        [TestMethod]
        public void TxtReaderTest()
        {
            TxtReader rdr = new TxtReader();
            rdr.Open(@"C:\DevConfigHelper.txt");
            var z = rdr.ReadAll();
            rdr.Reset();
            while (!rdr.EndOfStream)
            {
                var y = rdr.Read();
            }
            rdr.Close();
        }

        
           
    }
}


