﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Solomon.CreditRiskModel
{
    public interface IAnalyze
    {
        bool Analyze(Enums.PeriodTypes periodType, string keyFilePath,
            string keyEncryptionKey, byte[] rfcEncryptionKey, string keyEncryptionESD,
            byte[] rfcEncryptionESD, string keyEncryptionInterestRates, byte[] rfcEncryptionInterestRates);
    }

    public interface IFinance
    {
        bool LoadFinanceData(ref DataHolder dataHolder, string filePath);
        bool SaveFinanceData(ref FinanceData financeData, string filePath);
        bool LoadFinanceDataInterestRates(ref DataHolder dataHolder, string filePath);
        bool SaveFinanceDataInterestRates(ref DataHolder dataHolder, string filePath);
    }

    public interface IFiles
    {
        string GenerateNewKey(string path, string companyName, string effectiveDate, string expirationDate,
            string key, byte[] rfc);
        string GenerateNewEsdFile(string pathDecrypted, string pathEncrypted, string key, byte[] rfc);
        string DecryptExistingEsdFile(string pathEncrypted, string pathDecrypted, string key, byte[] rfc);
        string EncryptString(string theText, string key, byte[] rfc);
        string DecryptString(string theText, string key, byte[] rfc);
        bool DecryptKey(string keyText, ref string company,
            ref string expirationDate, ref string effectiveDate, string key, byte[] rfc);
        bool UserIsExpired(string keyFilePath, string key, byte[] rfc);
        bool UserIsEffective(string keyFilePath, string key, byte[] rfc);
        bool GetKeyFileContents(string path, ref string company, ref string effectiveDate, ref string expiredDate,
            string key, byte[] rfc);


    }
}
