﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;

namespace Solomon.CreditRiskModel
{
    public static class Utilities
    {
        public static string GetSetting(string keyName, string appExePath)
        {
            //THIS GETS FROM APP'S EXE.CONFIG, NOT FROM APP.CONFIG.
            string setting = string.Empty;
            try
            {
                Configuration config = ConfigurationManager.OpenExeConfiguration(appExePath);
                setting = config.AppSettings.Settings[keyName].Value.ToString();
            }
            catch { }
            return setting;
        }
    }
}
