﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;
using System.Data;
using System.Xml.Serialization;

namespace Solomon.CreditRiskModel
{
    public class DataHolder
    {
        public DataHolder()
        {
            FinanceDataCumulativeDefaultProbabilities = new FinanceData();
        }
        
        #region "primitives"
        
        public string SelectedInsuredCreditRating {get;set;}

        public string SelectedInsurerCreditRating { get; set; }
        
        private double _intRateInsurer = 0.0; 
        public double IntRateInsurer//aka IntRateInsr
        {
            get { return _intRateInsurer; }
            set
            {
                _intRateInsurer = value;
                //this.OnPropertyChanged("IntRateInsurer");
            }
        }

        private double _intRateBorrower = 0.0;
        public double IntRateBorrower
        {
            get { return _intRateBorrower; }
            set
            {
                _intRateBorrower = value;
                //this.OnPropertyChanged("IntRateBorrower");
            }
        }

        private double _loanAmt = 0;
        public double LoanAmt
        {
            get { return _loanAmt; }
            set
            {
                _loanAmt = value;
                //this.OnPropertyChanged("LoanAmt");
            }
        }

        private double _coverageRatio = 0.0;
        public double CoverageRatio
        {
            get { return _coverageRatio; }
            set
            {
                _coverageRatio = value;
                //this.OnPropertyChanged("CoverageRatio");
            }
        }

        private int _nterm = 0;
        public int Nterm
        {
            get { return _nterm; }
            set
            {
                _nterm = value;
                //this.OnPropertyChanged("Nterm");
            }
        }

        private double _totalInsuredProjectSavings = 0;
        public double TotalInsuredProjectSavings //B3  aka InsAmt
        {
            get { return _totalInsuredProjectSavings; }
            set
            {
                _totalInsuredProjectSavings = value;
                //this.OnPropertyChanged("TotalInsuredProjectSavings");
            }
        }

        private string _eSDDataPath = string.Empty;
        public string ESDDataPath//path to excel file stored on user's hard drive
        {
            get { return _eSDDataPath; }
            set
            {
                _eSDDataPath = value;
                //this.OnPropertyChanged("ESDDataPath");
            }
        }

        private int _creditRiskBorrower = 0;
        public int CreditRiskBorrower //aka CRBor
        {
            get { return _creditRiskBorrower; }
            set
            {
                _creditRiskBorrower = value;
                //this.OnPropertyChanged("CreditRiskBorrower");
            }
        }

        private int _creditRiskInsurer = 0;
        public int CreditRiskInsurer  //aka CRIns
        {
            get { return _creditRiskInsurer; }
            set
            {
                _creditRiskInsurer = value;
                //this.OnPropertyChanged("CreditRiskInsurer");
            }
        }

        private double _uninsuredLoanPayment = 0;
        //http://stackoverflow.com/questions/2109423/using-dataannotations-on-windows-forms-project
        //[DisplayFormat(DataFormatString = "{0:C0}")]
        public double UninsuredLoanPayment
        {
            get { return _uninsuredLoanPayment; }
            set
            {
                _uninsuredLoanPayment = value;
                //this.OnPropertyChanged("UninsuredLoanPayment");
            }
        }

        private double _insuredEffectiveLoanPayment = 0;
        public double InsuredEffectiveLoanPayment
        {
            get { return _insuredEffectiveLoanPayment; }
            set
            {
                _insuredEffectiveLoanPayment = value;
                //this.OnPropertyChanged("InsuredEffectiveLoanPayment");
            }
        }
        private double _savingsFromLoanPaymentReduction = 0;
        public double SavingsFromLoanPaymentReduction
        {
            get { return _savingsFromLoanPaymentReduction; }
            set
            {
                _savingsFromLoanPaymentReduction = value;
                //this.OnPropertyChanged("SavingsFromLoanPaymentReduction");
            }
        }
        
        #endregion

        #region"Lists"

        private List<string> _insuredCreditRating = GetShortRatingsList();
        public List<string> InsuredCreditRating
        {
            get { return _insuredCreditRating; }
            set
            {
                _insuredCreditRating = value;
                //this.OnPropertyChanged("InsuredCreditRating");
            }
        }

        private List<string> _insurerCreditRating = GetShortRatingsList();
        public List<string> InsurerCreditRating
        {
            get { return _insurerCreditRating; }
            set
            {
                _insurerCreditRating = value;
                //this.OnPropertyChanged("InsurerCreditRating");
            }
        }

        private List<decimal> _projectSavingsDistribution;
        public List<decimal> ProjectSavingsDistribution //the 9 $s on lower left side of GUI - underneath "Project Savings Distribution File:"
        {
            get { return _projectSavingsDistribution; }
            set
            {
                _projectSavingsDistribution = value;
                //this.OnPropertyChanged("ProjectSavingsDistribution");
            }
        }

        private List<double> _energySvgsDist;

        [XmlIgnore] //so client can't figure out what these are
        public List<double> EnergySvgsDist
        {
            get { return _energySvgsDist; }
            set
            {
                _energySvgsDist = value;
                //this.OnPropertyChanged("EnergySvgsDist");
            }
        }

        private List<double?> _projectSavingsCreditEnhancementNoInsurance;
        public List<double?> ProjectSavingsCreditEnhancementNoInsurance
        {
            get { return _projectSavingsCreditEnhancementNoInsurance; }
            set
            {
                _projectSavingsCreditEnhancementNoInsurance = value;
                //this.OnPropertyChanged("ProjectSavingsCreditEnhancementNoInsurance");
            }
        }

        private List<double?> _projectSavingsCreditEnhancementWithInsurance;
        public List<double?> ProjectSavingsCreditEnhancementWithInsurance
        {
            get { return _projectSavingsCreditEnhancementWithInsurance; }
            set
            {
                _projectSavingsCreditEnhancementWithInsurance = value;
                //this.OnPropertyChanged("ProjectSavingsCreditEnhancementWithInsurance");
            }
        }

        private List<double?> _lossReserves;
        public List<double?> LossReserves
        {
            get { return _lossReserves; }
            set
            {
                _lossReserves = value;
                //this.OnPropertyChanged("LossReserves");
            }
        }

        private List<double?> _creditRiskSavings;
        public List<double?> CreditRiskSavings
        {
            get { return _creditRiskSavings; }
            set
            {
                _creditRiskSavings = value;
                //this.OnPropertyChanged("CreditRiskSavings");
            }
        }

        private static List<string> GetShortRatingsList()
        {
            List<string> shortRatings = new List<string>();
            shortRatings.Add("B-");
            shortRatings.Add("B ");
            shortRatings.Add("B+ ");
            shortRatings.Add("BB- ");
            shortRatings.Add("BB ");
            shortRatings.Add("BB+ ");
            shortRatings.Add("BBB- ");
            shortRatings.Add("BBB ");
            shortRatings.Add("BBB+ ");
            shortRatings.Add("A- ");
            shortRatings.Add("A ");
            shortRatings.Add("A+ ");
            shortRatings.Add("AA- ");
            shortRatings.Add("AA ");
            shortRatings.Add("AA+");
            return shortRatings;
        }

        #endregion

        //15 elements. This could be populated for Annual (FinanceData sheet C19-Q19) or Monthly (FInanceData sheet cols T - AH)
        public double[] FinanceDataInterestRates { get; set; }
        public DataTable LossCostsNoInsurance { get; set; } //douuble[,]
        public DataTable LossCostsWithInsurance { get; set; } //double[,]

        public FinanceData FinanceDataCumulativeDefaultProbabilities {get;set;}
        

        public double[] IntRateArray = new double[15];
        public string[] SPRatings
        {
            get
            {
                return new string[] { "B-", "B", "B+", "BB-", "BB", "BB+", "BBB-", "BBB", "BBB+", "A-", "A", "A+", "AA-", "AA", "AA+" };
            }
        }

    }
}
