﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Solomon.CreditRiskModel
{
    public static class ErrorsInfo
    {
        private static List<string> _errors = new List<string>();
        public static void AddErrorInfo(string methodName, string errorInfo)
        {
            _errors.Add("Error in " + methodName + ": " + errorInfo);
        }
        public static string GetAllErrors()
        {
            string result=string.Empty;
            foreach (string err in _errors)
            {
                result += err + Environment.NewLine + Environment.NewLine;
            }
            return result;
        }
        public static void Reset()
        {
            _errors.Clear();
        }
    }
}
