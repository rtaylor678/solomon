﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.CompilerServices;
//using ProfileLiteSecurity;

namespace Solomon.CreditRiskModel.Components
{
    public class KeyManager
    {
        //public string BuildClientKey(string company,  string expirationDate,
        //    string effectiveDate,string key, byte[] rfc)// password, string location, string refnum)
        //{
        //    string retVal = string.Empty;

        //    //if (password.Length > 0)
        //    //{
        //    //    retVal = company.Trim() + "$" + password.Trim() + "$" + location.Trim() + "$" + refnum.ToUpper().Trim();
        //    //}
        //    //else
        //    //{
        //        //retVal = company.Trim() + "$" + location.Trim() + "$" + refnum.ToUpper().Trim();
        //    retVal = company.Trim() + "$" + expirationDate.Trim() + "$" + effectiveDate.Trim();
        //    //}
        //    Cryptographer crypto = new Cryptographer(key, rfc);
        //    return crypto.Encrypt(retVal);
        //}

        //public bool DecryptKey(string keyText, ref string company, 
        //    ref string expirationDate, ref string effectiveDate, string key, byte[] rfc)
        //{
        //    try
        //    {
        //        Cryptographer crypto = new Cryptographer(key, rfc);
        //        string[] keyDetails = crypto.Decrypt(keyText).Split('$');
        //        company = keyDetails[0];
        //        expirationDate = keyDetails[1];
        //        effectiveDate= keyDetails[2];
        //        return true;
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //}

        //public bool UserIsExpired(string keyFilePath, string key, byte[] rfc)
        //{
        //    FactoriesFactory factoryfactory = new FactoriesFactory();
        //    Reader.ReaderFactory factory = (Reader.ReaderFactory)factoryfactory.Create("READER"); // new Reader.ReaderFactory();
        //    Reader.TxtReader reader = (Reader.TxtReader)factory.Create("TEXT", "");
        //    try
        //    {
        //        string company = string.Empty;
        //        string expirationDate = string.Empty;
        //        string effectiveDate = string.Empty;
        //        reader.Open(keyFilePath);
        //        Reader.ReaderDetails details = reader.ReadWithDecryption(key, rfc);
        //        string[] detailsParts = details.value.Split('$');
        //        string[] dateParts = detailsParts[2].Split('/');
        //        DateTime expired = new DateTime(Int32.Parse(dateParts[2]),
        //            Int32.Parse(dateParts[0]),Int32.Parse(dateParts[1]));
        //        if (DateTime.Now > expired)
        //        {
        //            ErrorsInfo.AddErrorInfo("KeyManager", "Your key seems to have expired.");
        //            return true;
        //        }             
        //        return false;
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //    finally
        //    {
        //        reader.Close();
        //    }
        //}

        //public bool UserIsEffective(string keyFilePath, string key, byte[] rfc)
        //{
        //    FactoriesFactory factoryfactory = new FactoriesFactory();
        //    Reader.ReaderFactory factory = (Reader.ReaderFactory)factoryfactory.Create("READER"); // new Reader.ReaderFactory();
        //    Reader.TxtReader reader = (Reader.TxtReader)factory.Create("TEXT", "");
        //    try
        //    {
        //        string company = string.Empty;
        //        string expirationDate = string.Empty;
        //        string effectiveDate = string.Empty;
        //        reader.Open(keyFilePath);
        //        Reader.ReaderDetails details = reader.ReadWithDecryption(key, rfc);
        //        string[] detailsParts = details.value.Split('$');
        //        string[] dateParts = detailsParts[1].Split('/');
        //        DateTime effective = new DateTime(Int32.Parse(dateParts[2]),
        //            Int32.Parse(dateParts[0]),Int32.Parse(dateParts[1]));
        //        if (DateTime.Now >= effective)
        //        {
        //            return true;
        //        }
        //        ErrorsInfo.AddErrorInfo("KeyManager","Your key does not seem to be effective yet.");
        //        return false;
        //    }
        //    catch (Exception ex)
        //    {
        //        ErrorsInfo.AddErrorInfo("UserIsEffective", ex.Message);
        //        return false; //throw ex;
        //    }
        //    finally
        //    {
        //        reader.Close();
        //    }
        //}

        //public bool GetKeyFileContents(string path, ref string company, ref string effectiveDate, ref string expiredDate,
        //    string key, byte[] rfc)        
        //{
        //    try
        //    {
        //        FactoriesFactory factoryfactory = new FactoriesFactory();
        //        Reader.ReaderFactory factory = (Reader.ReaderFactory)factoryfactory.Create("READER"); // new Reader.ReaderFactory();
        //        Reader.TxtReader reader = (Reader.TxtReader)factory.Create("TEXT", "");
        //        reader.Open(path);
        //        Reader.ReaderDetails  details= reader.ReadWithDecryption(key, rfc);
        //        string[] keyParts = details.value.ToString().Split('$');
        //        company = keyParts[0];
        //        effectiveDate = keyParts[1];
        //        expiredDate = keyParts[2];
        //        return true;
        //    }
        //    catch (Exception ex)
        //    {
        //        ErrorsInfo.AddErrorInfo("GetKeyFileContents", ex.Message);
        //        return false;
        //    }

        //}
    }
}
