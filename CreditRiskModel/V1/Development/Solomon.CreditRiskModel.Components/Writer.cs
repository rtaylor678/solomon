﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
namespace Solomon.CreditRiskModel.Components
{
    public class Writer
    {
        //bool _append = false;
        //public Writer(bool append = false)
        //{
        //    _append = append;
        //}
        public bool MakeEncryptedCsvFile(string pathDecrypted, string pathEncrypted, string key, byte[] rfc)
        {
            try
            {
                if (File.Exists(pathEncrypted))
                    File.Delete(pathEncrypted);
                Solomon.CreditRiskModel.Components.Reader.TxtReader txt = new Reader.TxtReader();
                KeyManager keyManager = new KeyManager();
                StreamReader reader = new StreamReader(pathDecrypted);
                List<string> lines = new List<string>();
                while (!reader.EndOfStream)
                {
                    string line = reader.ReadLine();
                    if (line.Trim().Length > 0)
                        lines.Add(line.Trim());
                }
                reader.Close();
                return GenerateEncryptedFile(lines, pathEncrypted, key, rfc);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool GenerateEncryptedFile(List<string> lines, string path, string key, byte[] rfc)
        {
            StreamWriter writer = null;
            try
            {
                writer = new StreamWriter(path, false);
                KeyManager encrypter = new KeyManager();
                foreach (string line in lines)
                {
                    //return Solomon.CreditRiskModel.Cryptographer.Encrypt
                    //writer.WriteLine(encrypter.EncryptString(line));
                    Cryptographer crypto = new Cryptographer(key, rfc);
                    writer.WriteLine(crypto.Encrypt(line));
                }
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                writer.Close();
            }
        }

        public bool GenerateDecryptedFile(List<string> lines, string path, string key, byte[] rfc)
        {
            StreamWriter writer = null;
            try
            {
                writer = new StreamWriter(path, true);
                KeyManager encrypter = new KeyManager();
                foreach (string line in lines)
                {
                    Cryptographer crypto = new Cryptographer(key, rfc);
                    writer.WriteLine(crypto.Decrypt(line));
                }
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                writer.Close();
            }
        }

        public bool GenerateFile(List<string> lines, string path)
        {
            StreamWriter writer = null;
            try
            {
                writer = new StreamWriter(path, false);
                foreach (string line in lines)
                {
                    writer.WriteLine(line);
                }
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                writer.Close();
            }
        }

        public void Close()
        { //IO writer closed already
            return;
        }
    }
}
