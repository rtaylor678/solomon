﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Solomon.CreditRiskModel;
using System.IO;
using Solomon.CreditRiskModel.KeyComponents;

namespace Solomon.CreditRiskModel.Components.Data
{
    internal class AnalysisData : AbstractData
    {
        double _period=0;
        string _keyEncryptionKey;
        byte[] _rfcEncryptionKey;
        string _keyEncryptionESD;
        byte[] _rfcEncryptionESD;
        string _keyEncryptionInterestRates;
        byte[] _rfcEncryptionInterestRates;


        internal AnalysisData(Enums.PeriodTypes periodType, string keyFilePath, 
            string keyEncryptionKey, byte[] rfcEncryptionKey, string keyEncryptionESD, byte[] rfcEncryptionESD,
            string keyEncryptionInterestRates, byte[] rfcEncryptionInterestRates)
        {
            switch (periodType)
            {
                case Enums.PeriodTypes.YEAR:
                    _period = 1;
                    break;
                case Enums.PeriodTypes.MONTH:
                    _period = 12;
                    break;
                default:
                    throw new Exception(periodType + " is not a valid period type. Expecting YEAR or MONTH, etc.");
                    break;
            }

            _keyEncryptionKey=keyEncryptionKey;
            _rfcEncryptionKey=rfcEncryptionKey;
            _keyEncryptionESD=keyEncryptionESD;
            _rfcEncryptionESD = rfcEncryptionESD;
            _keyEncryptionInterestRates=keyEncryptionInterestRates;
            _rfcEncryptionInterestRates = rfcEncryptionInterestRates;
            IFiles keyManager = new FileManager();
            if (keyManager.UserIsExpired(keyFilePath, _keyEncryptionKey, _rfcEncryptionKey))
            {
                throw new Exception("User Key has expired.");
            }
            if (!keyManager.UserIsEffective(keyFilePath, _keyEncryptionKey, _rfcEncryptionKey))
            {
                throw new Exception("User Key is not effective yet.");
            }
        }

        private void PopulateBorrowerAndInsurerIntRateArrays(ref DataHolder data)
        {
            for (int creditRisk = 0; creditRisk < 15; creditRisk++) 
            {
                data.IntRateArray[creditRisk] = data.FinanceDataInterestRates[creditRisk]/_period; // not monthly / 12;
            }

            string[] SPRatings = data.SPRatings;
            bool exitFor = false;
            for (int creditRisk = 0; creditRisk < 15; creditRisk++)
            {
                if (exitFor)
                    break;

                if (SPRatings[creditRisk] == data.SelectedInsuredCreditRating.Trim())
                {
                    data.IntRateBorrower = data.IntRateArray[creditRisk] / 100;
                    data.CreditRiskBorrower = creditRisk;
                }

                if (SPRatings[creditRisk] == data.SelectedInsurerCreditRating.Trim())
                {
                    data.IntRateInsurer = data.IntRateArray[creditRisk] / 100;
                    data.CreditRiskInsurer = creditRisk;
                    //exitFor = true;
                }
                if (data.CreditRiskInsurer > 0 && data.CreditRiskBorrower > 0)
                    exitFor = true;
            }
        }

        internal bool PopulateInsuredEffectiveLoanPayment(ref DataHolder data)
        {
            try
            {
                if (data.IntRateBorrower < .001 || data.IntRateInsurer < .001)
                    PopulateBorrowerAndInsurerIntRateArrays(ref data);

                double exponentBorrower = Math.Pow(1 / (1 + data.IntRateBorrower), data.Nterm);
                double exponentInsurer = Math.Pow(1 / (1 + data.IntRateInsurer), data.Nterm);
                data.InsuredEffectiveLoanPayment = (data.LoanAmt - data.CoverageRatio * data.TotalInsuredProjectSavings) * data.IntRateBorrower / (1 - exponentBorrower) + data.CoverageRatio * data.TotalInsuredProjectSavings * data.IntRateInsurer / (1 - exponentInsurer);
                return true;
            }
            catch (Exception ex)
            {
                ErrorsInfo.AddErrorInfo("PopulateInsuredEffectiveLoanPayment", ex.Message);
                return false;
            }
        }
        
        private bool PopulateEnergySvgsDistArray(ref DataHolder data)
        {
            StreamReader reader = null;
            if( data.EnergySvgsDist != null)
                data.EnergySvgsDist.Clear();
            try
            {
                if (!File.Exists(data.ESDDataPath))
                {
                    ErrorsInfo.AddErrorInfo("PopulateEnergySvgsDistArray","Cannot find file at " + data.ESDDataPath);
                    return false;
                }
                reader = new StreamReader(data.ESDDataPath);
                while (!reader.EndOfStream)
                {
                    string entry = reader.ReadLine();
                    Cryptographer crypto = new Cryptographer(_keyEncryptionESD,_rfcEncryptionESD);
                    entry = crypto.Decrypt(entry);
                    if (entry.Trim().Length > 0)
                    {
                        float ESD = 0;
                        if (!float.TryParse(entry, out ESD))
                        {
                            ErrorsInfo.AddErrorInfo("PopulateEnergySvgsDistArray", "Cannot convert ESD file entry " + entry + " to float");
                            return false;
                        }
                        if (data.EnergySvgsDist == null)
                            data.EnergySvgsDist = new List<double>();
                        data.EnergySvgsDist.Add(ESD);
                    }
                }
                //VBA: EnergySvgsDist(1 To 102) As Single 
                //creates 1-based array 102 elements long
                while (data.EnergySvgsDist.Count < 102)
                {
                    data.EnergySvgsDist.Add(0); //this is what happens in prod.
                }
                return true;
            }
            catch (Exception ex)
            {
                ErrorsInfo.AddErrorInfo("PopulateEnergySvgsDistArray",ex.Message);
                return false;
            }
            finally
            {
                reader.Close();
            }
        }

        internal bool PopulateProjectSavingsDistribution(ref DataHolder data)
        {
            if (PopulateEnergySvgsDistArray(ref data))
            {
                List<decimal> result = new List<decimal>();
                for (int counter = 1; counter < 10; counter++)
                {
                    decimal toAdd = (decimal)data.EnergySvgsDist[(counter * 10)] ;
                    result.Add(toAdd);
                }
                data.ProjectSavingsDistribution = result;
                return true;
            }
            return false;
        }

        internal bool PopulateSavingsFromLoanPaymentReduction(ref DataHolder data)
        {
            //=B2*(B7-B8)
            //= loan term * (UninsuredAnnualLoanPmt - InsuredEffectiveLoanPmt  )
            try
            {
                if (data.UninsuredLoanPayment < .001)
                    PopulateUninsuredLoanPayment(ref data);
                data.SavingsFromLoanPaymentReduction = data.Nterm * (data.UninsuredLoanPayment - data.InsuredEffectiveLoanPayment);
                return true;
            }
            catch (Exception ex)
            {
                ErrorsInfo.AddErrorInfo("PopulateSavingsFromLoanPaymentReduction", ex.Message);
                return false;
            }
        }

        internal void PopulateUninsuredLoanPayment(ref DataHolder data)
        {
      
            if (data.IntRateBorrower < .001 || data.IntRateInsurer < .001)
                PopulateBorrowerAndInsurerIntRateArrays(ref data);

            Double exponentBorrower = Math.Pow(1 / (1 + data.IntRateBorrower), data.Nterm);
            data.UninsuredLoanPayment =  data.LoanAmt * data.IntRateBorrower / (1 - exponentBorrower);
        }
        
        internal bool PopulateLossCostsNoInsurance(ref DataHolder data) //use in other methods
        {
            try
            {
                //VBA: ReDim LossCosts(1 To 102, 1 To 15) 
                //creates 1-based multidim array 102 elements by 15 elements

                data.LossCostsNoInsurance = new System.Data.DataTable("LossCostsNoInsurance"); //new double[102, 15];
                int tableCounter = 0;
                for (tableCounter = 0; tableCounter < 15; tableCounter++)
                {
                    System.Data.DataColumn dc = new System.Data.DataColumn(tableCounter.ToString(), typeof(double));
                    data.LossCostsNoInsurance.Columns.Add(dc);
                }
                for (tableCounter = 0; tableCounter < 102; tableCounter++)
                {
                    System.Data.DataRow dr = data.LossCostsNoInsurance.NewRow();
                    for (int col = 0; col < 15; col++)
                    {
                        dr[col] = 0;
                    }
                    data.LossCostsNoInsurance.Rows.Add(dr);
                }



                for (int creditRisk = data.CreditRiskBorrower; creditRisk <= data.CreditRiskInsurer; creditRisk++)
                {
                    //NOTE intRateBorrower is NOT data.IntRateBorrower. intRateBorrower is meant to be local to this method only!!!
                    double intRateBorrower = data.IntRateArray[creditRisk] / 100;
                    double DefProb = 0;
                    for (int IP = 0; IP < data.Nterm; IP++)
                    {
                        double testfindataprob = FinanceDataCumulativeDefaultProbabilitiesValueAt( ref data,IP, creditRisk);
                        DefProb += FinanceDataCumulativeDefaultProbabilitiesValueAt(ref data, IP, creditRisk);
                    }
                    DefProb = DefProb / 100;
                    double ESvgsPerc = 0;
                    for (int rrr = 0; rrr < 102; rrr++) //101 elements in array, vba code might set last one to zero.
                    {
                        ESvgsPerc = data.EnergySvgsDist[rrr]; 
                        Double exponent = Math.Pow(1 / (1 + intRateBorrower), data.Nterm);
                        data.LossCostsNoInsurance.Rows[rrr][creditRisk] = (data.LoanAmt * intRateBorrower / (1 - exponent) - ESvgsPerc) * DefProb;
                        if ( data.LossCostsNoInsurance.Rows[rrr][creditRisk] != System.DBNull.Value &&  (double)data.LossCostsNoInsurance.Rows[rrr][creditRisk] < 0)
                            data.LossCostsNoInsurance.Rows[rrr][creditRisk] = 0;
                        string result = "data.LossCostsNoInsurance[" + rrr.ToString() + ", " + creditRisk.ToString() + "] = " + data.LossCostsNoInsurance.Rows[rrr][creditRisk].ToString();
                        if (rrr == 0 || rrr == 49 || rrr >= 100) 
                        {
                            string test = result;  //so can put a break on this bracket
                        }
                    }
                }

                //null out first elements before the element # matching data.CreditRiskBorrower is reached - not sure if this is really needed yet . . .
                List<double?> newProjectSavingsCreditEnhancementNoInsurance = new List<double?>();
                for (int counter = 1; counter <= data.CreditRiskBorrower; counter++)
                {
                    double? discard = null;
                    newProjectSavingsCreditEnhancementNoInsurance.Add(discard);
                }
                for (int creditRisk = data.CreditRiskBorrower + 1; creditRisk <= data.CreditRiskInsurer - 1; creditRisk++)
                {
                    for (int counter = 1; counter < 102; counter++)
                    {
                        var a = data.LossCostsNoInsurance.Rows[counter][data.CreditRiskBorrower];
                        var b = data.LossCostsNoInsurance.Rows[0][creditRisk];
                        var c = data.LossCostsNoInsurance.Rows[counter - 1][data.CreditRiskBorrower];
                        var d = data.LossCostsNoInsurance.Rows[0][creditRisk];

                        if ((double)data.LossCostsNoInsurance.Rows[counter][data.CreditRiskBorrower] < (double)data.LossCostsNoInsurance.Rows[0][creditRisk] &&
                            (double)data.LossCostsNoInsurance.Rows[counter - 1][data.CreditRiskBorrower] >= (double)data.LossCostsNoInsurance.Rows[0][creditRisk])
                        {
                            if ((double)data.LossCostsNoInsurance.Rows[counter][data.CreditRiskBorrower]
                                - (double)data.LossCostsNoInsurance.Rows[counter - 1][data.CreditRiskBorrower] == 0)
                                break;  //goto next CR2
                            double savings = 1 - 0.01 * ((counter - 1) + ((double)data.LossCostsNoInsurance.Rows[0][creditRisk] - (double)data.LossCostsNoInsurance.Rows[counter - 1][data.CreditRiskBorrower]) /
                                ((double)data.LossCostsNoInsurance.Rows[counter][data.CreditRiskBorrower] - (double)data.LossCostsNoInsurance.Rows[counter - 1][data.CreditRiskBorrower]));
                            newProjectSavingsCreditEnhancementNoInsurance.Add(savings);
                            
                        }
                    }

                }
                //fill in missing elements
                while (newProjectSavingsCreditEnhancementNoInsurance.Count < 14)
                {
                    double? discard = null;
                    newProjectSavingsCreditEnhancementNoInsurance.Add(discard);
                }
                data.ProjectSavingsCreditEnhancementNoInsurance = newProjectSavingsCreditEnhancementNoInsurance;
                return true;
            }
            catch (Exception ex)
            {
                ErrorsInfo.AddErrorInfo("PopulateLossCostsNoInsurance", ex.Message);
                return false;
            }
            
        }


        internal double FinanceDataCumulativeDefaultProbabilitiesValueAt(ref DataHolder data, int rowNumber, int colNumber)
        {
            int rowCount = 0;
            double returnValue = 0;
            foreach (System.Data.DataRow row in data.FinanceDataCumulativeDefaultProbabilities.CumulativeDefaultProbabilities.Rows)
            {
                if (rowCount == rowNumber)
                {
                    //NOTE: colCount starts at 1 to bypass the Period column
                    returnValue = (double)row[colNumber + 1];
                    return returnValue;
                }
                rowCount++;
            }

            string errMessage ="Could not find row " + rowNumber.ToString() + ", column " + colNumber.ToString() + "." ;
            /*
            This could be either:
             they have more LoanTerm than they show in finance data cumulative default probabilities grid, or
            they had 1 year of data in finance data cumulative default probabilities grid and they chose Month in dropdown and asked for 60 months, etc.
            */
            ErrorsInfo.AddErrorInfo("FinanceDataCumulativeDefaultProbabilities",errMessage);
            throw new Exception(errMessage);
        }

        internal bool PopulateLossCostsWithInsurance(ref DataHolder data)
        {
            try
            {
                data.LossCostsWithInsurance =  new System.Data.DataTable("LossCostsWithInsurance"); 
                int tableCounter = 0;
                for (tableCounter = 0; tableCounter < 15; tableCounter++)
                {
                    System.Data.DataColumn dc = new System.Data.DataColumn(tableCounter.ToString(), typeof(double));
                    data.LossCostsWithInsurance.Columns.Add(dc);
                }
                for (tableCounter = 0; tableCounter < 102; tableCounter++)
                {
                    System.Data.DataRow dr = data.LossCostsWithInsurance.NewRow();
                    for (int col = 0; col < 15; col++)
                    {
                        dr[col] = 0;
                    }
                    data.LossCostsWithInsurance.Rows.Add(dr);
                }

                for (int CR = data.CreditRiskBorrower; CR <= data.CreditRiskInsurer; CR++)
                {
                    //NOTE intRateBorrower is NOT data.IntRateBorrower. intRateBorrower is meant to be local to this method only!!!
                    double intRateBorrower = data.IntRateArray[CR] / 100;
                    double DefProb = 0;
                    for (int IP = 0; IP < data.Nterm; IP++)
                    {
                        DefProb += FinanceDataCumulativeDefaultProbabilitiesValueAt(ref data, IP, CR);
                    }
                    DefProb = DefProb / 100;
                    double ESvgsPerc = 0;
                    for (int rrr = 0; rrr < 102; rrr++)
                    {
                        if (rrr == 0)
                        {
                            ESvgsPerc = data.EnergySvgsDist[rrr];
                        }
                        else
                        {
                            double EffinsAmt = (data.TotalInsuredProjectSavings / data.Nterm) * data.CoverageRatio;
                            if (data.EnergySvgsDist[rrr] < EffinsAmt)
                            {
                                ESvgsPerc = EffinsAmt;
                            }
                            else
                            {
                                ESvgsPerc = data.EnergySvgsDist[rrr];
                            }
                        }

                        Double exponentBorrower = Math.Pow(1 / (1 + intRateBorrower), data.Nterm);
                        double exponentInsurer = Math.Pow(1 / (1 + data.IntRateInsurer), data.Nterm);
                        double lossCost = 0;
                        if (_period == 1)
                        {
                            lossCost =  ((data.LoanAmt - data.CoverageRatio * data.TotalInsuredProjectSavings) * intRateBorrower /
                            (1 - exponentBorrower)
                            + data.CoverageRatio * data.TotalInsuredProjectSavings *
                            data.IntRateInsurer / (1 - exponentInsurer) - ESvgsPerc) * DefProb;
                        }
                        else if (_period == 12)
                        {                            
                            //InsAmt = data.TotalInsuredProjectSavings
                            lossCost = ((data.LoanAmt - data.CoverageRatio * data.TotalInsuredProjectSavings) * intRateBorrower / (1 - exponentBorrower)
                                + data.CoverageRatio * data.TotalInsuredProjectSavings * data.IntRateInsurer /
                                (1 - exponentInsurer) - ESvgsPerc) * DefProb;
                          
                        }

                        data.LossCostsWithInsurance.Rows[rrr][CR]= lossCost;

                        string result = "data.LossCostsWithInsurance.Rows[" + rrr.ToString() + "][" + CR.ToString() + "] = " + data.LossCostsWithInsurance.Rows[rrr][CR].ToString();
                        if (rrr >= 100)
                        { string stophere = string.Empty; }
                        if ((double)data.LossCostsWithInsurance.Rows[rrr][CR] < 0)
                            data.LossCostsWithInsurance.Rows[rrr][CR] = 0;

                        System.Diagnostics.Debug.WriteLine(ESvgsPerc.ToString() + " - " + data.LossCostsWithInsurance.Rows[rrr][CR].ToString());
                    }
                } 

                //null out first elements before the element # matching data.CreditRiskBorrower is reached - not sure if this is really needed yet . . .
                List<double?> newProjectSavingsCreditEnhancementWithInsurance = new List<double?>();
                for (int counter = 1; counter <= data.CreditRiskBorrower; counter++)
                {
                    double? discard = null;
                    newProjectSavingsCreditEnhancementWithInsurance.Add(discard);
                }

                int CRBor = data.CreditRiskBorrower;
                int CRIns = data.CreditRiskInsurer;
                for (int CR2 = CRBor + 1; CR2 <= CRIns - 1; CR2++)
                {
                    for (int CR = 1; CR < 102; CR++)
                    {
                        if ((double)data.LossCostsWithInsurance.Rows[CR][CRBor] < (double)data.LossCostsWithInsurance.Rows[0][CR2] &&
                            (double)data.LossCostsWithInsurance.Rows[CR - 1][CRBor] >= (double)data.LossCostsWithInsurance.Rows[0][CR2])
                        {
                            if ((double)data.LossCostsWithInsurance.Rows[CR][data.CreditRiskBorrower]
                                - (double)data.LossCostsWithInsurance.Rows[CR - 1][data.CreditRiskBorrower] == 0)
                                break; 

                            
                            double savings = 1 - 0.01 * ((CR - 1) + ((double)data.LossCostsWithInsurance.Rows[0][CR2] - 
                                (double)data.LossCostsWithInsurance.Rows[CR - 1][CRBor]) / 
                                ((double)data.LossCostsWithInsurance.Rows[CR][CRBor] - 
                                (double)data.LossCostsWithInsurance.Rows[CR - 1][CRBor]));
                            newProjectSavingsCreditEnhancementWithInsurance.Add(savings);
                        }
                    }
                }

                //fill in missing elements

                while (newProjectSavingsCreditEnhancementWithInsurance.Count < 14)
                {
                    double? discard = null;
                    newProjectSavingsCreditEnhancementWithInsurance.Add(discard);
                }


                List<double?> newLossReserves = new List<double?>();

                //null out first elements before the element # matching CRBor is reached - not sure if this is really needed yet . . .
                if (newLossReserves == null)
                    newLossReserves = new List<double?>();
                for (int counter = 1; counter <= CRBor; counter++)
                {
                    double? discard = null;
                    newLossReserves.Add(discard);
                }

                //null out first elements before the element # matching CR2 (which will = CRBor below) is reached - not sure if this is really needed yet . . .
                List<double?> newCreditRiskSavings = new List<double?>();
                for (int counter = 1; counter < CRBor; counter++)
                {
                    double? discard = null;
                    newCreditRiskSavings.Add(discard);
                }

                for (int CR2 = CRBor; CR2 <= CRIns; CR2++)
                {
                    double test = (double)data.LossCostsWithInsurance.Rows[0][CR2];
                    newLossReserves.Add((double)data.LossCostsWithInsurance.Rows[0][CR2]);
                    bool added = false;
                    
                    if (newProjectSavingsCreditEnhancementWithInsurance != null &&
                        newProjectSavingsCreditEnhancementWithInsurance.Count() >= CR2)
                    {
                        if ((CR2 - 1)>=0 &&  newProjectSavingsCreditEnhancementWithInsurance[CR2 - 1] != null &&
                            CR2 > CRBor)
                        {
                            double result = (double)data.LossCostsWithInsurance.Rows[0][CRBor] - (double)data.LossCostsWithInsurance.Rows[0][CR2];
                            newCreditRiskSavings.Add(result);
                            added = true;
                        }
                    }
                    if (!added)
                    {
                        if (CR2 != 0) //never populate this; list has 15 but grid only has 14!!!
                        {
                            double? discard = null;
                            newCreditRiskSavings.Add(discard);
                        }
                    }
                }
                data.CreditRiskSavings = newCreditRiskSavings;
                data.LossReserves = newLossReserves;
                data.ProjectSavingsCreditEnhancementWithInsurance = newProjectSavingsCreditEnhancementWithInsurance;
                return true;
            }
            catch (Exception ex)
            {
                ErrorsInfo.AddErrorInfo("PopulateLossCostsWithInsurance", ex.Message);
                return false;
            }

           
        }
    }
}
