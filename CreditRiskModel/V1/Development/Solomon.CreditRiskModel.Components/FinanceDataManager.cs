﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Solomon.CreditRiskModel;
//using System.IO;
using System.Data;
using Solomon.CreditRiskModel.Data;
using Solomon.CreditRiskModel.Data.Reader;
using Solomon.CreditRiskModel.Data.Writer;

namespace Solomon.CreditRiskModel.Components
{
    public class FinanceDataManager : IFinance
    {
        public bool LoadFinanceData(ref DataHolder dataHolder, string filePath)
        {
            FactoriesFactory factoryfactory = new FactoriesFactory();
            ReaderFactory factory = (ReaderFactory)factoryfactory.Create("READER"); 
            TxtReader reader = (TxtReader)factory.Create("TEXT", "");
            try
            {
                reader.Open(filePath);
                dataHolder.FinanceDataCumulativeDefaultProbabilities.CumulativeDefaultProbabilities.Rows.Clear();
                while (!reader.EndOfStream)
                {
                    string line = reader.Read().value;
                    if (line.Trim().Length > 14)
                    {
                        if (!LoadNextRecord(ref dataHolder, line))
                            return false;
                    }
                }
                return true;
            }
            catch (Exception ex)
            {
                ErrorsInfo.AddErrorInfo("LoadFinanceData", ex.Message);
                return false;
            }
            finally
            {
                try
                {
                    reader.Close();
                }
                catch { }
            }
        }

        private bool LoadNextRecord(ref DataHolder dataHolder, string textRecord)
        {
            try
            {
                string[] dataForColumns = textRecord.Split(',');
                DataRow row =dataHolder.FinanceDataCumulativeDefaultProbabilities.CumulativeDefaultProbabilities.NewRow();
                row[0] = dataHolder.FinanceDataCumulativeDefaultProbabilities.CumulativeDefaultProbabilities.Rows.Count + 1;
                for (int counter = 1; counter < 16; counter++)
                {
                    row[counter] = dataForColumns[counter - 1];
                }
                dataHolder.FinanceDataCumulativeDefaultProbabilities.CumulativeDefaultProbabilities.Rows.Add(row);
                return true;
            }
            catch (Exception ex)
            {
                ErrorsInfo.AddErrorInfo("LoadNextRecord", ex.Message);
                return false;
            }
        }


        public bool SaveFinanceData(ref FinanceData financeData, string filePath)
        {
            FactoriesFactory factoryfactory = new FactoriesFactory();
            WriterFactory factory = (WriterFactory)factoryfactory.Create("WRITER");
            TxtWriter writer = (TxtWriter)factory.Create("TEXT", "");
            List<string> lines = new List<string>();
            try
            {
                foreach (DataRow row in financeData.CumulativeDefaultProbabilities.Rows)
                {
                    string line = string.Empty;
                    if (!RecordToString(row, ref line))
                        return false;
                    lines.Add(line);
                }
                return writer.GenerateFile(lines, filePath);
            }
            catch (Exception ex)
            {
                ErrorsInfo.AddErrorInfo("SaveFinanceData", ex.Message);
                return false;
            }
            finally
            {
                try
                {
                    writer.Close();
                }
                catch { }
            }
        }
        
        private bool RecordToString(DataRow row, ref string line)
        {
            try
            {
                for (int counter = 1; counter < 16; counter++)
                {
                    line += row[counter].ToString() + ",";
                }
                line=line.Remove(line.Length - 1); //remove trailing comma
                return true;
            }
            catch (Exception ex)
            {
                ErrorsInfo.AddErrorInfo("RecordToStringRecordToString", ex.Message);
                return false;
            }
        }
        

        public bool LoadFinanceDataInterestRates(ref DataHolder dataHolder, string filePath)
        {
            FactoriesFactory factoryfactory = new FactoriesFactory();
            ReaderFactory factory = (ReaderFactory)factoryfactory.Create("READER");
            TxtReader reader = (TxtReader)factory.Create("TEXT", "");
            try
            {
                reader.Open(filePath);
                while (!reader.EndOfStream)
                {
                    string line = reader.Read().value;
                    if (line.Trim().Length > 14)
                    {
                        if (!LoadNextInterestRateRecord(ref dataHolder, line))
                            return false;
                    }
                }
                return true;
            }
            catch (Exception ex)
            {
                ErrorsInfo.AddErrorInfo("LoadFinanceDataInterestRates", ex.Message);
                return false;
            }
            finally
            {
                try
                {
                    reader.Close();
                }catch { }
            }
        }

        private bool LoadNextInterestRateRecord(ref DataHolder dataHolder, string textRecord)
        {
            try
            {
                string[] dataForColumns = textRecord.Split(',');
                //clear old values
                if (dataHolder.FinanceDataInterestRates == null)
                {
                    dataHolder.FinanceDataInterestRates = new double[15];
                }
                else
                {
                    for (int counter = 0; counter < dataHolder.FinanceDataInterestRates.Length; counter++)
                    {
                        dataHolder.FinanceDataInterestRates[counter] = 0;
                    }
                }

                //load new values
                for (int counter = 0; counter < dataHolder.FinanceDataInterestRates.Length; counter++)
                {
                    double value = 0;
                    if (!double.TryParse(dataForColumns[counter], out value))
                    {
                        ErrorsInfo.AddErrorInfo("LoadNextInterestRateRecord", "Unable to convert '" + dataForColumns[counter] + "' to a number.");
                        return false;
                    }
                    dataHolder.FinanceDataInterestRates[counter] = value;
                }
                return true;
            }
            catch (Exception ex)
            {
                ErrorsInfo.AddErrorInfo("LoadNextInterestRateRecord", ex.Message);
                return false;
            }
        }

        public bool SaveFinanceDataInterestRates(ref DataHolder dataHolder, string filePath)
        {
            System.IO.StreamWriter writer = null;
            try
            {
                writer = new System.IO.StreamWriter(filePath, false);
                {
                   string line = string.Empty;
                    for(int counter=0;counter < dataHolder.FinanceDataInterestRates.Length; counter++)
                    {
                        line += dataHolder.FinanceDataInterestRates[counter].ToString() + ",";
                    }
                    line = line.Remove(line.Length - 1); //remove trailing comma
                    writer.WriteLine(line);
                }
                return true;
            }
            catch (Exception ex)
            {
                ErrorsInfo.AddErrorInfo("SaveFinanceDataInterestRates", ex.Message);
                return false;
            }
            finally
            {
                try
                {
                    writer.Close();
                }catch { }
            }
        }
        
    }
}
