﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Solomon.CreditRiskModel.Services;
using Solomon.CreditRiskModel.Interfaces;
using System.Collections.Generic;

namespace Solomon.CreditRiskModel.Services.Tests
{
    [TestClass]
    public class ServicesTestsAnnual
    {
        private DataHolder _data = null;

        private DataHolder PopulateDataHolder1()
        {
            DataHolder tempData = new DataHolder();
            //data.FinanceDataInterestRates = new float[] { 10.35f, 10.00f, 9.50f, 9.35f, 9.00f, 8.74f, 7.57f, 6.68f, 5.79f, 5.53f, 5.28f, 5.02f, 4.96f, 4.90f, 4.85f }; // string[] { "10.35", "10.00", "9.50", "9.35", "9.00", "8.74", "7.57", "6.68", "5.79", "5.53", "5.28", "5.02", "4.96", "4.90", "4.85" };
            tempData.FinanceDataInterestRates = new double[] { 10.345, 10, 9.5, 9.345, 9, 8.735, 7.565, 6.675, 5.785, 5.53, 5.275, 5.02, 4.96166666666667, 4.90333333333333, 4.845 };
            tempData.InsuredCreditRating = "BBB-";
            tempData.InsurerCreditRating = "AA+";
            return tempData;
        }

        [TestMethod]
        public void BorrowerAndInsurerIntRateArraysTest()
        {
            //"E:\SFB\Projects\Ernie\ErnieV1.0-Annual.xlsm"

            _data = PopulateDataHolder1();
            
            object[] constructor = new object[] { "YEAR" };
            object[] args = new object[1] { _data };
            PrivateObject privateObject = new PrivateObject(typeof(Solomon.CreditRiskModel.Components.Data.AnalysisData), constructor);            
            privateObject.Invoke("PopulateBorrowerAndInsurerIntRateArrays", args);
            _data = (DataHolder)args[0];

            Assert.IsTrue(_data.CreditRiskBorrower == 6);
            Assert.IsTrue(_data.CreditRiskInsurer == 14);

            Assert.IsTrue(_data.IntRateArray[0] == 10.345);
            Assert.IsTrue(_data.IntRateArray[1] == 10);
            Assert.IsTrue(_data.IntRateArray[2] == 9.5);
            Assert.IsTrue(_data.IntRateArray[3] == 9.345);
            Assert.IsTrue(_data.IntRateArray[4] == 9);
            Assert.IsTrue(_data.IntRateArray[5] == 8.735);
            Assert.IsTrue(_data.IntRateArray[6] == 7.565);
            Assert.IsTrue(_data.IntRateArray[7] == 6.675);
            Assert.IsTrue(_data.IntRateArray[8] == 5.785);
            Assert.IsTrue(_data.IntRateArray[9] == 5.53);
            Assert.IsTrue(_data.IntRateArray[10] == 5.275);
            Assert.IsTrue(_data.IntRateArray[11] == 5.02);
            Assert.IsTrue(_data.IntRateArray[12] == 4.96166666666667);
            Assert.IsTrue(_data.IntRateArray[13] == 4.90333333333333);
            Assert.IsTrue(_data.IntRateArray[14] == 4.845);

        }

        [TestMethod]
        public void InsuredAndUninsuredAnnualLoanPmtsTest()
        {
            if (_data == null)
            {
                _data = PopulateDataHolder1();
                _data.IntRateBorrower =.07565;
                _data.IntRateInsurer =.04845;
                _data.NTerm = 5;
                _data.LoanAmt = 8000000;
                _data.CoverageRatio = 1;
                _data.InsurAmount = 4000000;
            }
            Solomon.CreditRiskModel.Services.Data.AnalysisData analysis = new Data.AnalysisData("YEAR");
            analysis.PopulateUninsuredLoanPayment(ref _data);
            
            //NOTE: Excel returns 1980733.67532334. But .Net yields 1980733.161058086.
            //Because the exponent calc gives 0.694456571991399 in .net, but Excel gives  0.694456651320775 
            Assert.IsTrue( Within1(  _data.UninsuredLoanPayment, 1980733.67532334));
            
            analysis.PopulateInsuredEffectiveLoanPayment(ref _data);
            Assert.IsTrue(Within1(_data.InsuredEffectiveLoanPayment, 1910311.07915448));
        }


        [TestMethod]
        public void PopulateEnergySvgsDistArrayTest()
        {
            if (_data == null)
            {
                _data = PopulateDataHolder1();
                _data.IntRateBorrower = .07565;
                _data.IntRateInsurer = .04845;
                _data.NTerm = 5;
                _data.LoanAmt = 8000000;
                _data.CoverageRatio = 1;
                _data.InsurAmount = 4000000;

                _data.UninsuredLoanPayment = 1980733.67532334;
                _data.InsuredEffectiveLoanPayment = 1910311.07915448;
                _data.ESDDataPath = @"C:\AnnTest.txt";
            }
            object[] constructor = new object[] { "YEAR" };
            object[] args = new object[1] { _data };
            PrivateObject privateObject = new PrivateObject(typeof(Solomon.CreditRiskModel.Services.Data.AnalysisData), constructor);
            privateObject.Invoke("PopulateEnergySvgsDistArray", args);
            _data = (DataHolder)args[0];
            Assert.IsTrue(_data.EnergySvgsDist[0] == 0);
            Assert.IsTrue(Within1( _data.EnergySvgsDist[1],130589.5));
            Assert.IsTrue(Within1(_data.EnergySvgsDist[50], 156106.96875));
            Assert.IsTrue(Within1(_data.EnergySvgsDist[100], 181705.2));
        }

        [TestMethod]
        public void ProjectSavingsDistributionTest()
        {
            if (_data == null)
            {
                _data = PopulateDataHolder1();
                _data.IntRateBorrower = .07565;
                _data.IntRateInsurer = .04845;
                _data.NTerm = 5;
                _data.LoanAmt = 8000000;
                _data.CoverageRatio = 1;
                _data.InsurAmount = 4000000;

                _data.UninsuredLoanPayment = 1980733.67532334;
                _data.InsuredEffectiveLoanPayment = 1910311.07915448;
                _data.ESDDataPath = @"C:\AnnTest.txt";

                PopulateEnergySvgsDistArrayTest();
            }
            Solomon.CreditRiskModel.Services.Data.AnalysisData analysis = new Data.AnalysisData("YEAR");
            List<decimal> results = analysis.GetProjectSavingsDistribution(_data);

            Assert.IsTrue(Within1((double)results[0], 142006.2344));
            Assert.IsTrue(Within1((double)results[1], 146873.9062));
            Assert.IsTrue(Within1((double)results[2], 150326.8281));
            Assert.IsTrue(Within1((double)results[3], 153320.0156));
            Assert.IsTrue(Within1((double)results[4], 156106.9688));
            Assert.IsTrue(Within1((double)results[5], 158797.6406));
            Assert.IsTrue(Within1((double)results[6], 161541.0312));
            Assert.IsTrue(Within1((double)results[7], 164611.6719));
            Assert.IsTrue(Within1((double)results[8], 168171.2031));
        }

        //PopulateLossCostsNoInsurance

        private bool Within1(double toCompare, double compareWith)
        {
            if (toCompare < compareWith + 1 && toCompare > compareWith - 1)
            {
                return true;
            }
            else
            { return false; }
        }
    }
}

