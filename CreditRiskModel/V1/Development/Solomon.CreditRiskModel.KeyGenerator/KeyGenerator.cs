﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Solomon.CreditRiskModel.Services;
using Solomon.CreditRiskModel;

namespace Solomon.CreditRiskModel.KeyGenerator
{
    public partial class KeyGenerator : Form
    {
        private string _keyEncryptionKey = "884587801A43486";
        private byte[] _rfcEncryptionKey = new byte[] { 74, 40, 76, 81, 70, 35, 14, 86, 70, 37, 61, 87, 62 };
        private string _keyEncryptionESD = "F545A7BA84FA4E8";
        private byte[] _rfcEncryptionESD = new byte[] { 64, 52, 67, 50, 59, 48, 98, 22, 47, 26, 29, 29, 82 };
        private string _keyEncryptionInterestRates = "DEE33F5D39414A3";
        private byte[] _rfcEncryptionInterestRates = new byte[] { 98, 10, 226, 95, 90, 243, 33, 106, 99, 66, 70, 57, 100 };
        private string _dataPath = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments).ToString() + @"\Credit Risk Model";

        public KeyGenerator()
        {
            InitializeComponent();
            DateTime testDate = new DateTime(DateTime.Now.Year,DateTime.Now.Month, 1);
            Effective.Value = testDate;
            Expiration.Value = testDate.AddMonths(13);             
        }

        private void CreateKey_Click(object sender, EventArgs e)
        {
            if (CompanyName.Text.Trim().Length < 1)
            {
                MessageBox.Show("Please choose a Company Name and try again.");
                return;
            }
            ServiceManager serviceManager = new ServiceManager();

            Effective.Format = DateTimePickerFormat.Custom;
            Effective.CustomFormat = "MM/dd/yyyy";
            string effDate = Effective.Value.ToString("MM/dd/yyyy");

            Expiration.Format = DateTimePickerFormat.Custom;
            Expiration.CustomFormat = "MM/dd/yyyy";
            string expDate = Expiration.Value.ToString("MM/dd/yyyy");
            string[] expDateParts = expDate.Split('/');
            DateTime testDate = new DateTime(Int32.Parse(expDateParts[2]), Int32.Parse(expDateParts[0]), Int32.Parse(expDateParts[1]));
            if (testDate <= DateTime.Today)
            {
                if (MessageBox.Show("Expiration Date is before or on today. Do you still want to use it?", "", MessageBoxButtons.YesNo) != System.Windows.Forms.DialogResult.Yes)
                    return;
            }
            saveFileDialog1.FileName = string.Empty;
            saveFileDialog1.InitialDirectory = _dataPath;
            if (saveFileDialog1.ShowDialog() != DialogResult.OK || saveFileDialog1.FileName.Length < 1)
            {
                return;
            }
            string fileName = saveFileDialog1.FileName.Trim();
            if (!fileName.ToUpper().EndsWith(".KEY"))
                fileName += ".key";
            CompanyName.Text = CompanyName.Text.Trim();
            string result = serviceManager.GenerateNewKey(fileName, CompanyName.Text, effDate, expDate, _keyEncryptionKey, _rfcEncryptionKey);
            
            MessageBox.Show(result);
        }

        private void tabPage1_Click(object sender, EventArgs e)
        {

        }

        private void btnCreateESD_Click(object sender, EventArgs e)
        {
            txtEncryptedEsdPath.Text = SelectFileToWrite();
        }

        private string WriteNewEncryptedEsdFile()
        {

            ServiceManager serviceManager = new ServiceManager();
            return serviceManager.GenerateNewEsdFile(EsdFilePath.Text.Trim(), txtEncryptedEsdPath.Text.Trim(), _keyEncryptionESD, _rfcEncryptionESD);
        }

        private string WriteNewDecryptedEsdFile()
        {

            ServiceManager serviceManager = new ServiceManager();
            return serviceManager.DecryptExistingEsdFile(EsdFilePath.Text.Trim(), txtEncryptedEsdPath.Text.Trim(), _keyEncryptionESD, _rfcEncryptionESD);
        }

        private void btnLoadKey_Click(object sender, EventArgs e)
        {
            openFileDialog1.FileName = string.Empty;
            openFileDialog1.InitialDirectory = _dataPath;
            string fileName = string.Empty;
            if (openFileDialog1.ShowDialog() != DialogResult.OK || openFileDialog1.FileName.Length < 1)
            {
                return;
            }
            fileName = openFileDialog1.FileName;
            ServiceManager serviceManager = new ServiceManager();
            string company = string.Empty;
            string effective = string.Empty;
            string expired = string.Empty;
            if (serviceManager.GetKeyFileContents(fileName, ref company, ref effective, ref expired, _keyEncryptionKey, _rfcEncryptionKey))
            {
                CompanyName.Text = company;
                string[] dateParts = effective.Split('/');
                DateTime testDate = new DateTime(Int32.Parse(dateParts[2]), Int32.Parse(dateParts[0]), Int32.Parse(dateParts[1]));
                Effective.Value = testDate;
                dateParts = expired.Split('/');
                testDate = new DateTime(Int32.Parse(dateParts[2]), Int32.Parse(dateParts[0]), Int32.Parse(dateParts[1]));
                Expiration.Value = testDate;
            }
            else
            {
                if (ErrorsInfo.GetAllErrors().Trim().Length > 0)
                    MessageBox.Show(ErrorsInfo.GetAllErrors());
            }
        }

        private void btnSelectESDFile_Click(object sender, EventArgs e)
        {
            EsdFilePath.Text = SelectFileToRead();
        }
        
        private string SelectFileToRead()
        {
            openFileDialog1.FileName = string.Empty;
            openFileDialog1.InitialDirectory = _dataPath;
            if (openFileDialog1.ShowDialog() != DialogResult.OK || openFileDialog1.FileName.Length < 1)
            {
                return string.Empty;
            }
            return openFileDialog1.FileName;
        }

        private string SelectFileToWrite()
        {
            saveFileDialog1.FileName = string.Empty;
            saveFileDialog1.InitialDirectory = _dataPath;
            if (saveFileDialog1.ShowDialog() != DialogResult.OK || saveFileDialog1.FileName.Length < 1)
            {
                return string.Empty;
            }
            return saveFileDialog1.FileName;
        }


        private void radDecryptEsd_Click(object sender, EventArgs e)
        {
            radEncryptEsd.Checked = false;
            btnSelectESDFile.Text = "Select encrypted ESD file";
            EsdFilePath.Text = string.Empty;
            btnCreateESD.Text = "Choose new decrypted ESD file";
            txtEncryptedEsdPath.Text = string.Empty;
        }

        private void radEncryptEsd_Click(object sender, EventArgs e)
        {
            radDecryptEsd.Checked = false;
            btnSelectESDFile.Text = "Select ESD file to encrypt";
            EsdFilePath.Text = string.Empty;
            btnCreateESD.Text = "Choose new encrypted ESD file";
            txtEncryptedEsdPath.Text = string.Empty;
        }

        private void radEncryptEsd_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void btnProcessEsdFiles_Click(object sender, EventArgs e)
        {
            string result = null;
            if (radEncryptEsd.Checked)
            {
                ErrorsInfo.Reset();
                result = WriteNewEncryptedEsdFile();
            }
            else if (radDecryptEsd.Checked)
            {
                ErrorsInfo.Reset();
                result = WriteNewDecryptedEsdFile();
            }
            if (result != null)
            {
                if (result.Length > 0)
                {
                    MessageBox.Show(result);
                }
                else
                {
                    MessageBox.Show("Finished");
                }
            }
        }
    }
}
